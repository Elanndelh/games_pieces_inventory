﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

// somewhat based upon the TextMesh Pro example script: TMP_TextSelector_B
[RequireComponent(typeof(TextMeshProUGUI))]
public class OpenHyperlinks : MonoBehaviour, IPointerClickHandler {
    [SerializeField] bool _doesColorChangeOnHover = true;
    [SerializeField] Color _hoverColor = new Color(60f / 255f, 120f / 255f, 1f);

    TextMeshProUGUI _text;
    Canvas _canvas;
    Camera _camera;

    public bool IsLinkHighlighted { get { return _currentLink != -1; } }

    int _currentLink = -1;
    List<Color32[]> _originalVertexColors = new List<Color32[]>();


    protected virtual void Awake() {
        _text = GetComponent<TextMeshProUGUI>();
        _canvas = GetComponentInParent<Canvas>();

        // Get a reference to the camera if Canvas Render Mode is not ScreenSpace Overlay.
        if (_canvas.renderMode == RenderMode.ScreenSpaceOverlay)
            _camera = null;
        else
            _camera = _canvas.worldCamera;
    }
    /*********************************************************/

    void LateUpdate() {
        // is the cursor in the correct region (above the text area) and furthermore, in the link region?
        var isHoveringOver = TMP_TextUtilities.IsIntersectingRectTransform(_text.rectTransform, Input.mousePosition, _camera);
        int linkIndex = isHoveringOver ? TMP_TextUtilities.FindIntersectingLink(_text, Input.mousePosition, _camera)
            : -1;

        // Clear previous link selection if one existed.
        if (_currentLink != -1 && linkIndex != _currentLink) 
        {
            // Debug.Log("Clear old selection");
            SetLinkToColor(_currentLink, (linkIdx, vertIdx) => _originalVertexColors[linkIdx][vertIdx]);
            _originalVertexColors.Clear();
            _currentLink = -1;
        }

        // Handle new link selection.
        if (linkIndex != -1 && linkIndex != _currentLink) 
        {
            // Debug.Log("New selection");
            _currentLink = linkIndex;
            if( _doesColorChangeOnHover )
                _originalVertexColors = SetLinkToColor(linkIndex, (_linkIdx, _vertIdx) => _hoverColor);
        }

        // Debug.Log(string.Format("isHovering: {0}, link: {1}", isHoveringOver, linkIndex));
    }
    /*********************************************************/

    public void OnPointerClick(PointerEventData a_eventData) {
        // Debug.Log("Click at POS: " + a_eventData.position + "  World POS: " + a_eventData.worldPosition);

        int linkIndex = TMP_TextUtilities.FindIntersectingLink(_text, Input.mousePosition, _camera);
        if (linkIndex != -1) // was a link clicked?
        { 
            TMP_LinkInfo linkInfo = _text.textInfo.linkInfo[linkIndex];

            // Debug.Log(string.Format("id: {0}, text: {1}", linkInfo.GetLinkID(), linkInfo.GetLinkText()));
            // open the link id as a url, which is the metadata we added in the text field
            Application.OpenURL(linkInfo.GetLinkID());
        }
    }
    /*********************************************************/

    List<Color32[]> SetLinkToColor(int a_linkIndex, Func<int, int, Color32> a_colorForLinkAndVert) {
        TMP_LinkInfo linkInfo = _text.textInfo.linkInfo[a_linkIndex];

        var oldVertColors = new List<Color32[]>(); // store the old character colors

        for (int i = 0; i < linkInfo.linkTextLength; i++) // for each character in the link string
        { 
            int characterIndex = linkInfo.linkTextfirstCharacterIndex + i; // the character index into the entire text
            var charInfo = _text.textInfo.characterInfo[characterIndex];
            int meshIndex = charInfo.materialReferenceIndex; // Get the index of the material / sub text object used by this character.
            int vertexIndex = charInfo.vertexIndex; // Get the index of the first vertex of this character.

            Color32[] vertexColors = _text.textInfo.meshInfo[meshIndex].colors32; // the colors for this character
            oldVertColors.Add(vertexColors.ToArray());

            if (charInfo.isVisible) 
            {
                vertexColors[vertexIndex + 0] = a_colorForLinkAndVert(i, vertexIndex + 0);
                vertexColors[vertexIndex + 1] = a_colorForLinkAndVert(i, vertexIndex + 1);
                vertexColors[vertexIndex + 2] = a_colorForLinkAndVert(i, vertexIndex + 2);
                vertexColors[vertexIndex + 3] = a_colorForLinkAndVert(i, vertexIndex + 3);
            }
        }

        // Update Geometry
        _text.UpdateVertexData(TMP_VertexDataUpdateFlags.All);

        return oldVertColors;
    }
    /*********************************************************/
}