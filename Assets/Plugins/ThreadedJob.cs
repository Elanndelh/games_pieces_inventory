﻿using UnityEngine;
using System.Collections;

public class ThreadedJob {
	private bool m_IsDone = false, m_Canceled = false;
	protected object m_Handle = new object();
	private System.Threading.Thread m_Thread = null;

	public bool IsDone {
		get {
			bool tmp;

			lock (m_Handle) {
				tmp = m_IsDone;
			}

			return tmp;
		}

		set {
			lock (m_Handle) {
				m_IsDone = value;
			}
		}
	}

	public bool Canceled {
		get {
			bool tmp;

			lock (m_Handle) {
				tmp = m_Canceled;
			}

			return tmp;
		}

		set {
			lock (m_Handle) {
				m_Canceled = value;
			}
		}
	}

	public virtual void Start() {
		m_Thread = new System.Threading.Thread(Run);
		m_Thread.Start();
	}

	public virtual void Cancel() {
		Canceled = true;
	}

	public virtual void Abort() {
		m_Thread.Abort();
	}

	/// <summary>
	/// Job function. SHOUD NOT CONTAIN UNITY API CALL !
	/// </summary>
	protected virtual void ThreadFunction() { }
	/// <summary>
	/// Called when the job is finished. May contain Unity API.
	/// </summary>
	protected virtual void OnFinished() { }

	public virtual bool Update() {
		if (IsDone) {
			OnFinished();
			return true;
		}

		return false;
	}

	public IEnumerator WaitFor() {
		while(!Update()) {
			yield return null;
		}
	}

	private void Run() {
		ThreadFunction();
		IsDone = true;
	}
}