using System;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DepthFactory.Utils;
using SFB;

namespace DepthFactory.GamesPiecesInventory
{
    public class FirstLevelEditManager : MonoBehaviour
    {
        [SerializeField] TMP_InputField _titleIF;
        [SerializeField] MediaDisplay _imageDisplay;
        [SerializeField] TMP_Text _scanText;
        [SerializeField] TMP_InputField _scanIF;

        //public Action<A_Data> OnUpdate;
        public Action<GameData> OnUpdateGame;
        public Action OnDelete;

        A_Data _data;
        DatabaseManager _databaseManager;
        PopupManager _popupManager;
        string _mediaPath;

        const string POPUP_DELETE_MESSAGE = "Si vous confirmez, cet objet ({{ObjectName}}) sera définitivement supprimé.";
        const string DEFAULT_SCAN_MESSAGE = "Appuyer pour scanner";

        
    ///////////////////////////////////////////////////////////////
    /// GENERAL FUNCTIONS /////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        void Start()
        {
            _databaseManager = DatabaseManager.Instance;
            _popupManager = PopupManager.Instance;

            _mediaPath = System.IO.Directory.GetParent(Application.streamingAssetsPath).FullName + "/StreamingAssets/Medias/";
            System.IO.Directory.CreateDirectory(_mediaPath);
        }
        /*********************************************************/
        
    ///////////////////////////////////////////////////////////////
    /// PRIVATE FUNCTIONS /////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        void CB_SetTitle()
        {
            _titleIF.text = _data.name;
            //OnUpdate?.Invoke(_data);
            OnUpdateGame?.Invoke((GameData)_data);
        }
        /*********************************************************/
        void CB_SetImage()
        {
            if (string.IsNullOrEmpty(_data.path) == false)
            {
                _imageDisplay.Init(MediaType.Type.Image, _data.path);
            }
            else
            {
                _imageDisplay.ResetDisplay();
            }

            //OnUpdate?.Invoke(_data);
            OnUpdateGame?.Invoke((GameData)_data);
        }
        /*********************************************************/
        void CB_SetScan()
        {
            _scanText.text = ((GameData)_data).scan;
            DFBarcodeScanner.Instance.Btn_Close();
            //OnUpdate?.Invoke(_data);
            OnUpdateGame?.Invoke((GameData)_data);
        }
        /*********************************************************/

        /// <summary>
        /// Add photo to the server and form
        /// </summary>
        /// <param name="a_texture"></param>
        void CB_AddPhoto(List<Texture2D> a_textureList)
		{
            if (a_textureList != null && a_textureList.Count > 0)
			{
				foreach (Texture2D texture in a_textureList)
				{
					if (texture != null)
						MediaManager.Instance.UploadFile(texture, GPIConstants.URL_MEDIAS_PHP, _data.path, CB_UploadSelectedFile);
				}
			}
		}
        /*********************************************************/
		void CB_UploadSelectedFile(string a_path)
		{
            if (a_path.StartsWith("Erreur:"))
			{
                Debug.LogError("erreur");
			}
			else
			{
                _data.path = a_path;
                _databaseManager.UpdateGame(((GameData)_data), CB_SetImage);
			}
		}
		/*********************************************************/

        void CB_Delete()
        {
            OnDelete?.Invoke();

            if (string.IsNullOrEmpty(_data.path) == false)
                MediaManager.Instance.DeleteFile(_data.path, GPIConstants.URL_MEDIAS_PHP, CB_DeleteImage);
        }
        /*********************************************************/

        void CB_DeleteImage() { }
		/*********************************************************/

        void OnDeleteGame()
        {
            _databaseManager.DeleteGame(((GameData)_data), CB_Delete);
        }
        /*********************************************************/

        void OnScan(string a_value)
        {
            ((GameData)_data).scan = a_value;
            _databaseManager.UpdateGame(((GameData)_data), CB_SetScan);
        }
        /*********************************************************/
        
    ///////////////////////////////////////////////////////////////
    /// PUBLIC FUNCTIONS //////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        public void Init(GameData a_data)
        {
            _data = a_data;
            _titleIF.text = _data.name;
            _scanText.text = string.IsNullOrEmpty(((GameData)_data).scan) ? DEFAULT_SCAN_MESSAGE : ((GameData)_data).scan;
            _scanIF.onEndEdit.SetPersistentListenerState(0, UnityEngine.Events.UnityEventCallState.Off);
            _scanIF.text = ((GameData)_data).scan;
            _scanIF.onEndEdit.SetPersistentListenerState(0, UnityEngine.Events.UnityEventCallState.RuntimeOnly);

            if (string.IsNullOrEmpty(_data.path) == false)
            {
                _imageDisplay.Init(MediaType.Type.Image, _data.path);
            }
            else
            {
                _imageDisplay.ResetDisplay();
            }
        }
        /*********************************************************/

        public void Btn_SetTitle(string a_value)
        {
            _data.name = a_value;
            _databaseManager.UpdateGame(((GameData)_data), CB_SetTitle);
        }
        /*********************************************************/

        public void Btn_TakePhoto()
        {
            if (Webcam.Instance != null)
                Webcam.Instance.InitiatePhotoShot(CB_AddPhoto);
        }
        /*********************************************************/

        public void Btn_BrowseFile()
        {
#if UNITY_STANDALONE_OSX || UNITY_STANDALONE_WIN || UNITY_EDITOR
            // Open file with filter
            ExtensionFilter[] extensions;
            extensions = new ExtensionFilter[]
            {
                new ExtensionFilter("Image Files", "png", "jpg", "jpeg", "PNG", "JPEG", "JPG" )
            };

            string[] paths = StandaloneFileBrowser.OpenFilePanel("Import media file", _mediaPath, extensions, false);
            if (paths != null && paths.Length > 0)
            {
                MediaManager.Instance.UploadFile(paths[0], GPIConstants.URL_MEDIAS_PHP, _data.path, CB_UploadSelectedFile);
            }
#elif UNITY_ANDROID || UNITY_IOS
            NativeGallery.Permission permission = NativeGallery.GetImageFromGallery((path) =>
            {
                Debug.Log("Image path: " + path);
                if (path != null)
                {
                    MediaManager.Instance.UploadFile(path, GPIConstants.URL_MEDIAS_PHP, _data.path, CB_UploadSelectedFile);
                }
            }, "Select a PNG image", "image/png");
#endif
        }
        /*********************************************************/

        public void Btn_OpenScanner()
        {
            DFBarcodeScanner.Instance.Init(OnScan);
        }
        /*********************************************************/

        public void Btn_Delete()
        {
            _popupManager.Display(POPUP_DELETE_MESSAGE, OnDeleteGame);
        }
        /*********************************************************/

        public void Btn_EditScanIFVisibility(bool a_show)
        {
            _scanIF.gameObject.SetActive(a_show);
        }
        /*********************************************************/

        public void Btn_EditScan(string a_scan)
        {
            OnScan(a_scan);
        }
        /*********************************************************/
    }
}
