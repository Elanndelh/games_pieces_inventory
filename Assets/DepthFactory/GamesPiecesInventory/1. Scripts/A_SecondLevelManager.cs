using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using DepthFactory.Utils;

namespace DepthFactory.GamesPiecesInventory
{
    public abstract class A_SecondLevelManager : A_LevelManager
    {
        [SerializeField] protected TMP_Text _title;
        [SerializeField] protected TMP_InputField _titleIF;
        [SerializeField] protected MediaDisplay _imageDisplay;

        public Action OnDelete;

        protected PopupManager _popupManager;
        protected string _mediaPath;

        protected const string POPUP_DELETE_MESSAGE = "Si vous confirmez, ce sous-objet ({{SubObjectName}}) sera définitivement supprimé.";
        

    ///////////////////////////////////////////////////////////////
    /// GENERAL FUNCTIONS /////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        protected override void Start()
        {
            base.Start();
            _popupManager = PopupManager.Instance;

            _mediaPath = System.IO.Directory.GetParent(Application.streamingAssetsPath).FullName + "/StreamingAssets/Medias/";
            System.IO.Directory.CreateDirectory(_mediaPath);
        }
        /*********************************************************/
        
    ///////////////////////////////////////////////////////////////
    /// PROTECTED FUNCTIONS ///////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        protected abstract void CB_SetTitle();
        /*********************************************************/

        protected abstract void CB_AddPhoto(List<Texture2D> a_textureList);
        /*********************************************************/

        protected abstract void OnConfirmDelete();
        /*********************************************************/
        
    ///////////////////////////////////////////////////////////////
    /// PUBLIC FUNCTIONS //////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        public void UI_DisplayEditTitle(bool a_show)
        {
            if (IsWrongType())
                return;
            _title.enabled = !a_show;
            _titleIF.gameObject.SetActive(a_show);
        }
        /*********************************************************/

        public void UI_TakePhoto()
        {
            if (IsWrongType())
                return;
            if (Webcam.Instance != null)
                Webcam.Instance.InitiatePhotoShot(CB_AddPhoto);
        }
        /*********************************************************/

        public void UI_Delete()
        {
            if (IsWrongType())
                return;
            _popupManager.Display(POPUP_DELETE_MESSAGE, OnConfirmDelete);
        }
        /*********************************************************/
    }
}
