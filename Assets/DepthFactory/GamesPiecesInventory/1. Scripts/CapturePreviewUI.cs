﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace DepthFactory.Utils
{
    public class CapturePreviewUI : MonoBehaviour
    {
        [SerializeField] RawImage _image;

        Texture2D _texture;
        public Texture2D GetTexture { get { return _texture; } }

        public bool IsUsed { get { return _texture != null; } }

        
    ///////////////////////////////////////////////////////////////
    /// PUBLIC FUNCTIONS //////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        /// <summary>
        /// Sets given texture to preview rawimage component
        /// </summary>
        /// <param name="inTexture">texture to display with rawimage</param>
        public void SetImage(Texture2D inTexture)
        {
            _texture = inTexture;
            _image.texture = _texture;
        }
        /******************************************************/

        public void ResetImage()
        {
            _image.texture = null;
        }
        /******************************************************/
    }
}