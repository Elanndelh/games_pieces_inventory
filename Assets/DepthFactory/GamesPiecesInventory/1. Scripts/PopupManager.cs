using DepthFactory.UI;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace DepthFactory.GamesPiecesInventory
{
    public class PopupManager : Singleton<PopupManager>
    {
        [SerializeField] DF_TMPText _text;
        [SerializeField] Button _cancelButton;

        Action OnConfirm;

                
    ///////////////////////////////////////////////////////////////
    /// PRIVATE FUNCTIONS /////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        void Hide()
        {
            transform.localScale = Vector3.zero;
        }
        /******************************************************/
        
    ///////////////////////////////////////////////////////////////
    /// PUBLIC FUNCTIONS //////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        public void Display(string a_text, Action a_onConfirm, bool a_blockCancel = false)
        {
            transform.localScale = Vector3.one;
            //_text.text = a_text;
            _text.LocalizeText(a_text);
            OnConfirm = a_onConfirm;
            _cancelButton.interactable = !a_blockCancel;
        }
        /******************************************************/

        public void Btn_Cancel()
        {
            Hide();
        }
        /******************************************************/

        public void Btn_Confirm()
        {
            Hide();
            OnConfirm?.Invoke();
        }
        /******************************************************/
    }
}
