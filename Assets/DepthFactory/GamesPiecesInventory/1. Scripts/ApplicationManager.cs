using DepthFactory.Utils;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DepthFactory.UI;

namespace DepthFactory.GamesPiecesInventory
{
    public class ApplicationManager : MonoBehaviour, IPointerClickHandler
    {
        public enum Panels { Games, Game/*, GameEdit*/, Piece, Settings, About, Collection/*, CollectionEdit*/, Book }

        [SerializeField] Canvas _gamesCanvas, _gameCanvas, /*_gameEditCanvas,*/ _pieceCanvas, _settingsCanvas, _aboutCanvas, _collectionCanvas, _bookCanvas/*, _collectionEditCanvas*/;
        [SerializeField] GameManager _gameManager;
        [SerializeField] PieceManager _pieceManager;
        [SerializeField] CollectionManager _collectionManager;
        [SerializeField] BookManager _bookManager;
        //[SerializeField] GameEditManager _gameEditManager;
        //[SerializeField] FirstLevelEditManager _flEditManager;
        //[SerializeField] CollectionEditManager _collectionEditManager;
        [SerializeField] SettingsManager _settingsManager;

        [Header("Title")]
        [SerializeField] TMP_Text _title;
        [SerializeField] TMP_InputField _searchIF;
        [SerializeField] GameObject _menu;
        [SerializeField] TMP_Text _secondButtonText;

        [Header("List of games")]
        [SerializeField] ScrollRect _gamesScrollRect;
        [SerializeField] RectTransform _contentRT;
        [SerializeField] RectTransform _contentByLinesRT;
        [SerializeField] GameObject _gamePrefab;
        [SerializeField] GameObject _gameByLinesPrefab;

        [Header("List of collections")]
        [SerializeField] GameObject _collectionPrefab;
        [SerializeField] GameObject _collectionByLinesPrefab;

        [Header("Pagination")]
        [SerializeField] PaginationManager _paginationManager;

        [Header("Search")]
        [SerializeField] GameObject _gameAttributesBox;
        [SerializeField] GameObject _bookAttributesBox;
        [SerializeField] TMP_InputField _authorSearchIF;

        DatabaseManager _databaseManager;
        LocalizationManager _localizationManager;
        GameElement _currentGameElement;
        PieceElement _currentPieceElement;
        CollectionElement _currentCollectionElement;
        BookElement _currentBookElement;
        RectTransform _currentContentRT;


        
    ///////////////////////////////////////////////////////////////
    /// GENERAL FUNCTIONS /////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        void Awake()
        {
            GPIConstants.Init();
            SkinHelper.Init();
            _localizationManager = LocalizationManager.Instance;
            //LocalizationManager.OnTypeUpdate += OnTypeUpdate;
            _settingsManager.Init(OnQuitSettingsPanel);
            _secondButtonText.text = _settingsManager.Data.inventoryType == 0 ? "Liste � valider" : "Liste � lire";
            _currentContentRT = _settingsManager.Data.displayByLines ? _contentByLinesRT : _contentRT;
        }
        /*********************************************************/

        void Start()
        {
            ChangeCanvas(Panels.Games);
            _databaseManager = DatabaseManager.Instance;
            _databaseManager.DaysBeforeValidation = _settingsManager.Data.daysBeforeValidation;
            _databaseManager.URL = _settingsManager.Data.url;
            _databaseManager.UnvalidatedGames = false;
            GetFirstLevel();
            _paginationManager.OnPaginationChange += OnPaginationChange;

            //_databaseManager.TestISBN();
        }
        /*********************************************************/
        
    ///////////////////////////////////////////////////////////////
    /// PRIVATE FUNCTIONS /////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        void ChangeCanvas(Panels a_panel)
        {
            _gamesCanvas.enabled = a_panel == Panels.Games;
            _gameCanvas.enabled = a_panel == Panels.Game;
            //_gameEditCanvas.enabled = a_panel == Panels.GameEdit;
            _pieceCanvas.enabled = a_panel == Panels.Piece;
            _collectionCanvas.enabled = a_panel == Panels.Collection;
            _bookCanvas.enabled = a_panel == Panels.Book;
            //_collectionEditCanvas.enabled = a_panel == Panels.CollectionEdit;
            _settingsCanvas.enabled = a_panel == Panels.Settings;
            _aboutCanvas.enabled = a_panel == Panels.About;
        }
        /*********************************************************/

        void GetFirstLevel()
		{
            switch (LocalizationManager.Instance.CurrentType)
			{
                case LocalizationType.GameFR:
                    _databaseManager.GetGamesNumber(CB_GetGamesNumber);
                    break;
                case LocalizationType.BookFR:
                    _databaseManager.GetCollectionsNumber(CB_GetCollectionsNumber);
                    break;
            }
            _gameAttributesBox.SetActive(LocalizationManager.Instance.CurrentType == LocalizationType.GameFR);
            _bookAttributesBox.SetActive(LocalizationManager.Instance.CurrentType == LocalizationType.BookFR);
        }
		/*********************************************************/

        void OnPaginationChange()
		{
            _gamesScrollRect.verticalScrollbar.value = 1;
		}
		/*********************************************************/

    #region Games
		void CB_GetGamesNumber()
        {
            _paginationManager.Init(_settingsManager.Data.elementsNumberByPage, CB_DisplayGames);
        }
        /*********************************************************/

        void CB_DisplayGames()
        {
            ClearFirstLevel();
            _currentContentRT = _settingsManager.Data.displayByLines ? _contentByLinesRT : _contentRT;
            foreach (GameData gameData in _databaseManager.GamesData)
                InstantiateGame(gameData);
        }
        /*********************************************************/

        GameObject InstantiateGame(GameData a_data)
        {
            GameObject gameGO;
            
            if (_settingsManager.Data.displayByLines)
                gameGO = Instantiate(_gameByLinesPrefab, _contentByLinesRT);
            else 
                gameGO = Instantiate(_gamePrefab, _contentRT);
            gameGO.name = a_data.name;
            gameGO.GetComponent<GameElement>().Init(a_data, OnSelectGameElement);
            return gameGO;
        }
        /*********************************************************/

        void ClearFirstLevel()
        {
            foreach (Transform child in _currentContentRT)
                Destroy(child.gameObject);
        }
        /*********************************************************/

        void CB_AddGame()
        {
            GameData data = new GameData(_databaseManager.LastInsertedIndex, string.Empty, string.Empty);

            GameObject gameGO = InstantiateGame(data);
            _currentGameElement = gameGO.GetComponent<GameElement>();

            ChangeCanvas(Panels.Game);
            _gameManager.Init(data, OnSelectPieceElement, true);
            _gameManager.OnInsert = OnInsertPiece;
            _gameManager.OnUpdate = OnUpdateGame;
            _gameManager.OnDelete = OnDeleteGame;
        }
        /*********************************************************/

        void OnSelectGameElement(GameData a_data, GameElement a_element)
        {
            ChangeCanvas(Panels.Game);
            _gameManager.Init(a_data, OnSelectPieceElement);
            _gameManager.OnInsert = OnInsertPiece;
            _gameManager.OnUpdate = OnUpdateGame;
            _gameManager.OnDelete = OnDeleteGame;
            _currentGameElement = a_element;
        }
        /*********************************************************/

        void OnUpdateGame(GameData a_gameData)
        {
            _currentGameElement.DisplayData(a_gameData);
            _gameManager.UpdateData(a_gameData);
        }
        /*********************************************************/

        void OnSelectPieceElement(PieceData a_data, PieceElement a_element)
        {
            ChangeCanvas(Panels.Piece);
            _pieceManager.Init(a_data);
            _pieceManager.OnUpdate = OnUpdatePiece;
            _pieceManager.OnDelete = OnDeletePiece;
            _currentPieceElement = a_element;
        }
        /*********************************************************/

        void OnUpdatePiece(PieceData a_pieceData)
        {
            _currentPieceElement.DisplayData(a_pieceData);
        }
        /*********************************************************/

        void OnInsertPiece(PieceElement a_pieceElement, PieceData a_pieceData)
        {
            _currentPieceElement = a_pieceElement;

            ChangeCanvas(Panels.Piece);
            _pieceManager.Init(a_pieceData);
            _pieceManager.OnUpdate = OnUpdatePiece;
            _pieceManager.OnDelete = OnDeletePiece;
        }
        /*********************************************************/

        void OnDeletePiece()
        {
            ChangeCanvas(Panels.Game);
            Destroy(_currentPieceElement.gameObject);
            _currentPieceElement = null;
        }
        /*********************************************************/

        void OnDeleteGame()
        {
            GetFirstLevel();
            ChangeCanvas(Panels.Games);
            //Destroy(_currentGameElement.gameObject);
            _currentGameElement = null;
        }
        /*********************************************************/
    #endregion

    #region Books
        void CB_GetCollectionsNumber()
        {
            _paginationManager.Init(_settingsManager.Data.elementsNumberByPage, CB_DisplayCollections);
        }
        /*********************************************************/

        void CB_DisplayCollections()
        {
            ClearFirstLevel();
            _currentContentRT = _settingsManager.Data.displayByLines ? _contentByLinesRT : _contentRT;
            foreach (CollectionData collectionData in _databaseManager.CollectionsData)
                InstantiateCollection(collectionData);
        }
        /*********************************************************/

        void CB_AddCollection()
        {
            CollectionData data = new CollectionData(_databaseManager.LastInsertedIndex, string.Empty, string.Empty);

            GameObject gameGO = InstantiateCollection(data);
            _currentCollectionElement = gameGO.GetComponent<CollectionElement>();

            ChangeCanvas(Panels.Collection);
            _collectionManager.Init(data, OnSelectBookElement, true);
            _collectionManager.OnInsert = OnInsertBook;
            _collectionManager.OnUpdate = OnUpdateCollection;
            _collectionManager.OnDelete = OnDeleteCollection;
        }
        /*********************************************************/
        GameObject InstantiateCollection(CollectionData a_data)
        {
            GameObject collectionGO;

            if (_settingsManager.Data.displayByLines)
                collectionGO = Instantiate(_collectionByLinesPrefab, _contentByLinesRT);
            else
                collectionGO = Instantiate(_collectionPrefab, _contentRT);
            collectionGO.name = a_data.name;
            collectionGO.GetComponent<CollectionElement>().Init(a_data, OnSelectCollectionElement);
            return collectionGO;
        }
        /*********************************************************/

        void OnSelectCollectionElement(CollectionData a_data, CollectionElement a_element)
        {
            ChangeCanvas(Panels.Collection);
            //_gameManager.ActivateGOs();
            _collectionManager.Init(a_data, OnSelectBookElement);
            _collectionManager.OnInsert = OnInsertBook;
            _collectionManager.OnUpdate = OnUpdateCollection;
            _collectionManager.OnDelete = OnDeleteCollection;
            _currentCollectionElement = a_element;
        }
        /*********************************************************/

        void OnUpdateCollection(CollectionData a_data)
        {
            _currentCollectionElement.DisplayData(a_data);
            _collectionManager.UpdateData(a_data);
        }
        /*********************************************************/

        void OnSelectBookElement(BookData a_data, BookElement a_element)
        {
            ChangeCanvas(Panels.Book);
            //_pieceManager.ActivateGOs();
            _bookManager.Init(a_data);
            _bookManager.OnUpdate = OnUpdateBook;
            _bookManager.OnDelete = OnDeleteBook;
            _currentBookElement = a_element;
        }
        /*********************************************************/

        void OnUpdateBook(BookData a_pieceData)
        {
            _currentBookElement.DisplayData(a_pieceData);
        }
        /*********************************************************/

        void OnInsertBook(BookElement a_pieceElement, BookData a_pieceData)
        {
            _currentBookElement = a_pieceElement;

            ChangeCanvas(Panels.Book);
            //_pieceManager.ActivateGOs();
            _bookManager.Init(a_pieceData);
            _bookManager.OnUpdate = OnUpdateBook;
            _bookManager.OnDelete = OnDeleteBook;
        }
        /*********************************************************/

        void OnDeleteBook()
        {
            ChangeCanvas(Panels.Collection);
            Destroy(_currentBookElement.gameObject);
            _currentBookElement = null;
        }
        /*********************************************************/

        void OnDeleteCollection()
        {
            GetFirstLevel();
            ChangeCanvas(Panels.Games);
            //Destroy(_currentCollectionElement.gameObject);
            _currentCollectionElement = null;
        }
        /*********************************************************/
        #endregion

        void OnQuitSettingsPanel()
        {
            _databaseManager.DaysBeforeValidation = _settingsManager.Data.daysBeforeValidation;
            _gamesScrollRect.content = _settingsManager.Data.displayByLines ? _contentByLinesRT : _contentRT;
            GetFirstLevel();
            //_databaseManager.GetGamesNumber(CB_GetGamesNumber);
            _secondButtonText.text = _settingsManager.Data.inventoryType == 0 ? "Liste � valider" : "Liste � lire";
        }
        /*********************************************************/

        void OnSearchByScan(string a_text)
        {
            _searchIF.text = a_text;
            _databaseManager.Search = a_text;
            _databaseManager.GetGamesNumber(CB_GetGamesNumber);
            DFBarcodeScanner.Instance.Btn_Close();
        }
        /*********************************************************/

        /*void OnTypeUpdate()
		{
            switch (_localizationManager.CurrentType)
            {
                case LocalizationType.GameFR:
                    _gameManager.ActivateGOs(true);
                    _pieceManager.ActivateGOs(true);
                    _collectionManager.ActivateGOs(false);
                    _bookManager.ActivateGOs(false);
                    break;
                case LocalizationType.BookFR:
                    _gameManager.ActivateGOs(false);
                    _pieceManager.ActivateGOs(false);
                    _collectionManager.ActivateGOs(true);
                    _bookManager.ActivateGOs(true);
                    break;
            }
        }
        /*********************************************************/

    ///////////////////////////////////////////////////////////////
    /// PUBLIC FUNCTIONS //////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        public void Btn_DisplaySearch(bool a_show)
        {
            _title.enabled = !a_show;
            _searchIF.gameObject.SetActive(a_show);
        }
        /*********************************************************/

        public void Btn_LoadCanvas(int a_canvasIndex)
        {
            ChangeCanvas((Panels)a_canvasIndex);
        }
        /*********************************************************/

        // Called by BackButtons
        public void Btn_LoadFirstLevelCanvas()
        {
            switch (_localizationManager.CurrentType)
            {
                case LocalizationType.GameFR:
                    ChangeCanvas(Panels.Game);
                    break;
                case LocalizationType.BookFR:
                    ChangeCanvas(Panels.Collection);
                    break;
            }
        }
        /*********************************************************/
        // Called by FirstLevel > Title > Toggle
        /*public void Btn_LoadFirstLevelEditCanvas()
        {
            switch (_localizationManager.CurrentType)
            {
                case LocalizationType.GameFR:
                    ChangeCanvas(Panels.GameEdit);
                    break;
                case LocalizationType.BookFR:
                    ChangeCanvas(Panels.CollectionEdit);
                    break;
            }
        }
        /*********************************************************/
        // Not called
        public void Btn_LoadSecondLevelCanvas()
        {
            switch (_localizationManager.CurrentType)
            {
                case LocalizationType.GameFR:
                    ChangeCanvas(Panels.Piece);
                    break;
                case LocalizationType.BookFR:
                    ChangeCanvas(Panels.Book);
                    break;
            }
        }
        /*********************************************************/

        /*public void Btn_EditGame()
        {
            _gameEditManager.Init(_gameManager.Data);
            _gameEditManager.OnUpdate = OnUpdateGame;
            _gameEditManager.OnDelete = OnDeleteGame;
        }
        /*********************************************************/
        /*public void UI_EditFirstLevel()
        {
            switch (_localizationManager.CurrentType)
			{
                case LocalizationType.GameFR:
                    _gameEditManager.Init(_gameManager.Data);
                    _gameEditManager.OnUpdate = OnUpdateGame;
                    _gameEditManager.OnDelete = OnDeleteGame;
                    break;
                case LocalizationType.BookFR:
                    _collectionEditManager.Init(_collectionManager.Data);
                    _collectionEditManager.OnUpdate = OnUpdateCollection;
                    _collectionEditManager.OnDelete = OnDeleteCollection;
                    break;
            }
        }
        /*********************************************************/

        public void UI_AddFirstLevelItem()
		{
            switch (LocalizationManager.Instance.CurrentType)
            {
                case LocalizationType.GameFR:
                    GameData gameData = new GameData();
                    _databaseManager.AddGame(gameData, CB_AddGame);
                    break;
                case LocalizationType.BookFR:
                    CollectionData bookData = new CollectionData();
                    _databaseManager.AddCollection(bookData, CB_AddCollection);
                    break;
            }
        }
        /*********************************************************/

        #region Menu
        public void Btn_Menu()
        {
            _menu.SetActive(true);
        }
        /*********************************************************/

        public void Btn_Quit()
        {
            Application.Quit();
        }
        /*********************************************************/

        public void OnPointerClick(PointerEventData a_eventData)
        {
            _menu.SetActive(false);
        }
        /*********************************************************/

        /*public void Btn_LoadGames()
        {
            _menu.SetActive(false);
            _databaseManager.UnvalidatedGames = false;
            _databaseManager.GetGamesNumber(CB_GetGamesNumber);
        }
        /*********************************************************/

        // Called by Menu > Games Button
        public void UI_LoadFirstLevel()
        {
            _menu.SetActive(false);
            _databaseManager.UnvalidatedGames = false;
            GetFirstLevel();
        }
        /*********************************************************/

        /*public void Btn_LoadUnvalidatedGames()
        {
            _menu.SetActive(false);
            _databaseManager.UnvalidatedGames = true;
            _databaseManager.GetGamesNumber(CB_GetGamesNumber);
        }
        /*********************************************************/

        // Called by Menu > GamesToValidated Button
        public void UI_LoadFirstLevelFiltered()
        {
            _menu.SetActive(false);
            _databaseManager.UnvalidatedGames = true;
            GetFirstLevel();
        }
        /*********************************************************/

        public void Btn_LoadSettings()
        {
            _menu.SetActive(false);
            ChangeCanvas(Panels.Settings);
        }
        /*********************************************************/

        public void Btn_LoadAboutPanel()
        {
            _menu.SetActive(false);
            ChangeCanvas(Panels.About);
        }
        /*********************************************************/
        #endregion

        #region Search
        public void Btn_SearchGame(string a_text)
        {
            _databaseManager.Search = a_text;
            GetFirstLevel();
        }
        /*********************************************************/

        public void Btn_ResetSearch()
        {
            _searchIF.text = string.Empty;
            Btn_DisplaySearch(false);
            _databaseManager.Search = null;
            GetFirstLevel();
        }
        /*********************************************************/

        public void Btn_OpenSearchScan()
        {
            DFBarcodeScanner.Instance.Init(OnSearchByScan);
        }
        /*********************************************************/

        public void UI_SetAge(string a_value)
        {
            _databaseManager.Age = int.Parse(a_value);
            GetFirstLevel();
        }
        /*********************************************************/
        public void UI_SetPlayerNumber(string a_value)
        {
            _databaseManager.PlayerNumber = int.Parse(a_value);
            GetFirstLevel();
        }
        /*********************************************************/
        public void UI_SetDuration(string a_value)
        {
            _databaseManager.Duration = int.Parse(a_value);
            GetFirstLevel();
        }
        /*********************************************************/
        public void UI_SearchNameOrAuthor(string a_text)
        {
            _databaseManager.SubSearch = a_text;
            GetFirstLevel();
        }
        /*********************************************************/
        public void UI_ResetNameOrAuthorSearch()
        {
            _authorSearchIF.text = string.Empty;
            Btn_DisplaySearch(false);
            _databaseManager.SubSearch = null;
            GetFirstLevel();
        }
        /*********************************************************/
        #endregion
    }
}
