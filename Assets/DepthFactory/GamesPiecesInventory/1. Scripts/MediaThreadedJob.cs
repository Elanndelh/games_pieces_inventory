﻿using System;
using System.Collections;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using UnityEngine.Networking;

namespace DepthFactory.GamesPiecesInventory
{
    public class MediaThreadedJob : ThreadedJob
    {
        public string path;
        public byte[] imageBytes;
        public Action<byte[]> OnThreadFinished;

        UnityWebRequest _request;
        UnityWebRequestAsyncOperation _asyncOperation;


        static bool TrustCertificate(object sender, X509Certificate x509Certificate, X509Chain x509Chain, SslPolicyErrors sslPolicyErrors)
        {
            // all Certificates are accepted
            return true;
        }
        /*********************************************************/

        protected override void ThreadFunction()
        {
            // Do your threaded task. DON'T use the Unity API here
            try
            {
                if (new Uri(path).IsFile)
                {
                    imageBytes = File.ReadAllBytes(path);
                    /*byte[] bytes;
                    bytes = File.ReadAllBytes(path);
                    texture.LoadImage(bytes);*/
                }
                else
                {
/*#if !UNITY_2018_1_OR_NEWER
                    // TODO : use UnityWebRequestMultimedia
                    _request = UnityWebRequest.Post(PersistentData.Instance.data.host, path.GetAbsolutePath());
                    _request.certificateHandler = new Requests.AcceptAllCertificatesSignedWithASpecificKeyPublicKey();
                    _asyncOperation = _request.SendWebRequest();

                    if (RequestManager.Instance != null)
                        RequestManager.Instance.LaunchRequest(CoroutineRequest());
                    //yield return request.SendWebRequest();
                    //texture = _request.texture;
#else*/
                    ServicePointManager.ServerCertificateValidationCallback = TrustCertificate;
                    ServicePointManager.Expect100Continue = true;

                    HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(path);
                    //webRequest.ContentType = "application/x-www-form-urlencoded";
                    webRequest.Proxy = null;
                    webRequest.Method = "GET";
                    try
                    {
                        HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse();
                        int bytesRead;
                        //int position = 0;
                        int bufferSize = 512;
                        byte[] buffer = new byte[bufferSize];
                        using (MemoryStream ms = new MemoryStream())
                        {
                            using (BinaryReader reader = new BinaryReader(webResponse.GetResponseStream()))
                            {
                                ms.Position = 0;
                                while ((bytesRead = reader.Read(buffer, 0, bufferSize)) != 0)
                                {
                                    ms.Write(buffer, 0, bytesRead);
                                    //position += bytesRead;
                                }
                            }
                            imageBytes = ms.ToArray();
                        }
                        webResponse.Close();
                    }
                    catch
                    {
                        imageBytes = null;
                    }
//#endif
                }
            }
            catch { }
        }
        /*********************************************************/

        public IEnumerator CoroutineRequest()
        {
#if UNITY_2018_1_OR_NEWER
            while (!_request.isDone && !_asyncOperation.isDone)
            {
                yield return null;
            }
            if (string.IsNullOrEmpty(_request.error))
            {
                //if (request.texture != null && request.texture.width == 8 && request.texture.height == 8)
                //	SetData(ApplicationManager.Instance.pixel);
                //else
                //   SetData(request.texture);

                if (!string.IsNullOrEmpty(_request.downloadHandler.text))
                {
                    try
                    {
                        /*int bytesRead;
                        //int position = 0;
                        int bufferSize = 512;
                        byte[] buffer = new byte[bufferSize];
                        using (MemoryStream ms = new MemoryStream())
                        {
                            using (BinaryReader reader = new BinaryReader(_request.downloadHandler.text))
                            {
                                ms.Position = 0;
                                while ((bytesRead = reader.Read(buffer, 0, bufferSize)) != 0)
                                {
                                    ms.Write(buffer, 0, bytesRead);
                                    //position += bytesRead;
                                }
                            }
                            imageBytes = ms.ToArray();
                        }*/
                        imageBytes = System.Text.Encoding.UTF8.GetBytes(_request.downloadHandler.text);

                        //SetData(JSON.Parse(_request.downloadHandler.text));
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Error while parsing response for JSON " + _request.downloadHandler.text + "\n- Exception:" + e); //Debug.LogError();
                    }
                }

                // Callback only when request is successful
                /* if (onSuccessRequestCallback != null)
                     onSuccessRequestCallback(this);*/
            }
            else
            {
                string requestClass = this.GetType().Name;
                //SetError(_request.error + " from : " + requestClass);
                Console.WriteLine(_request.url);
            }

            //if (onSuccessRequestCallback != null)
            //    onSuccessRequestCallback(this);

            _request.Dispose();
            _request = null;
#endif
        }
        /*********************************************************/

        protected override void OnFinished()
        {
            // This is executed by the Unity main thread when the job is finished
            if (OnThreadFinished != null)
                OnThreadFinished(imageBytes);
        }
        /*********************************************************/
    }
}
