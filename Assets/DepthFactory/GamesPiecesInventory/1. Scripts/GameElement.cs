using System;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DepthFactory.Utils;

namespace DepthFactory.GamesPiecesInventory
{
    public class GameElement : MonoBehaviour//: A_FirstLevelElement
    {
        [SerializeField] MediaDisplay _imageDisplay;
        [SerializeField] TMP_Text _title;

        GameData _data;
        Action<GameData, GameElement> OnSelect;


    ///////////////////////////////////////////////////////////////
    /// PUBLIC FUNCTIONS //////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        public void Init(GameData a_data, Action<GameData, GameElement> a_selectAction)
        {
            DisplayData(a_data);
            OnSelect = a_selectAction;
        }
        /*********************************************************/

        public void DisplayData(GameData a_data)
        {
            _data = a_data;

            if (_imageDisplay != null)
            {
                if (string.IsNullOrEmpty(_data.path) == false)
                {
                    _imageDisplay.Init(MediaType.Type.Image, _data.path);
                }
                else
                {
                    _imageDisplay.ResetDisplay();
                }
            }
            _title.text = _data.name;
        }
        /*********************************************************/

        public void Btn_Select()
        {
            OnSelect?.Invoke(((GameData)_data), this);
        }
        /*********************************************************/
        public override string ToString()
        {
            return "[GameElement] " + _data.name + " (" + _data.id + ") siblingIndex: " + transform.GetSiblingIndex();
        }
        /*********************************************************/
    } // Class GameElement


    public class GameData : A_Data
    {
        public DateTime lastValidationDate;
        public string scan;
        public string descr;
        public PlaceType placeType;
        public string placeDetails;
        public string placeName;
        public DateTime placeReminder;
        public int ageLimit;
        public int minPlayerLimit;
        public int maxPlayerLimit;
        public int duration;
        public string customId;
        
    ///////////////////////////////////////////////////////////////
    /// CONSTRUCTORS //////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        public GameData() : base()
        {
            id = -1;
            name = string.Empty;
            path = string.Empty;
            lastValidationDate = default;
            scan = string.Empty;
            descr = string.Empty;
            placeType = PlaceType.Tidy;
            placeDetails = string.Empty;
            placeName = string.Empty;
            placeReminder = default;
            ageLimit = -1;
            minPlayerLimit = -1;
            maxPlayerLimit = -1;
            duration = -1;
            customId = string.Empty;
        }
        /*********************************************************/

        public GameData(GameData a_other) : base(a_other)
        {
            lastValidationDate = a_other.lastValidationDate;
            scan = a_other.scan;
            descr = a_other.descr;
            placeType = a_other.placeType;
            placeDetails = a_other.placeDetails;
            placeName = a_other.placeName;
            placeReminder = a_other.placeReminder;
            ageLimit = a_other.ageLimit;
            minPlayerLimit = a_other.minPlayerLimit;
            maxPlayerLimit = a_other.maxPlayerLimit;
            duration = a_other.duration;
            customId = a_other.customId;
        }
        /*********************************************************/

        public GameData(int a_id, string a_path, string a_name = "") : base (a_id, a_path, a_name)
        {
            lastValidationDate = DateTime.Now;
            scan = string.Empty;
            descr = string.Empty;
            placeDetails = string.Empty;
            placeName = string.Empty;
            customId = string.Empty;
        }
        /*********************************************************/
    } // Class GameData
}