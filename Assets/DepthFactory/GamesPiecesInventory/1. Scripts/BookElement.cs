using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DepthFactory.Utils;

namespace DepthFactory.GamesPiecesInventory
{
    public class BookElement : MonoBehaviour
    {
        [SerializeField] TMP_Text _title;
        [SerializeField] Image _noReadImage;
        [SerializeField] Color _noReadColor;
        [SerializeField] TMP_Text _number;
        [SerializeField] MediaDisplay _imageDisplay;

        BookData _data;
        Action<BookData, BookElement> OnSelect;
        Color _normalColor;

        
    ///////////////////////////////////////////////////////////////
    /// PUBLIC FUNCTIONS //////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        public void Init(BookData a_data, Action<BookData, BookElement> a_selectAction)
        {
            _normalColor = SkinHelper.GetColor("main"); //_numberBackground.color;
            DisplayData(a_data);
            OnSelect = a_selectAction;
        }
        /*********************************************************/

        public void Btn_Select()
        {
            OnSelect?.Invoke(_data, this);
        }
        /*********************************************************/

        public void DisplayData(BookData a_data)
        {
            _data = a_data;
            _title.text = _data.name;
            _number.text = _data.volumeNumber.ToString();
            if (string.IsNullOrEmpty(_data.path) == false)
            {
                _imageDisplay.Init(MediaType.Type.Image, _data.path);
            }
            else
            {
                _imageDisplay.ResetDisplay();
            }

            _noReadImage.color = _data.isRead ? _normalColor : _noReadColor;
        }
        /*********************************************************/

        public override string ToString()
        {
            return "[BookElement] " + _data.name + " (" + _data.id + ") siblingIndex: " + transform.GetSiblingIndex();
        }
        /*********************************************************/
    } // Class BookElement


    public enum PlaceType { Tidy, Loaned, Rented }


    public class BookData
    {
        public int id;
        public int idCollection;
        public string name;
        public string path;
        public int pageNumber;
        public bool isRead;
        public string scan;
        public string isbn;
        public string descr;
        public PlaceType placeType;
        public string placeDetails;
        public string placeName;
        public DateTime placeReminder;
        public int volumeNumber;
        public string author;

        
    ///////////////////////////////////////////////////////////////
    /// CONSTRUCTORS //////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        public BookData()
        {
            id = -1;
            idCollection = -1;
            name = string.Empty;
            path = string.Empty;
            pageNumber = 0;
            isRead = false;
            scan = string.Empty;
            isbn = string.Empty;
            descr = string.Empty;
            placeType = PlaceType.Tidy;
            placeDetails = string.Empty;
            placeName = string.Empty;
            placeReminder = default;
            volumeNumber = -1;
            author = string.Empty;
        }
        /*********************************************************/

        public BookData(BookData a_other)
        {
            id = a_other.id;
            idCollection = a_other.idCollection;
            name = a_other.name;
            path = a_other.path;
            pageNumber = a_other.pageNumber;
            isRead = a_other.isRead;
            scan = a_other.scan;
            isbn = a_other.isbn;
            descr = a_other.descr;
            placeType = a_other.placeType;
            placeDetails = a_other.placeDetails;
            placeName = a_other.placeName;
            placeReminder = a_other.placeReminder;
            volumeNumber = a_other.volumeNumber;
            author = a_other.author;
        }
        /*********************************************************/

        public BookData(int a_id, int a_idGame, string a_name, string a_path, int a_pageNumber)
        {
            id = a_id;
            idCollection = a_idGame;
            name = a_name;
            path = a_path;
            pageNumber = a_pageNumber;
            scan = string.Empty;
            isbn = string.Empty;
            descr = string.Empty;
            placeDetails = string.Empty;
            placeName = string.Empty;
            author = string.Empty;
        }
        /*********************************************************/
    } // Class BookData


    public class ISBNData
	{
        public string title;
        public int pageNumber;
        public string path;
        

    ///////////////////////////////////////////////////////////////
    /// CONSTRUCTORS //////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        public ISBNData()
		{
            title = string.Empty;
            pageNumber = 0;
            path = string.Empty;
        }
        /*********************************************************/

        public ISBNData(string a_title, int a_number, string a_path = "")
		{
            title = a_title;
            pageNumber = a_number;
            path = a_path;
        }
        /*********************************************************/
    } // Class ISBNData
}
