using DepthFactory.Utils;
using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace DepthFactory.GamesPiecesInventory
{
    public abstract class A_FirstLevelManager : A_LevelManager
    {
        [SerializeField] protected TMP_Text _title;
        [SerializeField] protected TMP_InputField _titleIF;
        [SerializeField] protected RectTransform _contentRT;
        [SerializeField] protected GameObject _subObjectPrefab;
        [SerializeField] protected MediaDisplay _imageDisplay;

        public Action OnDelete;

        protected PopupManager _popupManager;
        protected string _mediaPath;

        protected const string POPUP_DELETE_MESSAGE = "Si vous confirmez, cet objet ({{ObjectName}}) sera définitivement supprimé.";

        
    ///////////////////////////////////////////////////////////////
    /// GENERAL FUNCTIONS /////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        protected override void Start()
        {
            base.Start();
            _popupManager = PopupManager.Instance;

            _mediaPath = System.IO.Directory.GetParent(Application.streamingAssetsPath).FullName + "/StreamingAssets/Medias/";
            System.IO.Directory.CreateDirectory(_mediaPath);
        }
        /*********************************************************/

    ///////////////////////////////////////////////////////////////
    /// PROTECTED FUNCTIONS ///////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        protected abstract void DisplaySubObjects();
        /*********************************************************/

        protected abstract void CB_AddSubObject();
        /*********************************************************/

        protected void ClearSubObjects()
        {
            foreach (Transform child in _contentRT)
                Destroy(child.gameObject);
        }
        /*********************************************************/

        protected abstract void CB_SetTitle();
        /*********************************************************/

        protected abstract void CB_SetImage();
        /*********************************************************/

        protected abstract void CB_AddPhoto(List<Texture2D> a_textureList);
        /*********************************************************/

        protected abstract void CB_UploadSelectedFile(string a_path);
        /*********************************************************/

        protected abstract void CB_Delete();
        /*********************************************************/

        protected virtual void CB_DeleteObject() { }
        /*********************************************************/

        protected abstract void OnConfirmDelete();
        /*********************************************************/
        
    ///////////////////////////////////////////////////////////////
    /// PUBLIC FUNCTIONS //////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        public abstract void UI_AddSubObject();
        /*********************************************************/

        public void UI_CleanSubObjects()
        {
            foreach (Transform child in _contentRT)
                Destroy(child.gameObject);
        }
        /*********************************************************/

        public abstract void UI_SetTitle(string a_value);
        /*********************************************************/

        public void UI_TakePhoto()
        {
            if (IsWrongType())
                return;
            if (Webcam.Instance != null)
                Webcam.Instance.InitiatePhotoShot(CB_AddPhoto);
        }
        /*********************************************************/

        public abstract void UI_BrowseImage();
        /*********************************************************/

        public void UI_Delete()
        {
            if (IsWrongType())
                return;
            _popupManager.Display(POPUP_DELETE_MESSAGE, OnConfirmDelete);
        }
        /*********************************************************/

        public void UI_DisplayTitleIF(bool a_showIF)
        {
            _titleIF.gameObject.SetActive(a_showIF);
            _title.enabled = !a_showIF;
        }
        /*********************************************************/
    }
}
