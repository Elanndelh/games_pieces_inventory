
using UnityEngine;

namespace DepthFactory.GamesPiecesInventory
{
    public class GPIConstants : Constants
    {
        //public static readonly string URL = "http://127.0.0.1/GamesPiecesInventory/";
        //public static readonly string MAIN_PHP = "http://127.0.0.1/GamesPiecesInventory/Main.php"; 
        public static string URL = string.Empty;
        public static string URL_MAIN_PHP = string.Empty;
        public static string URL_MEDIAS_PHP = string.Empty;
        public static string SETTINGS_PATH = string.Empty;
        public static readonly string VERSIONS_NOTES_FILE = "VersionsNotes";

        static readonly string MAIN_PHP = "Main.php";
        static readonly string MEDIAS_PHP = "medias.php";

        #region DEFAULT SETTINGS
        public static readonly int DEFAULT_ELEMENT_NUMBER_BY_PAGE = 10;
        public static readonly int DEFAULT_DAYS_BEFORE_VALIDATION = 365;
        public static readonly string DEFAULT_URL = "https://klaewyss.fr/gpi/";
        public static readonly bool DEFAULT_DISPLAY_BY_LINES = false;
        public static readonly int DEFAULT_INVENTORY_TYPE = 0;
        #endregion


        public new static void Init()
        {
            SETTINGS_PATH = Application.persistentDataPath + "/Settings.json";
            SKIN_PATH = Application.persistentDataPath + "/Skin.json";
        }
        /*********************************************************/

        public static void InitURL(string a_url)
        {
            URL = a_url;
            URL_MAIN_PHP = URL + "/" + MAIN_PHP;
            URL_MEDIAS_PHP = URL + "/" + MEDIAS_PHP;
        }
        /*********************************************************/
    }
}
