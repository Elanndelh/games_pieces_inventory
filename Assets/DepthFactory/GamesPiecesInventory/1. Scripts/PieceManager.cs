using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DepthFactory.Utils;
using SFB;

namespace DepthFactory.GamesPiecesInventory
{
    public class PieceManager : A_SecondLevelManager
    {
        [SerializeField] TMP_InputField _numberIF;
        [SerializeField] GameObject _visualNumberOrigGO;
        [SerializeField] GameObject _editNumberOrigGO;
        [SerializeField] TMP_Text _numberOrigText;
        [SerializeField] TMP_InputField _numberOrigIF;

        public Action<PieceData> OnUpdate;

        PieceData _data;

        
    ///////////////////////////////////////////////////////////////
    /// PRIVATE FUNCTIONS /////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        void CB_SetNumber()
        {
            _numberIF.text = _data.number.ToString();

			OnUpdate?.Invoke(_data);
		}
        /*********************************************************/

        protected override void CB_SetTitle()
        {
            _title.text = _data.name;
            _titleIF.text = _data.name;

            OnUpdate?.Invoke(_data);
        }
        /*********************************************************/

        void CB_SetImage()
        {
            if (string.IsNullOrEmpty(_data.path) == false)
            {
                _imageDisplay.Init(MediaType.Type.Image, _data.path);
            }
            else
            {
                _imageDisplay.ResetDisplay();
            }

			OnUpdate?.Invoke(_data);
		}
        /*********************************************************/

        /// <summary>
        /// Add photo to the server and form
        /// </summary>
        /// <param name="a_texture"></param>
        protected override void CB_AddPhoto(List<Texture2D> a_textureList)
        {
            if (a_textureList != null && a_textureList.Count > 0)
            {
                foreach (Texture2D texture in a_textureList)
                {
                    if (texture != null)
                        MediaManager.Instance.UploadFile(texture, GPIConstants.URL_MEDIAS_PHP, _data.path, CB_UploadSelectedFile);
                }
            }
        }
        /*********************************************************/

        void CB_UploadSelectedFile(string a_path)
		{
            if (a_path.StartsWith("Erreur:"))
			{
                Debug.LogError("erreur");
			}
			else
			{
                _data.path = a_path;
                _databaseManager.UpdatePiece(_data, CB_SetImage);
			}
		}
		/*********************************************************/

        void CB_Delete()
        {
            OnDelete?.Invoke();

            if (string.IsNullOrEmpty(_data.path) == false)
                MediaManager.Instance.DeleteFile(_data.path, GPIConstants.URL_MEDIAS_PHP, CB_DeleteImage);
        }
		/*********************************************************/

        void CB_DeleteImage() { }
        /*********************************************************/

        protected override void OnConfirmDelete()
        {
            _databaseManager.DeletePiece(_data, CB_Delete);
        }
        /*********************************************************/
        
    ///////////////////////////////////////////////////////////////
    /// PUBLIC FUNCTIONS //////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        public void Init(PieceData a_data)
        {
            _data = a_data;
            _title.text = _data.name;
            _titleIF.text = _data.name;
            UI_DisplayEditTitle(string.IsNullOrEmpty(_data.name));
            _numberIF.SetTextWithoutNotify(_data.number.ToString());
            _numberOrigText.text = _data.numberOrig.ToString();
            _numberOrigIF.SetTextWithoutNotify(_data.numberOrig.ToString());

            Btn_DisplayNumberOrigEditMode(_data.numberOrig == 0);

            if (string.IsNullOrEmpty(_data.path) == false)
            {
                _imageDisplay.Init(MediaType.Type.Image, _data.path);
            }
            else
            {
                _imageDisplay.ResetDisplay();
            }
        }
        /*********************************************************/

        public void Btn_SubstractNumber()
        {
            _data.number--;
            _databaseManager.UpdatePiece(_data, CB_SetNumber);
        }
        /*********************************************************/
        public void Btn_AddNumber()
        {
            _data.number++;
            _databaseManager.UpdatePiece(_data, CB_SetNumber);
        }
        /*********************************************************/
        public void Btn_SetNumber(string a_value)
        {
            if (_localizationManager.CurrentType != LocalizationType.GameFR)
                return;
            _data.number = int.Parse(a_value);
            _databaseManager.UpdatePiece(_data, CB_SetNumber);
        }
        /*********************************************************/
        public void Btn_SetNumberOrig(string a_value)
        {
            if (IsWrongType())
                return;
            _data.numberOrig = int.Parse(a_value);
            _databaseManager.UpdatePiece(_data, CB_SetNumber);
        }
        /*********************************************************/

        public void Btn_SetTitle(string a_value)
        {
            if (IsWrongType())
                return;
            _data.name = a_value;
            _databaseManager.UpdatePiece(_data, CB_SetTitle);
        }
        /*********************************************************/

        public void Btn_BrowseFile()
        {
            if (IsWrongType())
                return;
#if UNITY_STANDALONE_OSX || UNITY_STANDALONE_WIN || UNITY_EDITOR
            // Open file with filter
            ExtensionFilter[] extensions;
            extensions = new ExtensionFilter[]
            {
                new ExtensionFilter("Image Files", "png", "jpg", "jpeg", "PNG", "JPEG", "JPG" )
            };

            string[] paths = StandaloneFileBrowser.OpenFilePanel("Import media file", _mediaPath, extensions, false);
            if (paths != null && paths.Length > 0)
            {
                MediaManager.Instance.UploadFile(paths[0], GPIConstants.URL_MEDIAS_PHP, _data.path, CB_UploadSelectedFile);
            }
#elif UNITY_ANDROID || UNITY_IOS
            NativeGallery.Permission permission = NativeGallery.GetImageFromGallery((path) =>
            {
                Debug.Log("Image path: " + path);
                if (path != null)
                {
                    MediaManager.Instance.UploadFile(path, GPIConstants.URL_MEDIAS_PHP, _data.path, CB_UploadSelectedFile);
                }
            }, "Select a PNG image", "image/png");
#endif
        }
        /*********************************************************/

        public void Btn_DisplayNumberOrigEditMode(bool a_showEditMode)
        {
            _visualNumberOrigGO.SetActive(!a_showEditMode);
            _editNumberOrigGO.SetActive(a_showEditMode);
        }
        /*********************************************************/

        public override void UI_QuitPanel() 
        {
            Btn_DisplayNumberOrigEditMode(false);
        }
        /*********************************************************/
    }
}
