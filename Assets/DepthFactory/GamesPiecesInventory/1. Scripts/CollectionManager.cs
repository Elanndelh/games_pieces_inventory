using System;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using DepthFactory.UI;
using DepthFactory.Utils;
using SFB;

namespace DepthFactory.GamesPiecesInventory
{
    public class CollectionManager : A_FirstLevelManager
    {
        [SerializeField] Toggle _completeToggle;

        [Header("Edit Panel")]
        [SerializeField] DF_EditableField _descrField;
        [SerializeField] Toggle _descrToggle;
        //[SerializeField] TMP_InputField _scanIF;

        //[SerializeField] List<GameObject> _specificsGOs = new List<GameObject>();

        public Action<CollectionData> OnUpdate;
        public Action<BookElement, BookData> OnInsert;

        CollectionData _data;
        public CollectionData Data { get { return _data; } }
        Action<BookData, BookElement> OnSelect;

        
    ///////////////////////////////////////////////////////////////
    /// PROTECTED FUNCTIONS ///////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        protected override void DisplaySubObjects()
        {
            foreach (BookData bookData in _databaseManager.BooksData)
            {
                GameObject gameGO = Instantiate(_subObjectPrefab, _contentRT);
                gameGO.name = bookData.name;
                gameGO.GetComponent<BookElement>().Init(bookData, OnSelect);
            }
        }
        /*********************************************************/

        protected override void CB_AddSubObject()
        {
            BookData data = new BookData(_databaseManager.LastInsertedIndex, _data.id, string.Empty, string.Empty, 0);

            GameObject gameGO = Instantiate(_subObjectPrefab, _contentRT);
            gameGO.name = data.name;
            gameGO.GetComponent<BookElement>().Init(data, OnSelect);

            OnInsert?.Invoke(gameGO.GetComponent<BookElement>(), data);
        }
        /*********************************************************/

        protected override void CB_SetTitle()
        {
            _titleIF.text = _data.name;
            OnUpdate?.Invoke(_data);
        }
        /*********************************************************/

        void CB_SetDescr()
		{
            _descrField.SetText(_data.descr);
            OnUpdate?.Invoke(_data);
        }
        /*********************************************************/

        protected override void CB_SetImage()
        {
            if (string.IsNullOrEmpty(_data.path) == false)
            {
                _imageDisplay.Init(MediaType.Type.Image, _data.path);
            }
            else
            {
                _imageDisplay.ResetDisplay();
            }

            OnUpdate?.Invoke(_data);
        }
        /*********************************************************/

        /// <summary>
        /// Add photo to the server and form
        /// </summary>
        /// <param name="a_texture"></param>
        protected override void CB_AddPhoto(List<Texture2D> a_textureList)
        {
            if (a_textureList != null && a_textureList.Count > 0)
            {
                foreach (Texture2D texture in a_textureList)
                {
                    if (texture != null)
                        MediaManager.Instance.UploadFile(texture, GPIConstants.URL_MEDIAS_PHP, _data.path, CB_UploadSelectedFile);
                }
            }
        }
        /*********************************************************/

        protected override void CB_UploadSelectedFile(string a_path)
        {
            if (a_path.StartsWith("Erreur:"))
            {
                Debug.LogError("erreur");
            }
            else
            {
                _data.path = a_path;
                _databaseManager.UpdateCollection(_data, CB_SetImage);
            }
        }
        /*********************************************************/

        protected override void CB_Delete()
        {
            OnDelete?.Invoke();

            if (string.IsNullOrEmpty(_data.path) == false)
                MediaManager.Instance.DeleteFile(_data.path, GPIConstants.URL_MEDIAS_PHP, CB_DeleteObject);
        }
        /*********************************************************/

        protected override void OnConfirmDelete()
        {
            _databaseManager.DeleteCollection(_data, CB_Delete);
        }
        /*********************************************************/

    ///////////////////////////////////////////////////////////////
    /// PRIVATE FUNCTIONS /////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        void CB_SetIsComplete()
        {
            OnUpdate?.Invoke(_data);
        }
        /*********************************************************/

    ///////////////////////////////////////////////////////////////
    /// PUBLIC FUNCTIONS //////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        public void Init(CollectionData a_collectionData, Action<BookData, BookElement> a_selectPieceAction, bool a_newData = false)
        {
            UpdateData(a_collectionData);
            OnSelect = a_selectPieceAction;

            //ClearSubObjects();
            if (a_newData)
			{
                UI_DisplayTitleIF(true);
                _descrField.UI_DisplayEdition(true);
                _descrToggle.isOn = true;
            } else
            {
                _databaseManager.GetBooks(_data.id, DisplaySubObjects);
            }
        }
        /*********************************************************/

        public void UpdateData(CollectionData a_gameData)
        {
            _data = a_gameData;
            _title.text = _data.name;
            _titleIF.text = _data.name;
            _completeToggle.SetIsOnWithoutNotify(_data.complete);

            _descrField.SetText(_data.descr);

            if (string.IsNullOrEmpty(_data.path) == false)
            {
                _imageDisplay.Init(MediaType.Type.Image, _data.path);
            }
            else
            {
                _imageDisplay.ResetDisplay();
            }
        }
        /*********************************************************/

        public override void UI_AddSubObject()
        {
            if (IsWrongType())
                return;

            BookData data = new BookData();
            data.idCollection = _data.id;
            _databaseManager.AddBook(data, CB_AddSubObject);
        }
        /*********************************************************/

        public void UI_SetIsComplete(bool a_isComplete)
		{
            _data.complete = a_isComplete;
            _databaseManager.UpdateCollection(_data, CB_SetIsComplete);
        }
        /*********************************************************/
        public override void UI_SetTitle(string a_value)
        {
            if (IsWrongType())
                return;

            _data.name = a_value;
            _databaseManager.UpdateCollection(_data, CB_SetTitle);
        }
        /*********************************************************/

        public void UI_SetDesc(string a_value)
        {
            if (IsWrongType())
                return;

            _data.descr = a_value;
            _databaseManager.UpdateCollection(_data, CB_SetDescr);
        }
        /*********************************************************/

        public override void UI_BrowseImage()
        {
            if (IsWrongType())
                return;

#if UNITY_STANDALONE_OSX || UNITY_STANDALONE_WIN || UNITY_EDITOR
            // Open file with filter
            ExtensionFilter[] extensions;
            extensions = new ExtensionFilter[]
            {
                new ExtensionFilter("Image Files", "png", "jpg", "jpeg", "PNG", "JPEG", "JPG" )
            };

            string[] paths = StandaloneFileBrowser.OpenFilePanel("Import media file", _mediaPath, extensions, false);
            if (paths != null && paths.Length > 0)
            {
                MediaManager.Instance.UploadFile(paths[0], GPIConstants.URL_MEDIAS_PHP, _data.path, CB_UploadSelectedFile);
            }
#elif UNITY_ANDROID || UNITY_IOS
            NativeGallery.Permission permission = NativeGallery.GetImageFromGallery((path) =>
            {
                Debug.Log("Image path: " + path);
                if (path != null)
                {
                    MediaManager.Instance.UploadFile(path, GPIConstants.URL_MEDIAS_PHP, _data.path, CB_UploadSelectedFile);
                }
            }, "Select a PNG image", "image/png");
#endif
        }
        /*********************************************************/

        public override void UI_QuitPanel()
        {
            UI_CleanSubObjects();
            _descrField.UI_DisplayEdition(false);
            UI_DisplayTitleIF(false);
        }
        /*********************************************************/
    }
}
