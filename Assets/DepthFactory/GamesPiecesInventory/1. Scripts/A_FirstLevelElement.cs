using System;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DepthFactory.Utils;

namespace DepthFactory.GamesPiecesInventory
{
    public class A_FirstLevelElement : MonoBehaviour
    {
        [SerializeField] protected MediaDisplay _imageDisplay;
        [SerializeField] protected TMP_Text _title;

        protected A_Data _data;
        //protected Action<A_Data, A_FirstLevelElement> OnSelect;


    ///////////////////////////////////////////////////////////////
    /// PUBLIC FUNCTIONS //////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        public void Init(A_Data a_data, Action<A_Data, A_FirstLevelElement> a_selectAction)
        {
            DisplayData(a_data);
            //OnSelect = a_selectAction;
        }
        /*********************************************************/

        public void DisplayData(A_Data a_data)
        {
            _data = a_data;

            if (_imageDisplay != null)
            {
                if (string.IsNullOrEmpty(_data.path) == false)
                {
                    _imageDisplay.Init(MediaType.Type.Image, _data.path);
                }
                else
                {
                    _imageDisplay.ResetDisplay();
                }
            }
            _title.text = _data.name;
        }
        /*********************************************************/

        /*public void Btn_Select()
        {
            OnSelect?.Invoke(_data, this);
        }
        /*********************************************************/
        public override string ToString()
        {
            return "[A_FirstLevelElement] " + _data.name + " (" + _data.id + ") siblingIndex: " + transform.GetSiblingIndex();
        }
        /*********************************************************/
    } // Class A_FirstLevelElement


    public abstract class A_Data
    {
        public int id;
        public string name;
        public string path;
        
    ///////////////////////////////////////////////////////////////
    /// CONSTRUCTORS //////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        public A_Data()
        {
            id = -1;
            name = string.Empty;
            path = string.Empty;
        }
        /*********************************************************/

        public A_Data(A_Data a_other)
        {
            id = a_other.id;
            name = a_other.name;
            path = a_other.path;
        }
        /*********************************************************/

        public A_Data(int a_id, string a_path, string a_name = "")
        {
            id = a_id;
            name = a_name;
            path = a_path;
        }
        /*********************************************************/
    } // Class A_Data
}