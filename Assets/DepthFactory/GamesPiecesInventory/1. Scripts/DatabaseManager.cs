﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using DepthFactory.Utils;
using System.Text;

namespace DepthFactory.GamesPiecesInventory
{
    public class DatabaseManager : MonoBehaviour
    {
        static DatabaseManager _instance;
        public static DatabaseManager Instance { get { return _instance; } }


        [SerializeField] GameObject _loadingGO;

        public string URL { get; set; }
        public int DaysBeforeValidation { get; set; }

        bool _unvalidatedGames;
        public bool UnvalidatedGames { get { return _unvalidatedGames; } set { _unvalidatedGames = value; } }

        string _search;
        public string Search { get { return _search; } set { _search = value; } }

        string _subSearch;
        public string SubSearch { get { return _subSearch; } set { _subSearch = value; } }

        int _age;
        public int Age { get { return _age; } set { _age = value; } }

        int _playerNumber;
        public int PlayerNumber { get { return _playerNumber; } set { _playerNumber = value; } }

        int _duration;
        public int Duration { get { return _duration; } set { _duration = value; } }

        public int NumberOfFirstLevelElements { get
			{
				return LocalizationManager.Instance.CurrentType switch
				{
					LocalizationType.GameFR => _numberOfGames,
					LocalizationType.BookFR => _numberOfCollections,
					_ => 0,
				};
			} 
        }

        int _numberOfGames;
        [SerializeField] List<GameData> _gamesData;
        public List<GameData> GamesData { get { return _gamesData; } }
        [SerializeField] List<PieceData> _piecesData;
        public List<PieceData> PiecesData { get { return _piecesData; } }
        int _lastInsertedIndex;
        public int LastInsertedIndex { get { return _lastInsertedIndex; } }

        int _numberOfCollections;
        public List<CollectionData> CollectionsData { get { return _collectionsData; } }
        [SerializeField] List<CollectionData> _collectionsData;
        public List<BookData> BooksData { get { return _booksData; } }
        [SerializeField] List<BookData> _booksData;
        public ISBNData ISBNData { get { return _isbnData; } }
        [SerializeField] ISBNData _isbnData;

        string _error;
        PopupManager _popup;

        const char CHAR_SEPARATOR = '¤';
        

    /////////////////////////////////////////////////////////////// 
    /// GENERAL FUNCTIONS ///////////////////////////////////////// 
    /////////////////////////////////////////////////////////////// 
        void Awake()
        {
            _instance = this;
            _gamesData = new List<GameData>();
            _piecesData = new List<PieceData>();
            _collectionsData = new List<CollectionData>();
            _booksData = new List<BookData>();
            _popup = PopupManager.Instance;
            _error = string.Empty;
        }
        /*********************************************************/

    /////////////////////////////////////////////////////////////// 
    /// PRIVATE FUNCTIONS ///////////////////////////////////////// 
    /////////////////////////////////////////////////////////////// 
        IEnumerator GetGamesCoroutine(int a_begin, int a_elementsNumber, Action a_callback)
        {
            _loadingGO.SetActive(true);
            PHPForm form = new PHPForm();

            form.AddField("action", "getGames");
            form.AddField("begin", a_begin);
            form.AddField("elementsNumber", a_elementsNumber);
            if (_unvalidatedGames)
                form.AddField("unvalidatedDate", DateTime.Now.AddDays(-DaysBeforeValidation).ToString("yyyy-MM-dd"));
            if (string.IsNullOrEmpty(_search) == false)
                form.AddField("search", _search);
            if (_age > 0)
                form.AddField("age", _age);
            if (_playerNumber > 0)
                form.AddField("playerNumber", _playerNumber);
            if (_duration > 0)
                form.AddField("duration", _duration);
            WWW request = new WWW(GPIConstants.URL_MAIN_PHP, form.CreateForm());
            yield return request;

            _gamesData.Clear();

            if (string.IsNullOrEmpty(request.error) == false)
                DisplayError("[GetGamesCoroutine] Error: " + request.error);

            string gamesDataStr = request.text;
            if (string.IsNullOrEmpty(gamesDataStr) == false)
            {
                gamesDataStr = gamesDataStr.Split('[')[1];
                gamesDataStr = gamesDataStr.Split(']')[gamesDataStr.Split(']').Length - 2];
                string[] tasksStr = gamesDataStr.Split('{');
                for (int i = 0; i < tasksStr.Length - 1; i++)
                {
                    GameData newGame = new GameData();
                    string[] tasksAttributesStr = tasksStr[i + 1].Split('|');

                    // Identifiant
                    string id = tasksAttributesStr[0].Split(CHAR_SEPARATOR)[1];
                    int.TryParse(id, out newGame.id);

                    // Title
                    string name = tasksAttributesStr[1].Split(CHAR_SEPARATOR)[1];
                    newGame.name = name;

                    // Path
                    string path = tasksAttributesStr[2].Split(CHAR_SEPARATOR)[1];
                    newGame.path = path;

                    // Last validation date
                    string date = tasksAttributesStr[3].Split(CHAR_SEPARATOR)[1];
                    DateTime.TryParse(date, out newGame.lastValidationDate);

                    // Scan
                    string scan = tasksAttributesStr[4].Split(CHAR_SEPARATOR)[1];
                    //scan = scan.Split('}')[0];
                    newGame.scan = scan;

                    // Scan
                    string value = tasksAttributesStr[5].Split(CHAR_SEPARATOR)[1];
                    //scan = scan.Split('}')[0];
                    newGame.descr = value;

                    // PlaceType
                    value = tasksAttributesStr[6].Split(CHAR_SEPARATOR)[1];
                    int.TryParse(value, out int valueInt);
                    newGame.placeType = (PlaceType)valueInt;

                    // PlaceDetails
                    value = tasksAttributesStr[7].Split(CHAR_SEPARATOR)[1];
                    newGame.placeDetails = value;

                    // PlaceName
                    value = tasksAttributesStr[8].Split(CHAR_SEPARATOR)[1];
                    newGame.placeName = value;

                    // PlaceReminder
                    value = tasksAttributesStr[9].Split(CHAR_SEPARATOR)[1];
                    //value = value.Split('}')[0];
                    DateTime.TryParse(value, out newGame.placeReminder);

                    // Age Limit
                    value = tasksAttributesStr[10].Split(CHAR_SEPARATOR)[1];
                    int.TryParse(value, out newGame.ageLimit);

                    // Min Player Limit
                    value = tasksAttributesStr[11].Split(CHAR_SEPARATOR)[1];
                    int.TryParse(value, out newGame.minPlayerLimit);

                    // Max Player Limit
                    value = tasksAttributesStr[12].Split(CHAR_SEPARATOR)[1];
                    int.TryParse(value, out newGame.maxPlayerLimit);

                    // Duration
                    value = tasksAttributesStr[13].Split(CHAR_SEPARATOR)[1];
                    //value = value.Split('}')[0];
                    int.TryParse(value, out newGame.duration);

                    // Custom Id
                    value = tasksAttributesStr[14].Split(CHAR_SEPARATOR)[1];
                    value = value.Split('}')[0];
                    newGame.customId = value;

                    _gamesData.Add(newGame);
                }
            }
            _loadingGO.SetActive(false);

            a_callback?.Invoke();
        }
        /*********************************************************/
        IEnumerator GetGamesNumberCoroutine(Action a_callback)
        {
            _loadingGO.SetActive(true);
            PHPForm form = new PHPForm();
            form.AddField("action", "getGamesNumber");
            if (_unvalidatedGames)
                form.AddField("unvalidatedDate", DateTime.Now.AddDays(-DaysBeforeValidation).ToString("yyyy-MM-dd"));
            if (string.IsNullOrEmpty(_search) == false)
                form.AddField("search", _search);
            if (_age > 0)
                form.AddField("age", _age);
            if (_playerNumber > 0)
                form.AddField("playerNumber", _playerNumber);
            if (_duration > 0)
                form.AddField("duration", _duration);
            WWW request = new WWW(GPIConstants.URL_MAIN_PHP, form.CreateForm());
            yield return request;

            if (string.IsNullOrEmpty(request.error) == false)
                DisplayError("[GetGamesNumberCoroutine] Error: " + request.error);

            int.TryParse(request.text, out _numberOfGames);

            _loadingGO.SetActive(false);
            a_callback?.Invoke();
        }
        /*********************************************************/
        IEnumerator GetPiecesCoroutine(int a_idGame, Action a_callback)
        {
            _loadingGO.SetActive(true);
            PHPForm form = new PHPForm();
            form.AddField("action", "getPieces");
            form.AddField("idGame", a_idGame);
            WWW request = new WWW(GPIConstants.URL_MAIN_PHP, form.CreateForm());
            yield return request;

            _piecesData.Clear();
            
            if (string.IsNullOrEmpty(request.error) == false)
                DisplayError("[GetPiecesCoroutine] Error: " + request.error);

            string piecesDataStr = request.text;
            if (string.IsNullOrEmpty(piecesDataStr) == false)
            {
                piecesDataStr = piecesDataStr.Split('[')[1];
                piecesDataStr = piecesDataStr.Split(']')[piecesDataStr.Split(']').Length-2];
                string[] piecesStr = piecesDataStr.Split('{');
                for (int i = 0; i < piecesStr.Length-1; i++)
                {
                    PieceData newPiece = new PieceData();
                    string[] tasksAttributesStr = piecesStr[i + 1].Split('|');

                    // Identifiant
                    string id = tasksAttributesStr[0].Split(CHAR_SEPARATOR)[1];
                    int.TryParse(id, out newPiece.id);

                    // Name
                    string name = tasksAttributesStr[2].Split(CHAR_SEPARATOR)[1];
                    newPiece.name = name;

                    // Path
                    string path = tasksAttributesStr[3].Split(CHAR_SEPARATOR)[1];
                    newPiece.path = path;

                    // Number
                    string number = tasksAttributesStr[4].Split(CHAR_SEPARATOR)[1];
                    int.TryParse(number, out newPiece.number);

                    // Number Orig
                    string numberOrig = tasksAttributesStr[5].Split(CHAR_SEPARATOR)[1];
                    numberOrig = numberOrig.Split('}')[0];
                    int.TryParse(numberOrig, out newPiece.numberOrig);

                    _piecesData.Add(newPiece);
                }

                a_callback?.Invoke();
            }
            _loadingGO.SetActive(false);
        }
        /*********************************************************/

        IEnumerator AddGameCoroutine(GameData a_data, Action a_callback)
        {
            _loadingGO.SetActive(true);
            PHPForm form = new PHPForm();
            form.AddField("action", "addGame");
            form.AddField("name", a_data.name);
            form.AddField("path", a_data.path);
            form.AddField("lastValidationDate", a_data.lastValidationDate.ToString("yyyy-MM-dd"));
            form.AddField("scan", a_data.scan);
            form.AddField("descr", a_data.descr);
            form.AddField("placeType", (int)a_data.placeType);
            form.AddField("placeDetails", a_data.placeDetails);
            form.AddField("placeName", a_data.placeName);
            form.AddField("placeReminder", a_data.placeReminder.ToString("yyyy-MM-dd"));
            form.AddField("ageLimit", a_data.ageLimit);
            form.AddField("minPlayerLimit", a_data.minPlayerLimit);
            form.AddField("maxPlayerLimit", a_data.maxPlayerLimit);
            form.AddField("duration", a_data.duration);
            form.AddField("customId", a_data.customId);
            WWW request = new WWW(GPIConstants.URL_MAIN_PHP, form.CreateForm());
            yield return request;

            _lastInsertedIndex = int.Parse(request.text);

            if (string.IsNullOrEmpty(request.error) == false)
                DisplayError("[AddGameCoroutine] Error: " + request.error);

            _loadingGO.SetActive(false);
            a_callback?.Invoke();
        }
        /*********************************************************/
        IEnumerator AddPieceCoroutine(PieceData a_data, Action a_callback)
        {
            _loadingGO.SetActive(true);
            PHPForm form = new PHPForm();
            form.AddField("action", "addPiece");
            form.AddField("idGame", a_data.idGame);
            form.AddField("name", a_data.name);
            form.AddField("path", a_data.path);
            form.AddField("number", a_data.number);
            form.AddField("numberOrig", a_data.numberOrig);
            WWW request = new WWW(GPIConstants.URL_MAIN_PHP, form.CreateForm());
            yield return request;

            if (string.IsNullOrEmpty(request.error) == false)
                DisplayError("[AddPieceCoroutine] Error: " + request.error);

            _lastInsertedIndex = int.Parse(request.text);

            _loadingGO.SetActive(false);
            a_callback?.Invoke();
        }
        /*********************************************************/

        IEnumerator UpdateGameCoroutine(GameData a_data, Action a_callback)
        {
            _loadingGO.SetActive(true);
            PHPForm form = new PHPForm();
            form.AddField("action", "updateGame");
            form.AddField("id", a_data.id);
            form.AddField("name", a_data.name);
            form.AddField("path", a_data.path);
            form.AddField("lastValidationDate", a_data.lastValidationDate.ToString("yyyy-MM-dd"));
            form.AddField("scan", a_data.scan);
            form.AddField("descr", a_data.descr);
            form.AddField("placeType", (int)a_data.placeType);
            form.AddField("placeDetails", a_data.placeDetails);
            form.AddField("placeName", a_data.placeName);
            form.AddField("placeReminder", a_data.placeReminder.ToString("yyyy-MM-dd"));
            form.AddField("ageLimit", a_data.ageLimit);
            form.AddField("minPlayerLimit", a_data.minPlayerLimit);
            form.AddField("maxPlayerLimit", a_data.maxPlayerLimit);
            form.AddField("duration", a_data.duration);
            form.AddField("customId", a_data.customId);
            WWW request = new WWW(GPIConstants.URL_MAIN_PHP, form.CreateForm());
            yield return request;
            
            if (string.IsNullOrEmpty(request.error) == false)
                DisplayError("[UpdateGameCoroutine] Error: " + request.error);

            _loadingGO.SetActive(false);
            a_callback?.Invoke();
        }
        /*********************************************************/
        IEnumerator UpdatePieceCoroutine(PieceData a_data, Action a_callback)
        {
            _loadingGO.SetActive(true);
            PHPForm form = new PHPForm();
            form.AddField("action", "updatePiece");
            form.AddField("id", a_data.id);
            form.AddField("name", a_data.name);
            form.AddField("path", a_data.path);
            form.AddField("number", a_data.number);
            form.AddField("numberOrig", a_data.numberOrig);
            WWW request = new WWW(GPIConstants.URL_MAIN_PHP, form.CreateForm());
            yield return request;

            if (string.IsNullOrEmpty(request.error) == false)
                DisplayError("[UpdatePieceCoroutine] Error: " + request.error);

            _loadingGO.SetActive(false);
            a_callback?.Invoke();
        }
        /*********************************************************/

        IEnumerator DeleteGameCoroutine(GameData a_data, Action a_callback)
        {
            _loadingGO.SetActive(true);
            PHPForm form = new PHPForm();
            form.AddField("action", "deleteGame");
            form.AddField("id", a_data.id);

            WWW request = new WWW(GPIConstants.URL_MAIN_PHP, form.CreateForm());
            yield return request;

            if (string.IsNullOrEmpty(request.error) == false)
                DisplayError("[DeleteGameCoroutine] Error: " + request.error);

            _loadingGO.SetActive(false);
            a_callback?.Invoke();
        }
        /*********************************************************/
        IEnumerator DeletePieceCoroutine(PieceData a_data, Action a_callback)
        {
            _loadingGO.SetActive(true);
            PHPForm form = new PHPForm();
            form.AddField("action", "deletePiece");
            form.AddField("id", a_data.id);

            WWW request = new WWW(GPIConstants.URL_MAIN_PHP, form.CreateForm());
            yield return request;

            if (string.IsNullOrEmpty(request.error) == false)
                DisplayError("[DeletePieceCoroutine] Error: " + request.error);

            _loadingGO.SetActive(false);
            a_callback?.Invoke();
        }
        /*********************************************************/

        IEnumerator GetCollectionsNumberCoroutine(Action a_callback)
        {
            _loadingGO.SetActive(true);
            PHPForm form = new PHPForm();
            form.AddField("action", "getCollectionsNumber");
            if (string.IsNullOrEmpty(_search) == false)
                form.AddField("search", _search);
            if (string.IsNullOrEmpty(_subSearch) == false)
                form.AddField("subSearch", _subSearch);
            WWW request = new WWW(GPIConstants.URL_MAIN_PHP, form.CreateForm());
            yield return request;

            if (string.IsNullOrEmpty(request.error) == false)
                DisplayError("[GetCollectionsNumberCoroutine] Error: " + request.error);

            Debug.Log(request.text);
            int.TryParse(request.text, out _numberOfCollections);

            _loadingGO.SetActive(false);
            a_callback?.Invoke();
        }
        /*********************************************************/
        IEnumerator GetCollectionsCoroutine(int a_begin, int a_elementsNumber, Action a_callback)
        {
            _loadingGO.SetActive(true);
            PHPForm form = new PHPForm();

            form.AddField("action", "getCollections");
            form.AddField("begin", a_begin);
            form.AddField("elementsNumber", a_elementsNumber);
            if (string.IsNullOrEmpty(_search) == false)
                form.AddField("search", _search);
            if (string.IsNullOrEmpty(_subSearch) == false)
                form.AddField("subSearch", _subSearch);
            WWW request = new WWW(GPIConstants.URL_MAIN_PHP, form.CreateForm());
            yield return request;

            _collectionsData.Clear();

            if (string.IsNullOrEmpty(request.error) == false)
                DisplayError("[GetCollectionsCoroutine] Error: " + request.error);

            string dataStr = request.text;
            Debug.Log(dataStr);
            if (string.IsNullOrEmpty(dataStr) == false)
            {
                dataStr = dataStr.Split('[')[1];
                dataStr = dataStr.Split(']')[dataStr.Split(']').Length - 2];
                string[] tasksStr = dataStr.Split('{');
                for (int i = 0; i < tasksStr.Length - 1; i++)
                {
                    CollectionData newElement = new CollectionData();
                    string[] tasksAttributesStr = tasksStr[i + 1].Split('|');

                    // Identifiant
                    string id = tasksAttributesStr[0].Split(CHAR_SEPARATOR)[1];
                    int.TryParse(id, out newElement.id);

                    // Title
                    string name = tasksAttributesStr[1].Split(CHAR_SEPARATOR)[1];
                    newElement.name = name;

                    // Path
                    string path = tasksAttributesStr[2].Split(CHAR_SEPARATOR)[1];
                    newElement.path = path;

                    // Descr
                    string descr = tasksAttributesStr[3].Split(CHAR_SEPARATOR)[1];
                    newElement.descr = descr;

                    // Complete
                    string complete = tasksAttributesStr[4].Split(CHAR_SEPARATOR)[1];
                    complete = complete.Split('}')[0];
                    newElement.complete = complete == "1";

                    _collectionsData.Add(newElement);
                }
            }
            _loadingGO.SetActive(false);

            a_callback?.Invoke();
        }
        /*********************************************************/
        IEnumerator AddCollectionCoroutine(CollectionData a_data, Action a_callback)
        {
            _loadingGO.SetActive(true);
            PHPForm form = new PHPForm();
            form.AddField("action", "addCollection");
            form.AddField("name", a_data.name);
            form.AddField("path", a_data.path);
            form.AddField("descr", a_data.descr);
            form.AddField("complete", a_data.complete ? "1" : "0");
            WWW request = new WWW(GPIConstants.URL_MAIN_PHP, form.CreateForm());
            yield return request;

            _lastInsertedIndex = int.Parse(request.text);

            if (string.IsNullOrEmpty(request.error) == false)
                DisplayError("[AddCollectionCoroutine] Error: " + request.error);

            _loadingGO.SetActive(false);
            a_callback?.Invoke();
        }
        /*********************************************************/
        IEnumerator UpdateCollectionCoroutine(CollectionData a_data, Action a_callback)
        {
            _loadingGO.SetActive(true);
            PHPForm form = new PHPForm();
            form.AddField("action", "updateCollection");
            form.AddField("id", a_data.id);
            form.AddField("name", a_data.name);
            form.AddField("path", a_data.path);
            form.AddField("descr", a_data.descr);
            form.AddField("complete", a_data.complete ? "1" : "0");
            WWW request = new WWW(GPIConstants.URL_MAIN_PHP, form.CreateForm());
            yield return request;

            if (string.IsNullOrEmpty(request.error) == false)
                DisplayError("[UpdateCollectionCoroutine] Error: " + request.error);

            _loadingGO.SetActive(false);
            a_callback?.Invoke();
        }
        /*********************************************************/
        IEnumerator DeleteCollectionCoroutine(CollectionData a_data, Action a_callback)
        {
            _loadingGO.SetActive(true);
            PHPForm form = new PHPForm();
            form.AddField("action", "deleteCollection");
            form.AddField("id", a_data.id);

            WWW request = new WWW(GPIConstants.URL_MAIN_PHP, form.CreateForm());
            yield return request;

            if (string.IsNullOrEmpty(request.error) == false)
                DisplayError("[DeleteCollectionCoroutine] Error: " + request.error);

            _loadingGO.SetActive(false);
            a_callback?.Invoke();
        }
        /*********************************************************/

        IEnumerator GetBooksCoroutine(int a_idCollection, Action a_callback)
        {
            _loadingGO.SetActive(true);
            PHPForm form = new PHPForm();
            form.AddField("action", "getBooks");
            form.AddField("idCollection", a_idCollection);
            if (string.IsNullOrEmpty(_subSearch) == false)
                form.AddField("subSearch", _subSearch);
            WWW request = new WWW(GPIConstants.URL_MAIN_PHP, form.CreateForm());
            yield return request;

            _booksData.Clear();

            if (string.IsNullOrEmpty(request.error) == false)
                DisplayError("[GetBooksCoroutine] Error: " + request.error);

            string dataStr = request.text;
            if (string.IsNullOrEmpty(dataStr) == false)
            {
                dataStr = dataStr.Split('[')[1];
                dataStr = dataStr.Split(']')[dataStr.Split(']').Length - 2];
                string[] taskStr = dataStr.Split('{');
                for (int i = 0; i < taskStr.Length - 1; i++)
                {
                    BookData newElement = new BookData();
                    string[] tasksAttributesStr = taskStr[i + 1].Split('|');

                    // Identifiant
                    string id = tasksAttributesStr[0].Split(CHAR_SEPARATOR)[1];
                    int.TryParse(id, out newElement.id);

                    // Name
                    string name = tasksAttributesStr[2].Split(CHAR_SEPARATOR)[1];
                    newElement.name = name;

                    // Path
                    string path = tasksAttributesStr[3].Split(CHAR_SEPARATOR)[1];
                    newElement.path = path;

                    // Number
                    string pageNumber = tasksAttributesStr[4].Split(CHAR_SEPARATOR)[1];
                    int.TryParse(pageNumber, out newElement.pageNumber);

                    // Is Read
                    string isRead = tasksAttributesStr[5].Split(CHAR_SEPARATOR)[1];
                    newElement.isRead = isRead == "1";

                    // Scan
                    string scan = tasksAttributesStr[6].Split(CHAR_SEPARATOR)[1];
                    newElement.scan = scan;

                    // ISBN
                    string isbn = tasksAttributesStr[7].Split(CHAR_SEPARATOR)[1];
                    newElement.isbn = isbn;

                    // Descr
                    string descr = tasksAttributesStr[8].Split(CHAR_SEPARATOR)[1];
                    //descr = descr.Split('}')[0];
                    newElement.descr = descr;

                    // PlaceType
                    string value = tasksAttributesStr[9].Split(CHAR_SEPARATOR)[1];
					int.TryParse(value, out int valueInt);
					newElement.placeType = (PlaceType)valueInt;

                    // PlaceDetails
                    value = tasksAttributesStr[10].Split(CHAR_SEPARATOR)[1];
                    newElement.placeDetails = value;

                    // PlaceName
                    value = tasksAttributesStr[11].Split(CHAR_SEPARATOR)[1];

                    // PlaceReminder
                    value = tasksAttributesStr[12].Split(CHAR_SEPARATOR)[1];
                    DateTime.TryParse(value, out newElement.placeReminder);

                    // Volume Orig
                    value = tasksAttributesStr[13].Split(CHAR_SEPARATOR)[1];
                    int.TryParse(value, out newElement.volumeNumber);

                    // Volume Orig
                    value = tasksAttributesStr[14].Split(CHAR_SEPARATOR)[1];
                    value = value.Split('}')[0];
                    newElement.author = value;

                    _booksData.Add(newElement);
                }

                a_callback?.Invoke();
            }
            _loadingGO.SetActive(false);
        }
        /*********************************************************/
        IEnumerator AddBookCoroutine(BookData a_data, Action a_callback)
        {
            _loadingGO.SetActive(true);
            PHPForm form = new PHPForm();
            form.AddField("action", "addBook");
            form.AddField("idCollection", a_data.idCollection);
            form.AddField("name", a_data.name);
            form.AddField("path", a_data.path);
            form.AddField("pageNumber", a_data.pageNumber);
            form.AddField("isRead", a_data.isRead ? "1" : "0");
            form.AddField("scan", a_data.scan);
            form.AddField("isbn", a_data.isbn);
            form.AddField("descr", a_data.descr);
            form.AddField("placeType", (int)a_data.placeType);
            form.AddField("placeDetails", a_data.placeDetails);
            form.AddField("placeName", a_data.placeName);
            form.AddField("placeReminder", a_data.placeReminder.ToString("yyyy-MM-dd"));
            form.AddField("volumeNumber", a_data.volumeNumber);
            form.AddField("author", a_data.author);
            WWW request = new WWW(GPIConstants.URL_MAIN_PHP, form.CreateForm());
            yield return request;

            if (string.IsNullOrEmpty(request.error) == false)
                DisplayError("[AddBookCoroutine] Error: " + request.error);

            _lastInsertedIndex = int.Parse(request.text);

            _loadingGO.SetActive(false);
            a_callback?.Invoke();
        }
        /*********************************************************/
        IEnumerator UpdateBookCoroutine(BookData a_data, Action a_callback)
        {
            _loadingGO.SetActive(true);
            PHPForm form = new PHPForm();
            form.AddField("action", "updateBook");
            form.AddField("id", a_data.id);
            form.AddField("name", a_data.name);
            form.AddField("path", a_data.path);
            form.AddField("pageNumber", a_data.pageNumber);
            form.AddField("isRead", a_data.isRead ? "1" : "0");
            form.AddField("scan", a_data.scan);
            form.AddField("isbn", a_data.isbn);
            form.AddField("descr", a_data.descr);
            form.AddField("placeType", (int)a_data.placeType);
            form.AddField("placeDetails", a_data.placeDetails);
            form.AddField("placeName", a_data.placeName);
            form.AddField("placeReminder", a_data.placeReminder.ToString("yyyy-MM-dd"));
            form.AddField("volumeNumber", a_data.volumeNumber);
            form.AddField("author", a_data.author);
            WWW request = new WWW(GPIConstants.URL_MAIN_PHP, form.CreateForm());
            yield return request;

            if (string.IsNullOrEmpty(request.error) == false)
                DisplayError("[UpdateBookCoroutine] Error: " + request.error);

            _loadingGO.SetActive(false);
            a_callback?.Invoke();
        }
        /*********************************************************/
        IEnumerator DeleteBookCoroutine(BookData a_data, Action a_callback)
        {
            _loadingGO.SetActive(true);
            PHPForm form = new PHPForm();
            form.AddField("action", "deleteBook");
            form.AddField("id", a_data.id);

            WWW request = new WWW(GPIConstants.URL_MAIN_PHP, form.CreateForm());
            yield return request;

            if (string.IsNullOrEmpty(request.error) == false)
                DisplayError("[DeleteBookCoroutine] Error: " + request.error);

            _loadingGO.SetActive(false);
            a_callback?.Invoke();
        }
        /*********************************************************/

        IEnumerator GetISBNDataCoroutine(string a_isbn, Action<ISBNData> a_callback)
        {
            _loadingGO.SetActive(true);
            string url = "https://openlibrary.org/api/books?bibkeys=ISBN:" + a_isbn + "&jscmd=data&format=json";
            Debug.Log(url);
            WWW request = new WWW(url);
            yield return request;

            _isbnData = new ISBNData();

            if (string.IsNullOrEmpty(request.error) == false)
                DisplayError("[TestISBNCoroutine] Error: " + request.error);

            string dataStr = DecodeEncodedNonAsciiCharacters(request.text);
            Debug.Log(dataStr);
            if (string.IsNullOrEmpty(dataStr) == false)
            {
                dataStr = dataStr.Remove(0, dataStr.Split('{')[1].Length + 1);
                Debug.Log(dataStr);
                string[] taskStr = dataStr.Split(',');
                for (int i = 0; i < taskStr.Length - 1; i++)
                {
                    //Debug.Log(taskStr[i + 1].Split(':')[0]);
                    if (taskStr[i + 1].Split(':')[0].Contains("title"))
                    {
                        _isbnData.title = taskStr[i + 1].Split(':')[1].Replace("\"", "");
                        Debug.Log("title: " + _isbnData.title);
                    }
                    else if (taskStr[i + 1].Split(':')[0].Contains("number_of_pages"))
                    {
                        string value = taskStr[i + 1].Split(':')[1].Replace("\"", "");
                        int.TryParse(value, out _isbnData.pageNumber);
                        Debug.Log("pages: " + value);
                    }
                    else if (taskStr[i + 1].Split(':')[0].Contains("large"))
                    {
                        _isbnData.path = (taskStr[i + 1].Split(':')[1] + ":" + taskStr[i + 1].Split(':')[2]).Replace("\"", "").Replace("}", "");
                        Debug.Log("cover: " + _isbnData.path);
                    }
                }
            }

            _loadingGO.SetActive(false);
            a_callback?.Invoke(_isbnData);
        }
        /*********************************************************/

        static string DecodeEncodedNonAsciiCharacters(string value)
        {
            return System.Text.RegularExpressions.Regex.Replace(
                value,
                @"\\u(?<Value>[a-fA-F0-9]{4})",
                m => {
                    return ((char)int.Parse(m.Groups["Value"].Value, System.Globalization.NumberStyles.HexNumber)).ToString();
                });
        }
        /*********************************************************/

        IEnumerator TestISBNCoroutine()
		{
            WWW request = new WWW("https://openlibrary.org/api/books?bibkeys=ISBN:9782748517378&jscmd=data&format=json");
            yield return request;

            if (string.IsNullOrEmpty(request.error) == false)
                DisplayError("[TestISBNCoroutine] Error: " + request.error);

            string dataStr = DecodeEncodedNonAsciiCharacters(request.text);
            Debug.Log(dataStr);
            if (string.IsNullOrEmpty(dataStr) == false)
            {
                dataStr = dataStr.Remove(0, dataStr.Split('{')[1].Length + 1);
                Debug.Log(dataStr);
                string[] taskStr = dataStr.Split(',');
                for (int i = 0; i < taskStr.Length - 1; i++)
                {
                    //Debug.Log(taskStr[i + 1].Split(':')[0]);
                    if (taskStr[i + 1].Split(':')[0].Contains("title"))
					{
                        Debug.Log("title: " + taskStr[i + 1].Split(':')[1].Replace("\"", ""));
					}
                    else if (taskStr[i + 1].Split(':')[0].Contains("number_of_pages"))
                    {
                        Debug.Log("pages: " + taskStr[i + 1].Split(':')[1].Replace("\"", ""));
                    }
                    else if (taskStr[i + 1].Split(':')[0].Contains("large"))
                    {
                        Debug.Log("cover: " + (taskStr[i + 1].Split(':')[1]+":"+ taskStr[i + 1].Split(':')[2]).Replace("\"", "").Replace("}", ""));
                    }
                }
            }
        }

        void DisplayError(string a_error)
        {
            _error += a_error + "\n";
            if (_error.Contains("404"))
                _error += "Il s'agit probablement d'un problème d'accès au serveur renseigné, veuillez vérifier vos paramètres.\n";
            Debug.LogError(_error);
            _popup.Display(_error, OnConfirmError, true);
        }
        /*********************************************************/

        void OnConfirmError()
        {
            _error = string.Empty;
        }
        /*********************************************************/
                    
    /////////////////////////////////////////////////////////////// 
    /// PUBLIC FUNCTIONS ////////////////////////////////////////// 
    /////////////////////////////////////////////////////////////// 
    #region Games
        public void GetGames(int a_begin, int a_elementsNumber, Action a_callback)
        {
            StartCoroutine(GetGamesCoroutine(a_begin, a_elementsNumber, a_callback));
        }
        /*********************************************************/
        public void GetGamesNumber(Action a_callback)
        {
            StartCoroutine(GetGamesNumberCoroutine(a_callback));
        }
        /*********************************************************/
        public void GetPieces(int a_idGame, Action a_callback)
        {
            StartCoroutine(GetPiecesCoroutine(a_idGame, a_callback));
        }
        /*********************************************************/
        
        public void AddGame(GameData a_data, Action a_callback)
        {
            StartCoroutine(AddGameCoroutine(a_data, a_callback));
        }
        /*********************************************************/
        public void AddPiece(PieceData a_data, Action a_callback)
        {
            StartCoroutine(AddPieceCoroutine(a_data, a_callback));
        }
        /*********************************************************/

        public void UpdateGame(GameData a_data, Action a_callback)
        {
            StartCoroutine(UpdateGameCoroutine(a_data, a_callback));
        }
        /*********************************************************/
        public void UpdatePiece(PieceData a_data, Action a_callback)
        {
            StartCoroutine(UpdatePieceCoroutine(a_data, a_callback));
        }
        /*********************************************************/

        public void DeleteGame(GameData a_data, Action a_callback)
        {
            StartCoroutine(DeleteGameCoroutine(a_data, a_callback));
        }
        /*********************************************************/
        public void DeletePiece(PieceData a_data, Action a_callback)
        {
            StartCoroutine(DeletePieceCoroutine(a_data, a_callback));
        }
        /*********************************************************/
    #endregion


        public void GetCollectionsNumber(Action a_callback)
        {
            StartCoroutine(GetCollectionsNumberCoroutine(a_callback));
        }
        /*********************************************************/
        public void GetCollections(int a_begin, int a_elementsNumber, Action a_callback)
        {
            StartCoroutine(GetCollectionsCoroutine(a_begin, a_elementsNumber, a_callback));
        }
        /*********************************************************/
        public void AddCollection(CollectionData a_data, Action a_callback)
        {
            StartCoroutine(AddCollectionCoroutine(a_data, a_callback));
        }
        /*********************************************************/
        public void UpdateCollection(CollectionData a_data, Action a_callback)
        {
            StartCoroutine(UpdateCollectionCoroutine(a_data, a_callback));
        }
        /*********************************************************/
        public void DeleteCollection(CollectionData a_data, Action a_callback)
        {
            StartCoroutine(DeleteCollectionCoroutine(a_data, a_callback));
        }
        /*********************************************************/

        public void GetBooks(int a_idCollection, Action a_callback)
        {
            StartCoroutine(GetBooksCoroutine(a_idCollection, a_callback));
        }
        /*********************************************************/
        public void AddBook(BookData a_data, Action a_callback)
        {
            StartCoroutine(AddBookCoroutine(a_data, a_callback));
        }
        /*********************************************************/
        public void UpdateBook(BookData a_data, Action a_callback)
        {
            StartCoroutine(UpdateBookCoroutine(a_data, a_callback));
        }
        /*********************************************************/
        public void DeleteBook(BookData a_data, Action a_callback)
        {
            StartCoroutine(DeleteBookCoroutine(a_data, a_callback));
        }
        /*********************************************************/
        public void GetISBNData(string a_isbn, Action<ISBNData> a_callback)
        {
            StartCoroutine(GetISBNDataCoroutine(a_isbn, a_callback));
        }
        /*********************************************************/

        public void TestISBN()
		{
            StartCoroutine(TestISBNCoroutine());
		}
    }
}
