using DepthFactory.Utils;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Collections.Generic;

namespace DepthFactory.GamesPiecesInventory
{
    public enum InventoryType { Games, Books }

    public class SettingsManager : MonoBehaviour
    {
        [Header("Parameters")]
        [SerializeField] TMP_Text _debugText;
        [SerializeField] TMP_InputField _urlIF;
        [SerializeField] TMP_InputField _elementsNumberByPageIF;
        [SerializeField] TMP_InputField _daysBeforeValidationIF;
        [SerializeField] ColorPicker _colorPicker;
        [SerializeField] Canvas _colorPickerCanvas;
        [SerializeField] Image _mainColorImage;
        [SerializeField] Image _complementaryColorImage;
        [SerializeField] Image _secondaryColorImage;
        [SerializeField] Toggle _displayByLineToggle;
        [SerializeField] TMP_Dropdown _typeDD;
        [SerializeField] GameObject _validationGO;

        [Header("Localization")]
        [SerializeField] RectTransform _localizationContent;
        [SerializeField] GameObject _localizationPrefab;

        SettingsData _data;
        public SettingsData Data { get { return _data; } }
        //#// Cr�er une fonction statique par option ? Ex : public static string URL() { return _data.url; }

        SkinData _skinData;
        public SkinData SkinData { get { return _skinData; } }

        int _currentColorIndex;
        Animator _animator;
        LocalizationManager _localization;

        Action OnQuitPanel;

        
    ///////////////////////////////////////////////////////////////
    /// GENERAL FUNCTIONS /////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        void Awake()
        {
            _localization = LocalizationManager.Instance;
            LocalizationManager.OnTypeUpdate += OnTypeUpdate;
        }
        /******************************************************/

        void Start()
        {
            _animator = GetComponent<Animator>();
            _colorPicker.OnClose = OnClosePicker;
            _colorPicker.onValueChanged.AddListener(ChangeColor);

            InstantiateLocalizationGOs();
        }
        /******************************************************/
        
    ///////////////////////////////////////////////////////////////
    /// PRIVATE FUNCTIONS /////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        void ChangeColor(Color a_newColor)
        {
            switch (_currentColorIndex)
            {
                case 0:
                    _mainColorImage.color = a_newColor;
                    _skinData.mainColor = a_newColor;
                    break;
                case 1:
                    _complementaryColorImage.color = a_newColor;
                    _skinData.complementaryColor = a_newColor;
                    break;
                case 2:
                    _secondaryColorImage.color = a_newColor;
                    _skinData.secondaryColor = a_newColor;
                    break;
            }
            string skinStr = JsonUtility.ToJson(_skinData);
            File.WriteAllText(Constants.SKIN_PATH, skinStr);
            SkinHelper.OnSkinUpdate?.Invoke();
        }
        /******************************************************/

        void OnClosePicker()
        {
            _colorPickerCanvas.enabled = false;
        }
        /******************************************************/

        void InstantiateLocalizationGOs()
        {
            foreach (LocalizationPhrase phrase in _localization.Phrases)
            {
                GameObject phraseGO = Instantiate(_localizationPrefab, _localizationContent);
                phraseGO.GetComponent<LocalizationElement>().Init(phrase);
            }
        }
        /******************************************************/

        void OnTypeUpdate()
        {
            _validationGO.SetActive(_localization.CurrentType == LocalizationType.GameFR);
        }
        /******************************************************/
        
    ///////////////////////////////////////////////////////////////
    /// PUBLIC FUNCTIONS //////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        public void Init(Action a_onQuitPanel)
        {
            OnQuitPanel = a_onQuitPanel;
            _typeDD.ClearOptions();
            _typeDD.AddOptions(new List<string> { "Inventaire de jeux", "Inventaire de livres" });

            // Load settings
            if (File.Exists(GPIConstants.SETTINGS_PATH))
            {
                string settingsStr = File.ReadAllText(GPIConstants.SETTINGS_PATH);
                _data = JsonUtility.FromJson<SettingsData>(settingsStr);
            } else
            {
                _data = new SettingsData()
                {
                    elementsNumberByPage = GPIConstants.DEFAULT_ELEMENT_NUMBER_BY_PAGE,
                    daysBeforeValidation = GPIConstants.DEFAULT_DAYS_BEFORE_VALIDATION,
                    url = GPIConstants.DEFAULT_URL,
                    displayByLines = false,
                    inventoryType = 0
                };

                string settingsStr = JsonUtility.ToJson(_data);
                File.WriteAllText(GPIConstants.SETTINGS_PATH, settingsStr);
            }
            _elementsNumberByPageIF.text = _data.elementsNumberByPage.ToString();
            _daysBeforeValidationIF.text = _data.daysBeforeValidation.ToString();
            _urlIF.text = _data.url;
            _displayByLineToggle.isOn = _data.displayByLines;
            _typeDD.value = _data.inventoryType;
            _localization.CurrentType = ((LocalizationType)_data.inventoryType);

            // Load skin
            if (File.Exists(Constants.SKIN_PATH))
            {
                string skinStr = File.ReadAllText(Constants.SKIN_PATH);
                _skinData = JsonUtility.FromJson<SkinData>(skinStr);
            } else
            {
                _skinData = new SkinData()
                {
                    mainColor = Constants.DEFAULT_MAIN_COLOR,
                    complementaryColor = Constants.DEFAULT_COMPLEMENTARY_COLOR,
                    secondaryColor = Constants.DEFAULT_SECONDARY_COLOR
                };

                string skinStr = JsonUtility.ToJson(_skinData);
                File.WriteAllText(Constants.SKIN_PATH, skinStr);
            }

            _mainColorImage.color = _skinData.mainColor;
            _complementaryColorImage.color = _skinData.complementaryColor;
            _secondaryColorImage.color = _skinData.secondaryColor;

            GPIConstants.InitURL(_data.url);
        }
        /******************************************************/

        public void Btn_SetElementsNumberByPage(string a_number)
        {
            _data.elementsNumberByPage = int.Parse(a_number);
            string settingsStr = JsonUtility.ToJson(_data);
            File.WriteAllText(GPIConstants.SETTINGS_PATH, settingsStr);
        }
        /******************************************************/

        public void Btn_SetDaysBeforeValidation(string a_number)
        {
            _data.daysBeforeValidation = int.Parse(a_number);
            string settingsStr = JsonUtility.ToJson(_data);
            File.WriteAllText(GPIConstants.SETTINGS_PATH, settingsStr);
        }
        /******************************************************/

        public void Btn_SetURL(string a_url)
        {
            _data.url = a_url;
            string settingsStr = JsonUtility.ToJson(_data);
            File.WriteAllText(GPIConstants.SETTINGS_PATH, settingsStr);
            GPIConstants.InitURL(_data.url);
        }
        /******************************************************/

        public void Btn_SetType(int a_type)
        {
            _data.inventoryType = a_type;
            string settingsStr = JsonUtility.ToJson(_data);
            File.WriteAllText(GPIConstants.SETTINGS_PATH, settingsStr);
            GPIConstants.InitURL(_data.url);

            _localization.CurrentType = ((LocalizationType)a_type);
        }
        /******************************************************/

        public void Btn_SetDisplayByLines(bool a_byLines)
        {
            _data.displayByLines = a_byLines;
            string settingsStr = JsonUtility.ToJson(_data);
            File.WriteAllText(GPIConstants.SETTINGS_PATH, settingsStr);
            GPIConstants.InitURL(_data.url);
        }
        /******************************************************/

        public void Btn_SelectColor(int a_indexColor)
        {
            _colorPickerCanvas.enabled = true;
            _currentColorIndex = a_indexColor;
            _colorPicker.SetTitle("S�lectionnez la " + _skinData.GetColorName(_currentColorIndex));
        }
        /******************************************************/

        public void Btn_ResetSettings()
        {
            _data.elementsNumberByPage = GPIConstants.DEFAULT_ELEMENT_NUMBER_BY_PAGE;
            _data.daysBeforeValidation = GPIConstants.DEFAULT_DAYS_BEFORE_VALIDATION;
            _data.url = GPIConstants.DEFAULT_URL;
            _data.displayByLines = GPIConstants.DEFAULT_DISPLAY_BY_LINES;
            _data.inventoryType = GPIConstants.DEFAULT_INVENTORY_TYPE;
            _elementsNumberByPageIF.text = _data.elementsNumberByPage.ToString();
            _daysBeforeValidationIF.text = _data.daysBeforeValidation.ToString();
            _urlIF.text = _data.url;
            _displayByLineToggle.isOn = _data.displayByLines;
            string settingsStr = JsonUtility.ToJson(_data);
            File.WriteAllText(GPIConstants.SETTINGS_PATH, settingsStr);
            GPIConstants.InitURL(_data.url);

            _mainColorImage.color = Constants.DEFAULT_MAIN_COLOR;
            _skinData.mainColor = Constants.DEFAULT_MAIN_COLOR;
            _complementaryColorImage.color = Constants.DEFAULT_COMPLEMENTARY_COLOR;
            _skinData.complementaryColor = Constants.DEFAULT_COMPLEMENTARY_COLOR;
            _secondaryColorImage.color = Constants.DEFAULT_SECONDARY_COLOR;
            _skinData.secondaryColor = Constants.DEFAULT_SECONDARY_COLOR;
            string skinStr = JsonUtility.ToJson(_skinData);
            File.WriteAllText(Constants.SKIN_PATH, skinStr);
            SkinHelper.OnSkinUpdate?.Invoke();
        }
        /******************************************************/

        public void Btn_QuitPanel()
        {
            OnQuitPanel?.Invoke();
        }
        /******************************************************/

        public void Btn_DisplayTextLocalizations(bool a_show)
        {
            _animator.SetBool("Show", a_show);
        }
        /*********************************************************/

        public void UI_ResetLocalization()
		{
            _localization.ResetLocalization();
        }
        /*********************************************************/
    }


    //[Serializable]
    public class SettingsData
    {
        public int elementsNumberByPage;
        public int daysBeforeValidation;
        public string url;
        public bool displayByLines;
        public int inventoryType;
    }
}
