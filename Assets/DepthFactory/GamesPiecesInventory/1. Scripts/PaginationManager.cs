using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace DepthFactory.GamesPiecesInventory
{
    public class PaginationManager : MonoBehaviour
    {
        [SerializeField] TMP_Text _paginationText;
        [SerializeField] Button _firstPaginationButton;
        [SerializeField] TMP_Text _lastPaginationButtonText;
        [SerializeField] List<Button> _paginationButtons;
        [SerializeField] List<TMP_Text> _paginationTexts;
        [SerializeField] GameObject _beginSuspensionPointsGO;
        [SerializeField] GameObject _endSuspensionPointsGO;

        public Action OnPaginationChange;

        DatabaseManager _databaseManager;
        LocalizationManager _localizationManager;
        int _elementNumberByPage = 5;
        int _numberOfPages;
        int _currentPage = 0;
        int[] _currentPagesNumbers;

        Action OnLoadPageFirstLevelElements;

        const string PAGINATION_FORMAT = "R�sultat {0} - {1}\nsur {2}";

        
    ///////////////////////////////////////////////////////////////
    /// GENERAL FUNCTIONS /////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        void Start()
        {
            _databaseManager = DatabaseManager.Instance;
            _localizationManager = LocalizationManager.Instance;
        }
        /******************************************************/
        
    ///////////////////////////////////////////////////////////////
    /// PRIVATE FUNCTIONS /////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        void DisplayPagination()
        {
            int minElement = _currentPage * _elementNumberByPage + 1;
            int maxElement = Math.Min((_currentPage + 1) * _elementNumberByPage, _databaseManager.NumberOfFirstLevelElements);
            _paginationText.text = string.Format(PAGINATION_FORMAT, minElement, maxElement, _databaseManager.NumberOfFirstLevelElements);

            _beginSuspensionPointsGO.SetActive(_currentPage >= 4);
            _endSuspensionPointsGO.SetActive(_currentPage < _numberOfPages - 3 && _numberOfPages > 5);

            _currentPagesNumbers = new int[4];
            _currentPagesNumbers[0] = _currentPage <= 3 ? 2 : _currentPage - 1;
            _currentPagesNumbers[1] = _currentPage <= 3 ? 3 : _currentPage;
            _currentPagesNumbers[2] = _currentPage <= 3 ? 4 : _currentPage + 1;
            _currentPagesNumbers[3] = _currentPage <= 3 ? 5 : _currentPage + 2;
            
            for (int i = 0; i < 4; i++)
            {
                int iTmp = _currentPagesNumbers[i]-1;
                _paginationButtons[i].onClick.RemoveAllListeners();
                _paginationButtons[i].gameObject.SetActive(_numberOfPages >= i+2 && _currentPagesNumbers[i] < _numberOfPages);
                _paginationTexts[i].text = _currentPagesNumbers[i].ToString();
                _paginationButtons[i].onClick.AddListener(() => Btn_LoadPage(iTmp));
            }
        }
        /******************************************************/

        void Btn_LoadPage(int a_page)
        {
            SetCurrentColor(SkinHelper.GetColor("secondary"));
            _currentPage = a_page;
            SetCurrentColor(SkinHelper.GetColor("main"));
            DisplayPagination();
			switch (_localizationManager.CurrentType)
			{
				case LocalizationType.GameFR:
					_databaseManager.GetGames(a_page * _elementNumberByPage, _elementNumberByPage, OnLoadPageFirstLevelElements);
					break;
				case LocalizationType.BookFR:
					_databaseManager.GetCollections(a_page * _elementNumberByPage, _elementNumberByPage, OnLoadPageFirstLevelElements);
					break;
			}
            OnPaginationChange?.Invoke();
        }
		/******************************************************/

		void SetCurrentColor(Color a_color)
        {
            if (_currentPage == 0)
                _firstPaginationButton.image.color = a_color;
            else if (_currentPage == _numberOfPages - 1)
                _lastPaginationButtonText.GetComponentInParent<Image>().color = a_color;
            else
            {
                if (_currentPage <= 3)
                    _paginationButtons[_currentPage - 1].image.color = a_color;
                else if (_currentPage < _numberOfPages - 1)
                     _paginationButtons[2].image.color = a_color;
            }
        }
        /******************************************************/
        
    ///////////////////////////////////////////////////////////////
    /// PUBLIC FUNCTIONS //////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        public void Init(int a_elementsNumberByPage, Action a_onLoadPageGames)
        {
            _elementNumberByPage = a_elementsNumberByPage;
            OnLoadPageFirstLevelElements = a_onLoadPageGames;

            _numberOfPages = Mathf.CeilToInt((float)_databaseManager.NumberOfFirstLevelElements / (float)_elementNumberByPage);
            _lastPaginationButtonText.text = _numberOfPages.ToString();

            _firstPaginationButton.onClick.AddListener(() => Btn_LoadPage(0));
            _lastPaginationButtonText.GetComponentInParent<Button>().onClick.AddListener(() => Btn_LoadPage(_numberOfPages-1));
            Btn_LoadPage(0);
        }
		/******************************************************/
	}
}
