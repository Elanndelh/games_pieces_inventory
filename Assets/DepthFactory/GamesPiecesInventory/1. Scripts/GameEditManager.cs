using System;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DepthFactory.Utils;
using SFB;
using DepthFactory.UI;

namespace DepthFactory.GamesPiecesInventory
{
    //public class GameEditManager : A_FirstLevelEditionManager
    //{
    //    [SerializeField] TMP_Text _scanText;
    //    [SerializeField] TMP_InputField _scanIF;
    //    [SerializeField] DF_EditableField _descrField;
    //    [SerializeField] TMP_InputField _ageLimitIF;
    //    [SerializeField] TMP_InputField _minPlayerLimitIF;
    //    [SerializeField] TMP_InputField _maxPlayerLimitIF;
    //    [SerializeField] TMP_InputField _durationIF;
    //
    //    public Action<GameData> OnUpdate;
    //
    //    GameData _data;
    //
    //    const string DEFAULT_SCAN_MESSAGE = "Appuyer pour scanner";
    //
    //            
    /////////////////////////////////////////////////////////////////
    ///// PRIVATE FUNCTIONS /////////////////////////////////////////
    /////////////////////////////////////////////////////////////////
    //    protected override void CB_SetTitle()
    //  {
    //      _titleIF.text = _data.name;
    //      OnUpdate?.Invoke(_data);
    //  }
    //    /*********************************************************/
    //
    //    protected override void CB_SetImage()
    //  {
    //      if (string.IsNullOrEmpty(_data.path) == false)
    //      {
    //          _imageDisplay.Init(MediaType.Type.Image, _data.path);
    //      }
    //      else
    //      {
    //          _imageDisplay.ResetDisplay();
    //      }
    //
    //      OnUpdate?.Invoke(_data);
    //  }
    //    /*********************************************************/
    //
    //    void CB_SetScan()
    //    {
    //        _scanText.text = _data.scan;
    //        DFBarcodeScanner.Instance.Btn_Close();
    //        OnUpdate?.Invoke(_data);
    //    }
    //    /*********************************************************/
    //
    //    /// <summary>
        /// Add photo to the server and form
        /// </summary>
        /// <param name="a_texture"></param>
    //    protected override void CB_AddPhoto(List<Texture2D> a_textureList)
    //    {
    //        if (a_textureList != null && a_textureList.Count > 0)
    //        {
    //            foreach (Texture2D texture in a_textureList)
    //            {
    //                if (texture != null)
    //                    MediaManager.Instance.UploadFile(texture, GPIConstants.URL_MEDIAS_PHP, _data.path, CB_UploadSelectedFile);
    //            }
    //        }
    //    }
    //    /*********************************************************/
    //
    //    protected override void CB_UploadSelectedFile(string a_path)
    //    {
    //        if (a_path.StartsWith("Erreur:"))
    //        {
    //            Debug.LogError("erreur");
    //        }
    //        else
    //        {
    //            _data.path = a_path;
    //            _databaseManager.UpdateGame(_data, CB_SetImage);
    //        }
    //    }
    //    /*********************************************************/
    //
    //    protected override void CB_Delete()
    //    {
    //        OnDelete?.Invoke();
    //
    //        if (string.IsNullOrEmpty(_data.path) == false)
    //            MediaManager.Instance.DeleteFile(_data.path, GPIConstants.URL_MEDIAS_PHP, CB_DeleteImage);
    //    }
    //    /*********************************************************/
    //
    //    protected override void OnConfirmDelete()
    //    {
    //        _databaseManager.DeleteGame(_data, CB_Delete);
    //    }
    //    /*********************************************************/
    //
    //    void CB_DeleteImage() { }
	//	/*********************************************************/
    //
    //    void CB_SetDescr()
	//	{
    //        _descrField.SetText(_data.descr);
    //        OnUpdate?.Invoke(_data);
    //    }
    //    /*********************************************************/
    //
    //    void CB_SetThenUpdate()
    //    {
    //        OnUpdate?.Invoke(_data);
    //    }
    //    /*********************************************************/
    //
    //    void OnScan(string a_value)
    //    {
    //        _data.scan = a_value;
    //        _databaseManager.UpdateGame(_data, CB_SetScan);
    //    }
    //    /*********************************************************/
    //    
    /////////////////////////////////////////////////////////////////
    ///// PUBLIC FUNCTIONS //////////////////////////////////////////
    /////////////////////////////////////////////////////////////////
    //    public void Init(GameData a_data)
    //    {
    //        _data = a_data;
    //        _titleIF.text = _data.name;
    //        _scanText.text = string.IsNullOrEmpty(_data.scan) ? DEFAULT_SCAN_MESSAGE : _data.scan;
    //        _scanIF.SetTextWithoutNotify(_data.scan);
    //        _descrField.SetText(_data.descr);
    //        _ageLimitIF.SetTextWithoutNotify(_data.ageLimit.ToString());
    //        _minPlayerLimitIF.SetTextWithoutNotify(_data.minPlayerLimit.ToString());
    //        _maxPlayerLimitIF.SetTextWithoutNotify(_data.maxPlayerLimit.ToString());
    //        _durationIF.SetTextWithoutNotify(_data.duration.ToString());
    //
    //        if (string.IsNullOrEmpty(_data.path) == false)
    //        {
    //            _imageDisplay.Init(MediaType.Type.Image, _data.path);
    //        }
    //        else
    //        {
    //            _imageDisplay.ResetDisplay();
    //        }
    //    }
    //    /*********************************************************/
    //
    //    public override void UI_SetTitle(string a_value)
    //    {
    //        if (IsWrongType())
    //            return;
    //
    //        _data.name = a_value;
    //        _databaseManager.UpdateGame(_data, CB_SetTitle);
    //    }
    //    /*********************************************************/
    //
    //    public override void UI_BrowseImage()
    //    {
    //        if (LocalizationManager.Instance.CurrentType != LocalizationType.GameFR)
    //            return;
    //
//#if UNITY_STANDALONE_OSX || UNITY_STANDALONE_WIN || UNITY_EDITOR
    //        // Open file with filter
    //        ExtensionFilter[] extensions;
    //        extensions = new ExtensionFilter[]
    //        {
    //            new ExtensionFilter("Image Files", "png", "jpg", "jpeg", "PNG", "JPEG", "JPG" )
    //        };
    //
    //        string[] paths = StandaloneFileBrowser.OpenFilePanel("Import media file", _mediaPath, extensions, false);
    //        if (paths != null && paths.Length > 0)
    //        {
    //            MediaManager.Instance.UploadFile(paths[0], GPIConstants.URL_MEDIAS_PHP, _data.path, CB_UploadSelectedFile);
    //        }
//#elif UNITY_ANDROID || UNITY_IOS
    //        NativeGallery.Permission permission = NativeGallery.GetImageFromGallery((path) =>
    //        {
    //            Debug.Log("Image path: " + path);
    //            if (path != null)
    //            {
    //                MediaManager.Instance.UploadFile(path, GPIConstants.URL_MEDIAS_PHP, _data.path, CB_UploadSelectedFile);
    //            }
    //        }, "Select a PNG image", "image/png");
//#endif
    //    }
    //    /*********************************************************/
    //
    //    public void Btn_OpenScanner()
    //    {
    //        DFBarcodeScanner.Instance.Init(OnScan);
    //    }
    //    /*********************************************************/
    //
    //    public void Btn_EditScanIFVisibility(bool a_show)
    //    {
    //        _scanIF.gameObject.SetActive(a_show);
    //    }
    //    /*********************************************************/
    //
    //    public void Btn_EditScan(string a_scan)
    //    {
    //        OnScan(a_scan);
    //    }
    //    /*********************************************************/
    //
    //    public void UI_SetDescr(string a_value)
    //    {
    //        _data.descr = a_value;
    //        _databaseManager.UpdateGame(_data, CB_SetDescr);
    //    }
    //    /*********************************************************/
    //
    //    public void UI_SetAgeLimit(string a_value)
    //    {
    //        _data.ageLimit = int.Parse(a_value);
    //        _databaseManager.UpdateGame(_data, CB_SetThenUpdate);
    //    }
    //    /*********************************************************/
    //    public void UI_SetMinPlayerLimit(string a_value)
    //    {
    //        _data.minPlayerLimit = int.Parse(a_value);
    //        _databaseManager.UpdateGame(_data, CB_SetThenUpdate);
    //    }
    //    /*********************************************************/
    //    public void UI_SetMaxPlayerLimit(string a_value)
    //    {
    //        _data.maxPlayerLimit = int.Parse(a_value);
    //        _databaseManager.UpdateGame(_data, CB_SetThenUpdate);
    //    }
    //    /*********************************************************/
    //    public void UI_SetDuration(string a_value)
    //    {
    //        _data.duration = int.Parse(a_value);
    //        _databaseManager.UpdateGame(_data, CB_SetThenUpdate);
    //    }
    //    /*********************************************************/
    //
    //    public override void UI_QuitPanel() 
    //    {
    //        Btn_EditScanIFVisibility(false);
    //    }
    //    /*********************************************************/
    //}
}
