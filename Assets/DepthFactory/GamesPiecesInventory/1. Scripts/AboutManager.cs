using UnityEngine;
using TMPro;

namespace DepthFactory.GamesPiecesInventory
{
    public class AboutManager : MonoBehaviour
    {
        [SerializeField] TMP_Text _versionText;
        Animator _animator;

        
    ///////////////////////////////////////////////////////////////
    /// GENERAL FUNCTIONS /////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        void Start()
        {
            _animator = GetComponent<Animator>();
            InitializeVersionText();
        }
        /*********************************************************/
        
    ///////////////////////////////////////////////////////////////
    /// PRIVATE FUNCTIONS /////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        void InitializeVersionText()
        {
            if (Resources.Load<TextAsset>(GPIConstants.VERSIONS_NOTES_FILE) != null)
                _versionText.text = Resources.Load<TextAsset>(GPIConstants.VERSIONS_NOTES_FILE).text;
        }
        /*********************************************************/
        
    ///////////////////////////////////////////////////////////////
    /// PUBLIC FUNCTIONS //////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        public void Btn_DisplayVersionsNotes(bool a_show)
        {
            _animator.SetBool("Show", a_show);
        }
        /*********************************************************/
    }
}
