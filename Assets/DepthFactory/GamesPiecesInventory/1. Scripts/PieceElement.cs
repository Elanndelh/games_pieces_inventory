using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DepthFactory.Utils;

namespace DepthFactory.GamesPiecesInventory
{
    public class PieceElement : MonoBehaviour
    {
        [SerializeField] TMP_Text _title;
        [SerializeField] Image _numberBackground;
        [SerializeField] Color _problemColor;
        [SerializeField] TMP_Text _number;
        [SerializeField] MediaDisplay _imageDisplay;

        PieceData _data;
        Action<PieceData, PieceElement> OnSelect;
        Color _normalColor;

        
    ///////////////////////////////////////////////////////////////
    /// PUBLIC FUNCTIONS //////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        public void Init(PieceData a_data, Action<PieceData, PieceElement> a_selectAction)
        {
            _normalColor = SkinHelper.GetColor("main"); //_numberBackground.color;
            DisplayData(a_data);
            OnSelect = a_selectAction;
        }
        /*********************************************************/

        public void Btn_Select()
        {
            OnSelect?.Invoke(_data, this);
        }
        /*********************************************************/

        public void DisplayData(PieceData a_data)
        {
            _data = a_data;
            _title.text = _data.name;
            _number.text = _data.number.ToString();
            if (string.IsNullOrEmpty(_data.path) == false)
            {
                _imageDisplay.Init(MediaType.Type.Image, _data.path);
            }
            else
            {
                _imageDisplay.ResetDisplay();
            }

            _numberBackground.color = _data.number >= _data.numberOrig ? _normalColor : _problemColor;
        }
        /*********************************************************/

        public override string ToString()
        {
            return "[PieceElement] " + _data.name + " (" + _data.id + ") siblingIndex: " + transform.GetSiblingIndex();
        }
        /*********************************************************/
    } // Class PieceElement


    public class PieceData
    {
        public int id;
        public int idGame;
        public string name;
        public string path;
        public int number;
        public int numberOrig;

        
    ///////////////////////////////////////////////////////////////
    /// CONSTRUCTORS //////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        public PieceData()
        {
            id = -1;
            idGame = -1;
            name = string.Empty;
            path = string.Empty;
            number = 0;
            numberOrig = 0;
        }
        /*********************************************************/

        public PieceData(PieceData a_other)
        {
            id = a_other.id;
            idGame = a_other.idGame;
            name = a_other.name;
            path = a_other.path;
            number = a_other.number;
            numberOrig = a_other.numberOrig;
        }
        /*********************************************************/

        public PieceData(int a_id, int a_idGame, string a_name, string a_path, int a_number)
        {
            id = a_id;
            idGame = a_idGame;
            name = a_name;
            path = a_path;
            number = a_number;
        }
        /*********************************************************/
    } // Class PieceData
}
