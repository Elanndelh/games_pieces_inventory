using TMPro;
using UnityEngine;

namespace DepthFactory.GamesPiecesInventory
{
    public class LocalizationElement : MonoBehaviour
    {
        [SerializeField] TMP_Text _keyText;
        [SerializeField] TMP_InputField _locGameFRIF;
        [SerializeField] TMP_InputField _locBookFRIF;
        LocalizationPhrase _data;

        
    ///////////////////////////////////////////////////////////////
    /// PRIVATE FUNCTIONS /////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        void OnLocalizationUpdate()
        {
            _locGameFRIF.onEndEdit.SetPersistentListenerState(0, UnityEngine.Events.UnityEventCallState.Off);
            _locGameFRIF.text = LocalizationManager.Instance.GetLocalization(_data.key, LocalizationType.GameFR);
            _locGameFRIF.onEndEdit.SetPersistentListenerState(0, UnityEngine.Events.UnityEventCallState.RuntimeOnly);
            _locBookFRIF.onEndEdit.SetPersistentListenerState(0, UnityEngine.Events.UnityEventCallState.Off);
            _locBookFRIF.text = LocalizationManager.Instance.GetLocalization(_data.key, LocalizationType.BookFR);
            _locBookFRIF.onEndEdit.SetPersistentListenerState(0, UnityEngine.Events.UnityEventCallState.RuntimeOnly);
        }
        /******************************************************/

    ///////////////////////////////////////////////////////////////
    /// PUBLIC FUNCTIONS //////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        public void Init(LocalizationPhrase a_phrase)
        {
            _data = a_phrase;
            _keyText.text = _data.key;

            LocalizationManager.OnLocalizationUpdate += OnLocalizationUpdate;

            OnLocalizationUpdate();
        }
        /******************************************************/

        public void Btn_ChangeLocalizationGameFR(string a_newValue)
        {
            _data.SetTranslation(LocalizationType.GameFR, a_newValue);
            LocalizationManager.Instance.ChangeLocalization(_data);
        }
        /******************************************************/

        public void Btn_ChangeLocalizationBookFR(string a_newValue)
        {
            _data.SetTranslation(LocalizationType.BookFR, a_newValue);
            LocalizationManager.Instance.ChangeLocalization(_data);
        }
        /******************************************************/
    }
}
