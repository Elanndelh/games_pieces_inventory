﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
using BarcodeScanner;
using BarcodeScanner.Scanner;

namespace DepthFactory.Utils
{
    //using Iteca.DebugEx;

    //[CLSCompliant(false)]
    //[ExecuteInEditMode]
    public class CameraManager : MonoBehaviour
    {
        #region Singleton
        private static CameraManager instance;
        public static CameraManager Instance {
            get {
                if (CameraManager.instance == null) {
                    var instances = GameObject.FindObjectsOfType<CameraManager>();
                    if (instances.Length == 0) {
                        throw new NullReferenceException("There is no CameraManager but a script depends on it");
                    } else if (instances.Length > 1) {
                        UnityEngine.Debug.LogError("There is more than one CameraManager, this should not happen");
                        instance = instances[0];
                    } else {
                        instance = instances[0];
                    }
                }
                return instance;
            }
        }
        #endregion

        public TMP_Text _textDebug;

        public RectTransform fullscreenViewport;
        [Range(0.1f, 100f)]
        public float sizeDistanceRatio = 1f;
        [SerializeField]
        public Vector2 requestCaptureSize = new Vector2(1024, 768);

        List<GameObject> _canvasUIList;

        WebCamTexture _webcamTexture = null;
        [SerializeField] GameObject plane;
        [SerializeField] TextMeshProUGUI _outputText;

        bool _isWebcamOn = false;
        public bool IsWebcamOn() { return _isWebcamOn; }
        //public bool IsWebcamOn() { return _isWebcamOn; }
        int _currentWebcamDeviceIndex = -1;

        Vector3 currentRotation = Vector3.zero;
        bool isFlipped = false;
        ScreenOrientation storedScreenOrientation;

        
    ///////////////////////////////////////////////////////////////
    /// GENERAL FUNCTIONS /////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        void Start()
        {
            if (plane == null)
                plane = GameObject.FindWithTag("Webcam");
            if (plane != null)
            {
                //DebugEx.Write("WebCamTexture.devices.Length: " + WebCamTexture.devices.Length, false);
                //Debug.Log("WebCamTexture.devices.Length: " + WebCamTexture.devices.Length);
                if (WebCamTexture.devices.Length > 0)
                {
                    //_currentWebcamDeviceIndex = WebCamTexture.devices.Length > 1 ? 1 : 0;
                    _currentWebcamDeviceIndex = 0;
                    _webcamTexture = new WebCamTexture(WebCamTexture.devices[_currentWebcamDeviceIndex].name, (int)requestCaptureSize.x, (int)requestCaptureSize.y);

                    if (plane.GetComponent<RawImage>() != null)
                        plane.GetComponent<RawImage>().texture = _webcamTexture;

                    _webcamTexture.Play();
                    _webcamTexture.Stop();

                    _canvasUIList = new List<GameObject>();
                    foreach (Canvas c in GameObject.FindObjectsOfType<Canvas>())
                        _canvasUIList.Add(c.gameObject);

                    plane.SetActive(true);
                }
                else
                {
                    plane.SetActive(false);
                }
            }
        }
        /******************************************************/

#if UNITY_IOS
        private void Update()
        {
            // Check the device orientation fire an event when it is fire. Posiible usage for future Android Tablet
            if(Screen.orientation != storedScreenOrientation)
            {
                storedScreenOrientation = Screen.orientation;
                StartCoroutine(AdjustWebcamRotation());
            }
        }
        /******************************************************/
#endif

        void OnDisable()
        {
            if (_isWebcamOn && _webcamTexture != null)
            {
                _webcamTexture.Stop();
            }
        }
        /******************************************************/
        
    ///////////////////////////////////////////////////////////////
    /// PRIVATE FUNCTIONS /////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        IEnumerator EnableImageCoroutine()
        {
            yield return new WaitForEndOfFrame();
            if (plane.GetComponent<MaskableGraphic>() != null)
                plane.GetComponent<MaskableGraphic>().enabled = true;
        }
        /******************************************************/

        Texture2D RotateTexture90(Texture2D a_originalTexture, bool a_clockwise)
        {
            Color32[] original = a_originalTexture.GetPixels32();
            Color32[] rotated = new Color32[original.Length];
            int w = a_originalTexture.width;
            int h = a_originalTexture.height;

            int iRotated, iOriginal;

            for (int j = 0; j < h; ++j)
            {
                for (int i = 0; i < w; ++i)
                {
                    iRotated = (i + 1) * h - j - 1;
                    iOriginal = a_clockwise ? original.Length - 1 - (j * w + i) : j * w + i;
                    rotated[iRotated] = original[iOriginal];
                }
            }

            Texture2D rotatedTexture = new Texture2D(h, w);
            rotatedTexture.SetPixels32(rotated);
            rotatedTexture.Apply();
            return rotatedTexture;
        }
        /******************************************************/

        Texture2D FlipImage(Texture2D a_originalTexture)
        {
            Color32[] original = a_originalTexture.GetPixels32();
            Color32[] rotated = new Color32[original.Length];

            rotated = original;

            int width = a_originalTexture.width;
            int height = a_originalTexture.height;

            for (int row = 0; row < height; ++row)
                System.Array.Reverse(rotated, row * width, width);

            Texture2D rotatedTexture = new Texture2D(width, height);
            rotatedTexture.filterMode = FilterMode.Point;
            rotatedTexture.SetPixels32(rotated);
            rotatedTexture.Apply();
            return rotatedTexture;
        }
        /******************************************************/

        IEnumerator AdjustWebcamRotation()
        {
            yield return new WaitForEndOfFrame();
            if (WebCamTexture.devices.Length > 0)
            {
                isFlipped = WebCamTexture.devices[_currentWebcamDeviceIndex].isFrontFacing;
                if (isFlipped)
                {
                    plane.transform.localEulerAngles = new Vector3(plane.transform.localEulerAngles.x, 180, plane.transform.localEulerAngles.z);
                }
                else
                {
                    plane.transform.localEulerAngles = new Vector3(plane.transform.localEulerAngles.x, 0, plane.transform.localEulerAngles.z);
                }


                plane.transform.localEulerAngles = new Vector3(plane.transform.localEulerAngles.x, plane.transform.localEulerAngles.y, -_webcamTexture.videoRotationAngle);
                currentRotation = plane.transform.localEulerAngles;

#if UNITY_IOS //the viewport for ipad was inversed for no reason during tests
                if( SceneManager.GetActiveScene().name == Constants.MOBILE_SCENE_NAME)
                {
                    plane.transform.localEulerAngles += new Vector3(0, 180, 0);//if iphone
                    if (WebCamTexture.devices[_currentWebcamDeviceIndex].isFrontFacing)
                    {
                        currentRotation += new Vector3(0, 0, 180); //strange behavior : minature are turned by 180 with the front camera but not the viewport. in Iphone
                    }
                }
                else
                    plane.transform.localEulerAngles += new Vector3(0, 180, 180); //ipad
#endif

                //if (_webcamTexture != null)
                //    _textDebug.text = _webcamTexture.videoRotationAngle.ToString() + ", " + _currentWebcamDeviceIndex.ToString() + ", " + WebCamTexture.devices[_currentWebcamDeviceIndex].isFrontFacing;
            }
        }
        /******************************************************/

        Texture2D ApplyTransformation(Vector3 a_currentRotation, bool a_isFlipped, Texture2D a_snap)
        {
            if (a_isFlipped)
            {
                a_snap = FlipImage(a_snap);
            }

            for (int i = 0; i < Mathf.Abs(a_currentRotation.z); i += 90)
            {
                a_snap = RotateTexture90(a_snap, a_currentRotation.z < 0);
            }

            return a_snap;
        }
        /******************************************************/
        
    ///////////////////////////////////////////////////////////////
    /// PUBLIC FUNCTIONS //////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        public void ToggleWebcamDisplay(GameObject a_switchArrow = null)
        {
            _isWebcamOn = !_isWebcamOn;
            GetComponent<Canvas>().enabled = _isWebcamOn;

            if (_webcamTexture != null)
            {
                if (_isWebcamOn)
                {
                    _webcamTexture.Play();
                }
                else
                    _webcamTexture.Stop();
            }

            // Need this to refresh Raw/Image and display texture
            if (plane.GetComponent<MaskableGraphic>() != null)
                plane.GetComponent<MaskableGraphic>().enabled = false;
            StartCoroutine(EnableImageCoroutine());

            if (WebCamTexture.devices.Length > 1) //0
                if (a_switchArrow != null)
                    a_switchArrow.SetActive(_isWebcamOn);

            StartCoroutine(AdjustWebcamRotation());
        }
        /******************************************************/

        public void ShowWebcamDisplay(GameObject a_switchArrow = null)
        {
             _isWebcamOn = true;
            GetComponent<CanvasGroup>().alpha = 1.0f;
            this.GetComponent<CanvasGroup>().blocksRaycasts = true;
            _webcamTexture.Stop();

            if (WebCamTexture.devices.Length > 0)
                if (a_switchArrow != null)
                    a_switchArrow.SetActive(true);
        }
        /******************************************************/

        public void HideWebcamDisplay(GameObject a_switchArrow = null)
        {
            if(_isWebcamOn)
            {
                _isWebcamOn = !_isWebcamOn;
                GetComponent<Canvas>().enabled = _isWebcamOn;
                _webcamTexture.Stop();

                if (WebCamTexture.devices.Length > 0)
                    if (a_switchArrow != null)
                        a_switchArrow.SetActive(_isWebcamOn);
            }
        }
        /******************************************************/

        public void SwitchWebcam()
        {
            if (_isWebcamOn)
            {
                _webcamTexture.Stop();
            }
            if (WebCamTexture.devices.Length > 1)
            {
                _currentWebcamDeviceIndex++;
                _currentWebcamDeviceIndex %= WebCamTexture.devices.Length;
                //Debug.Log(WebCamTexture.devices[_currentWebcamDeviceIndex].name);
                _webcamTexture = new WebCamTexture(WebCamTexture.devices[_currentWebcamDeviceIndex].name);
                plane.GetComponent<RawImage>().texture = _webcamTexture;
            }

            StartCoroutine( AdjustWebcamRotation() );
            
            if (_isWebcamOn)
            {
                plane.GetComponent<MaskableGraphic>().enabled = false;
                plane.GetComponent<MaskableGraphic>().enabled = true;
                _webcamTexture.Play();
            }
        }
        /******************************************************/

        public Texture2D GetTextureFromWebcam()
        {
            Texture2D snap = new Texture2D(_webcamTexture.width, _webcamTexture.height);
            snap.SetPixels(_webcamTexture.GetPixels());
            //ROTATE LA TEXTURE ICI
            snap = ApplyTransformation(currentRotation, isFlipped, snap);
            snap.Apply();

            return snap;
        }
        /******************************************************/

        //NOT USED// 
        public void SaveToPNG(string filePath)
        {
            Texture2D snap = new Texture2D(_webcamTexture.width, _webcamTexture.height);
            snap.SetPixels(_webcamTexture.GetPixels());
            snap.Apply();
            System.IO.File.WriteAllBytes(filePath, snap.EncodeToPNG());
        }
        /******************************************************/
    }
}
