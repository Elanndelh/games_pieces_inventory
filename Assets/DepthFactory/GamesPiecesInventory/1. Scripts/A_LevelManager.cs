using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DepthFactory.GamesPiecesInventory
{
    public abstract class A_LevelManager : MonoBehaviour
    {
        [SerializeField] protected LocalizationType _locType;

        protected DatabaseManager _databaseManager;
        protected LocalizationManager _localizationManager;


    ///////////////////////////////////////////////////////////////
    /// GENERAL FUNCTIONS /////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        /*protected virtual void Awake()
        {
        }
        /*********************************************************/
        protected virtual void Start()
        {
            _databaseManager = DatabaseManager.Instance;
            _localizationManager = LocalizationManager.Instance;
        }
        /*********************************************************/
        
    ///////////////////////////////////////////////////////////////
    /// PROTECTED FUNCTIONS ///////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        protected bool IsWrongType()
        {
            return _localizationManager.CurrentType != _locType;
        }
        /*********************************************************/
        
    ///////////////////////////////////////////////////////////////
    /// PUBLIC FUNCTIONS //////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        public abstract void UI_QuitPanel();
        /*********************************************************/
    }
}
