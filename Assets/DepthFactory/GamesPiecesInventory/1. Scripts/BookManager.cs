using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DepthFactory.Utils;
using SFB;
using DepthFactory.UI;

namespace DepthFactory.GamesPiecesInventory
{
    public class BookManager : A_SecondLevelManager
    {
        [SerializeField] TMP_InputField _numberIF;
        //[SerializeField] GameObject _visualNumberOrigGO;
        //[SerializeField] GameObject _editNumberOrigGO;
        [SerializeField] Toggle _readToggle;
        [SerializeField] TMP_Text _scanText;
        [SerializeField] TMP_InputField _scanIF;
        [SerializeField] TMP_InputField _isbnIF;
        [SerializeField] DF_EditableField _descrField;
        [SerializeField] TMP_InputField _volumeNumberIF;
        [SerializeField] TMP_InputField _authorIF;

        [Header("Place Panel")]
        [SerializeField] TMP_Dropdown _placeTypeDD;
        [SerializeField] DF_EditableField _placeDetailsField;
        [SerializeField] GameObject _loanedRentedInformationsGO;
        [SerializeField] TMP_InputField _placeNameIF;

        public Action<BookData> OnUpdate;

        Animator _animator;
        BookData _data;
        ISBNData _isbnData;
        bool _forceRewrite;

        const string DEFAULT_SCAN_MESSAGE = "Appuyer pour scanner";

        
	///////////////////////////////////////////////////////////////
	/// GENERAL FUNCTIONS /////////////////////////////////////////
	///////////////////////////////////////////////////////////////
		void Awake()
		{
            _animator = GetComponent<Animator>();
            _placeTypeDD.ClearOptions();
			List<string> options = new List<string>{ "Rangement", "Pr�t", "Location" };
			_placeTypeDD.AddOptions(options);
        }
        /*********************************************************/

	///////////////////////////////////////////////////////////////
	/// PRIVATE FUNCTIONS /////////////////////////////////////////
	///////////////////////////////////////////////////////////////
		void CB_SetNumber()
        {
            _numberIF.text = _data.pageNumber.ToString();

			OnUpdate?.Invoke(_data);
		}
        /*********************************************************/

        protected override void CB_SetTitle()
        {
            _title.text = _data.name;
            _titleIF.text = _data.name;

            OnUpdate?.Invoke(_data);
        }
        /*********************************************************/

        void CB_SetImage()
        {
            if (string.IsNullOrEmpty(_data.path) == false)
            {
                _imageDisplay.Init(MediaType.Type.Image, _data.path);
            }
            else
            {
                _imageDisplay.ResetDisplay();
            }

			OnUpdate?.Invoke(_data);
		}
        /*********************************************************/
        void CB_SetScan()
        {
            _scanText.text = _data.scan;
            _scanIF.text = _data.scan;

            if (string.IsNullOrEmpty(_data.isbn))
                UI_SetISBN(_data.scan);

            OnUpdate?.Invoke(_data);
        }
        /*********************************************************/
        void CB_SetISBN()
        {
            _isbnIF.SetTextWithoutNotify(_data.isbn);

            OnUpdate?.Invoke(_data);
            Debug.Log(_data.isbn);
            _databaseManager.GetISBNData(_data.isbn, CB_GetISBNData);
        }
        /*********************************************************/

        void CB_GetISBNData(ISBNData a_isbnData)
		{
            _isbnData = a_isbnData;
            ReloadISBNData(_forceRewrite);
            _forceRewrite = false;
        }
        /*********************************************************/

        void CB_SetIsRead()
        {
            OnUpdate?.Invoke(_data);
        }
        /*********************************************************/

        void CB_SetDescr()
        {
            _descrField.SetText(_data.descr);
            OnUpdate?.Invoke(_data);
        }
        /*********************************************************/

        void CB_SetPlaceDetails()
        {
            _placeDetailsField.SetText(_data.placeDetails);
            OnUpdate?.Invoke(_data);
        }
        /*********************************************************/
        void CB_SetPlaceName()
        {
            OnUpdate?.Invoke(_data);
        }
        /*********************************************************/
        void CB_SetVolumeNumber()
        {
            _volumeNumberIF.text = _data.volumeNumber.ToString();

            OnUpdate?.Invoke(_data);
        }
        /*********************************************************/
        void CB_SetAuthor()
        {
            _authorIF.text = _data.author;

            OnUpdate?.Invoke(_data);
        }
        /*********************************************************/

        /// <summary>
        /// Add photo to the server and form
        /// </summary>
        /// <param name="a_texture"></param>
        protected override void CB_AddPhoto(List<Texture2D> a_textureList)
        {
            if (a_textureList != null && a_textureList.Count > 0)
            {
                foreach (Texture2D texture in a_textureList)
                {
                    if (texture != null)
                        MediaManager.Instance.UploadFile(texture, GPIConstants.URL_MEDIAS_PHP, _data.path, CB_UploadSelectedFile);
                }
            }
        }
        /*********************************************************/

        void CB_UploadSelectedFile(string a_path)
		{
            if (a_path.StartsWith("Erreur:"))
			{
                Debug.LogError("erreur");
			}
			else
			{
                _data.path = a_path;
                _databaseManager.UpdateBook(_data, CB_SetImage);
			}
		}
		/*********************************************************/

        void CB_Delete()
        {
            OnDelete?.Invoke();

            if (string.IsNullOrEmpty(_data.path) == false)
                MediaManager.Instance.DeleteFile(_data.path, GPIConstants.URL_MEDIAS_PHP, CB_DeleteImage);
        }
		/*********************************************************/

        void CB_DeleteImage() { }
        /*********************************************************/

        protected override void OnConfirmDelete()
        {
            _databaseManager.DeleteBook(_data, CB_Delete);
        }
        /*********************************************************/

        void OnScan(string a_value)
        {
            _data.scan = a_value;
            DFBarcodeScanner.Instance.Btn_Close();
            _databaseManager.UpdateBook(_data, CB_SetScan);
        }
        /*********************************************************/

        void DisplayImage()
        {
            if (string.IsNullOrEmpty(_data.path) == false)
            {
                _imageDisplay.Init(MediaType.Type.Image, _data.path);
            }
            else
            {
                _imageDisplay.ResetDisplay();
            }
        }
        /*********************************************************/

        void UpdatePlacePanel()
		{
            _loanedRentedInformationsGO.SetActive(_data.placeType != PlaceType.Tidy);
		}
        /*********************************************************/
        
    ///////////////////////////////////////////////////////////////
    /// PUBLIC FUNCTIONS //////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        public void Init(BookData a_data)
        {
            _data = a_data;
            _title.text = _data.name;
            _titleIF.text = _data.name;
            UI_DisplayEditTitle(string.IsNullOrEmpty(_data.name));
            _numberIF.text = _data.pageNumber.ToString();
            _scanText.text = string.IsNullOrEmpty(_data.scan) ? DEFAULT_SCAN_MESSAGE : _data.scan;
            _scanIF.SetTextWithoutNotify(_data.scan);
            _isbnIF.SetTextWithoutNotify(_data.isbn);
            _readToggle.SetIsOnWithoutNotify(_data.isRead);
            _descrField.SetText(_data.descr);
            _volumeNumberIF.text = _data.volumeNumber.ToString();
            _authorIF.text = _data.author;

            _placeTypeDD.SetValueWithoutNotify((int)_data.placeType);
            _placeDetailsField.SetText(_data.placeDetails);
            UpdatePlacePanel();
            _placeNameIF.SetTextWithoutNotify(_data.placeName);

            DisplayImage();
        }
        /*********************************************************/

        public void UI_SetNumber(string a_value)
        {
            if (IsWrongType())
                return;

            _data.pageNumber = int.Parse(a_value);
            _databaseManager.UpdateBook(_data, CB_SetNumber);
        }
        /*********************************************************/

        public void UI_EditScanIFVisibility(bool a_show)
        {
            _scanIF.gameObject.SetActive(a_show);
        }
        /*********************************************************/
        public void UI_OpenScanner()
        {
            if (IsWrongType())
                return;

            DFBarcodeScanner.Instance.Init(OnScan);
        }
        /*********************************************************/
        public void UI_EditScan(string a_scan)
        {
            OnScan(a_scan);
        }
        /*********************************************************/
        public void UI_SetISBN(string a_value)
        {
            if (IsWrongType())
                return;

            _data.isbn = a_value;
            _databaseManager.UpdateBook(_data, CB_SetISBN);
        }
        /*********************************************************/

        public void UI_SetTitle(string a_value)
        {
            if (IsWrongType())
                return;

            _data.name = a_value;
            _databaseManager.UpdateBook(_data, CB_SetTitle);
        }
        /*********************************************************/

        public void UI_SetRead(bool a_isRead)
        {
            if (IsWrongType())
                return;

            _data.isRead = a_isRead;
            _databaseManager.UpdateBook(_data, CB_SetIsRead);
        }
        /*********************************************************/

        public void UI_SetDesc(string a_value)
        {
            if (IsWrongType())
                return;

            _data.descr = a_value;
            _databaseManager.UpdateBook(_data, CB_SetDescr);
        }
        /*********************************************************/

        public void UI_SetPlaceType(int a_value)
        {
            if (IsWrongType())
                return;

            _data.placeType = (PlaceType)a_value;
            UpdatePlacePanel();
            _databaseManager.UpdateBook(_data, CB_SetPlaceDetails);
        }
        /*********************************************************/
        public void UI_SetPlaceDetails(string a_value)
        {
            if (IsWrongType())
                return;

            _data.placeDetails = a_value;
            _databaseManager.UpdateBook(_data, CB_SetPlaceDetails);
        }
        /*********************************************************/
        public void UI_SetPlaceName(string a_value)
        {
            if (IsWrongType())
                return;

            _data.placeName = a_value;
            _databaseManager.UpdateBook(_data, CB_SetPlaceName);
        }
        /*********************************************************/

        public void UI_SetVolumeNumber(string a_value)
        {
            if (IsWrongType())
                return;

            _data.volumeNumber = int.Parse(a_value);
            _databaseManager.UpdateBook(_data, CB_SetVolumeNumber);
        }
        /*********************************************************/

        public void UI_SetAuthor(string a_value)
        {
            if (IsWrongType())
                return;

            _data.author = a_value;
            _databaseManager.UpdateBook(_data, CB_SetAuthor);
        }
        /*********************************************************/

        public void UI_BrowseFile()
        {
            if (IsWrongType())
                return;

#if UNITY_STANDALONE_OSX || UNITY_STANDALONE_WIN || UNITY_EDITOR
            // Open file with filter
            ExtensionFilter[] extensions;
            extensions = new ExtensionFilter[]
            {
                new ExtensionFilter("Image Files", "png", "jpg", "jpeg", "PNG", "JPEG", "JPG" )
            };

            string[] paths = StandaloneFileBrowser.OpenFilePanel("Import media file", _mediaPath, extensions, false);
            if (paths != null && paths.Length > 0)
            {
                MediaManager.Instance.UploadFile(paths[0], GPIConstants.URL_MEDIAS_PHP, _data.path, CB_UploadSelectedFile);
            }
#elif UNITY_ANDROID || UNITY_IOS
            NativeGallery.Permission permission = NativeGallery.GetImageFromGallery((path) =>
            {
                Debug.Log("Image path: " + path);
                if (path != null)
                {
                    MediaManager.Instance.UploadFile(path, GPIConstants.URL_MEDIAS_PHP, _data.path, CB_UploadSelectedFile);
                }
            }, "Select a PNG image", "image/png");
#endif
        }
        /*********************************************************/

        /*public void UI_DisplayNumberOrigEditMode(bool a_showEditMode)
        {
            _visualNumberOrigGO.SetActive(!a_showEditMode);
            _editNumberOrigGO.SetActive(a_showEditMode);
        }
        /*********************************************************/

        /*public void UI_DisplayPlacePanel(bool a_showPlacePanel) //Not used?
		{
            _animator.SetBool("Show", a_showPlacePanel);
        }
		/*********************************************************/

		public override void UI_QuitPanel()
		{
            //UI_DisplayPlacePanel(false);
            UI_EditScanIFVisibility(false);
            _descrField.UI_DisplayEdition(false);
            _placeDetailsField.UI_DisplayEdition(false);
        }
        /*********************************************************/

        void ReloadISBNData(bool a_forceRewrite)
        {
            if (_isbnData == null)
                return;

            if (a_forceRewrite || string.IsNullOrEmpty(_data.name))
			{
                _data.name = _isbnData.title;
                _title.text = _data.name;
                _titleIF.text = _data.name;
            }
            if (a_forceRewrite || _data.pageNumber <= 0)
            {
                _data.pageNumber = _isbnData.pageNumber;
                _numberIF.text = _data.pageNumber.ToString();
            }
            if (a_forceRewrite || string.IsNullOrEmpty(_data.path))
            {
                _data.path = _isbnData.path;
                DisplayImage();
            }

            _databaseManager.UpdateBook(_data, CB_SetNumber);
        }
        /*********************************************************/

        public void UI_ReloadISBNData()
        {
            _forceRewrite = true;
            _databaseManager.GetISBNData(_data.isbn, CB_GetISBNData);
        }
        /*********************************************************/
    }
}
