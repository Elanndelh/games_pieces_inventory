using System;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DepthFactory.Utils;
using SFB;
using DepthFactory.UI;

namespace DepthFactory.GamesPiecesInventory
{
    //public class CollectionEditManager : A_FirstLevelEditionManager
    //{
    //    [SerializeField] DF_EditableField _descrField;
    //    [SerializeField] TMP_InputField _scanIF;
    //
    //    public Action<CollectionData> OnUpdate;
    //
    //    CollectionData _data;
    //
    //    
    /////////////////////////////////////////////////////////////////
    ///// PRIVATE FUNCTIONS /////////////////////////////////////////
    /////////////////////////////////////////////////////////////////
    //    protected override void CB_SetTitle()
    //  {
    //      _titleIF.text = _data.name;
    //      OnUpdate?.Invoke(_data);
    //  }
    //    /*********************************************************/
    //
    //    void CB_SetDescr()
	//	{
    //        _descrField.SetText(_data.descr);
    //        OnUpdate?.Invoke(_data);
    //    }
    //    /*********************************************************/
    //
    //    protected override void CB_SetImage()
    //    {
    //        if (string.IsNullOrEmpty(_data.path) == false)
    //        {
    //            _imageDisplay.Init(MediaType.Type.Image, _data.path);
    //        }
    //        else
    //        {
    //            _imageDisplay.ResetDisplay();
    //        }
    //
    //        OnUpdate?.Invoke(_data);
    //    }
    //    /*********************************************************/
    //
    //    /// <summary>
    //    /// Add photo to the server and form
    //    /// </summary>
    //    /// <param name="a_texture"></param>
    //    protected override void CB_AddPhoto(List<Texture2D> a_textureList)
    //    {
    //        if (a_textureList != null && a_textureList.Count > 0)
    //        {
    //            foreach (Texture2D texture in a_textureList)
    //            {
    //                if (texture != null)
    //                    MediaManager.Instance.UploadFile(texture, GPIConstants.URL_MEDIAS_PHP, _data.path, CB_UploadSelectedFile);
    //            }
    //        }
    //    }
    //    /*********************************************************/
    //
    //    protected override void CB_UploadSelectedFile(string a_path)
    //    {
    //        if (a_path.StartsWith("Erreur:"))
    //        {
    //            Debug.LogError("erreur");
    //        }
    //        else
    //        {
    //            _data.path = a_path;
    //            _databaseManager.UpdateCollection(_data, CB_SetImage);
    //        }
    //    }
    //    /*********************************************************/
    //
    //    protected override void CB_Delete()
    //    {
    //        OnDelete?.Invoke();
    //
    //        if (string.IsNullOrEmpty(_data.path) == false)
    //            MediaManager.Instance.DeleteFile(_data.path, GPIConstants.URL_MEDIAS_PHP, CB_DeleteObject);
    //    }
    //    /*********************************************************/
    //
    //    protected override void OnConfirmDelete()
    //    {
    //        _databaseManager.DeleteCollection(_data, CB_Delete);
    //    }
    //    /*********************************************************/
    //    
    /////////////////////////////////////////////////////////////////
    ///// PUBLIC FUNCTIONS //////////////////////////////////////////
    /////////////////////////////////////////////////////////////////
    //    public void Init(CollectionData a_data)
    //    {
    //        _data = a_data;
    //        _titleIF.text = _data.name;
    //        _descrField.SetText(_data.descr);
    //
    //        if (string.IsNullOrEmpty(_data.path) == false)
    //        {
    //            _imageDisplay.Init(MediaType.Type.Image, _data.path);
    //        }
    //        else
    //        {
    //            _imageDisplay.ResetDisplay();
    //        }
    //    }
    //    /*********************************************************/
    //
    //    public override void UI_SetTitle(string a_value)
    //    {
    //        if (IsWrongType())
    //            return;
    //
    //        _data.name = a_value;
    //        _databaseManager.UpdateCollection(_data, CB_SetTitle);
    //    }
    //    /*********************************************************/
    //
    //    public void UI_SetDesc(string a_value)
	//	{
    //        if (IsWrongType())
    //            return;
    //
    //        _data.descr = a_value;
    //        _databaseManager.UpdateCollection(_data, CB_SetDescr);
    //    }
    //    /*********************************************************/
    //
    //    public override void UI_BrowseImage()
    //    {
    //        if (IsWrongType())
    //            return;
    //
//#if UNITY_STANDALONE_OSX || UNITY_STANDALONE_WIN || UNITY_EDITOR
    //        // Open file with filter
    //        ExtensionFilter[] extensions;
    //        extensions = new ExtensionFilter[]
    //        {
    //            new ExtensionFilter("Image Files", "png", "jpg", "jpeg", "PNG", "JPEG", "JPG" )
    //        };
    //
    //        string[] paths = StandaloneFileBrowser.OpenFilePanel("Import media file", _mediaPath, extensions, false);
    //        if (paths != null && paths.Length > 0)
    //        {
    //            MediaManager.Instance.UploadFile(paths[0], GPIConstants.URL_MEDIAS_PHP, _data.path, CB_UploadSelectedFile);
    //        }
//#elif UNITY_ANDROID || UNITY_IOS
    //        NativeGallery.Permission permission = NativeGallery.GetImageFromGallery((path) =>
    //        {
    //            Debug.Log("Image path: " + path);
    //            if (path != null)
    //            {
    //                MediaManager.Instance.UploadFile(path, GPIConstants.URL_MEDIAS_PHP, _data.path, CB_UploadSelectedFile);
    //            }
    //        }, "Select a PNG image", "image/png");
//#endif
    //    }
    //    /*********************************************************/
    //
    //    public override void UI_QuitPanel()
    //    {
    //        _descrField.UI_DisplayEdition(false);
    //    }
    //    /*********************************************************/
    //}
}
