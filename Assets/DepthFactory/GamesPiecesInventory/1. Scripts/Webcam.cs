﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace DepthFactory.Utils
{
    public class Webcam : MonoBehaviour
    {
        public string screenshotPath;

        #region Singleton
        private static Webcam instance;
        public static Webcam Instance
        {
            get
            {
                if (Webcam.instance == null)
                {
                    var instances = GameObject.FindObjectsOfType<Webcam>();
                    if (instances.Length == 0)
                    {
                        throw new NullReferenceException("There is no Webcam but a script depends on it");
                    }
                    else if (instances.Length > 1)
                    {
                        UnityEngine.Debug.LogError("There is more than one Webcam, this should not happen");
                        instance = instances[0];
                    }
                    else
                    {
                        instance = instances[0];
                    }
                }
                return instance;
            }
        }
        #endregion

        #region Properties
        [SerializeField] GameObject _webcamPlane;
        [Header("Webcam Buttons")]
        [SerializeField] Button _activeWebcamButton;
        public Button GetActiveButton() { return _activeWebcamButton; }
        [SerializeField] Button _switchWebcamButton;
        public Button GetSwitchButton() { return _switchWebcamButton; }
        [SerializeField] Button _cancelWebcamButton;
        public Button CancelButton() { return _cancelWebcamButton; }
        [SerializeField] Button _takePhotoButton;
        public Button GetTakePhotoButton() { return _takePhotoButton; }
        [SerializeField] Button _validSelectionButton;
        public Button GetValidSelectionButton() { return _validSelectionButton; }
        [SerializeField] Color _pressedColor;

        //public Action<byte[]> ImageCaptured;
        public Action<List<Texture2D>> OnPhotosSelected;

        [SerializeField] TMP_Text _textDebug;
        
        // GameObject 
        string _lastPath;
        public string GetLastPath() { return _lastPath; }
        public string GetUniqueLastPath()
        {
            string tempPath = _lastPath;
            _lastPath = null;
            return tempPath;
        }

        Vector2 _initialWebcamSizeDelta;
        Vector2 _initialWebcamBackgroundAnchorMin;
        Vector2 _initialWebcamBackgroundAnchorMax;
        Vector3 _initialWebcamBackgroundLocalPosition;

        /// <summary>
        /// Capture Previews management
        /// </summary>
        int _nextCapturePreview = 0;
        bool _authorized = true;
        public List<CapturePreviewUI> _capturePreviewUIs;
        #endregion

        #region Events
        /// <summary>
        /// Event called when picture has been taken
        /// </summary>
        [Tooltip("Event called when picture has been taken")]
        public Action<Texture2D> OnPictureTaken = null;

        /// <summary>
        /// Event called when picture has been taken, selected and _validSelectionButton has been pressed
        /// </summary>
        [Tooltip("Event called when picture has been taken, selected and _validSelectionButton has been pressed")]
        public Action<List<Texture2D>> OnPicturesTakenAndSelected = null;
        #endregion

        
    ///////////////////////////////////////////////////////////////
    /// GENERAL FUNCTIONS /////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        void Awake()
        {
#if UNITY_IOS 
            //Start coroutine to request user authorisation
            if (!Application.HasUserAuthorization(UserAuthorization.WebCam))
            {
                StartCoroutine(RequestUserAuthorization());
            }
            else
            {
                _authorized = true;
            }
#elif UNITY_STANDALONE || UNITY_EDITOR || UNITY_ANDROID
                _authorized = true;
#endif
        }
        /******************************************************/

        void Start()
        {
            screenshotPath = Application.persistentDataPath + "/photos/";
            if (!Directory.Exists(screenshotPath))
                Directory.CreateDirectory(screenshotPath);
            
            InitializeWebcamButton();

            //_textDebug.text = WebCamTexture.devices.Length.ToString();
            //Debug.Log(WebCamTexture.devices.Length);
            if (WebCamTexture.devices.Length > 0)
            {
                // Init buttons state
                if (_activeWebcamButton != null)
                    _activeWebcamButton.GetComponent<Image>().enabled = true;
                //if (WebCamTexture.devices.Length > 1)
                //    _switchWebcamButton.GetComponent<Image>().enabled = true;

                _validSelectionButton.interactable = false;

                _capturePreviewUIs = new List<CapturePreviewUI>(GetComponentsInChildren<CapturePreviewUI>());

                foreach (CapturePreviewUI capture in _capturePreviewUIs)
                {
                    if (capture != null)
                    {
                       // capture.OnSelectionChanged += CaptureSelectionChanged;
                    }
                }

                if (_webcamPlane == null)
                {
                    _webcamPlane = GameObject.FindWithTag("Webcam");
                }
                if (_webcamPlane)
                {
                    Button webcamPlaneButton = _webcamPlane.GetComponent<Button>();
                    if (webcamPlaneButton != null)
                    {
                        webcamPlaneButton.onClick.AddListener(delegate
                        {
                            OnPhoto();
                        });
                    }

                    _initialWebcamSizeDelta = _webcamPlane.GetComponent<RectTransform>().sizeDelta;
                    _initialWebcamBackgroundAnchorMin = _webcamPlane.transform.parent.GetComponent<RectTransform>().anchorMin;
                    _initialWebcamBackgroundAnchorMax = _webcamPlane.transform.parent.GetComponent<RectTransform>().anchorMax;
                    _initialWebcamBackgroundLocalPosition = _webcamPlane.transform.parent.GetComponent<RectTransform>().localPosition;
                }
                else
                {
                    if (_activeWebcamButton != null)
                        _activeWebcamButton.gameObject.SetActive(false);
                    _switchWebcamButton.gameObject.SetActive(false);
                    _takePhotoButton.gameObject.SetActive(false);
                }
            }
            else
            {
                if (_activeWebcamButton != null)
                    _activeWebcamButton.gameObject.SetActive(false);
                _switchWebcamButton.gameObject.SetActive(false);
                _takePhotoButton.gameObject.SetActive(false);
            }
        }
        /******************************************************/
        
    ///////////////////////////////////////////////////////////////
    /// PRIVATE FUNCTIONS /////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        /// <summary>
        /// Initialize webcam's buttons : add listener on click, set visibility of some buttons
        /// </summary>
        void InitializeWebcamButton()
        {
            //Debug.Log(_activeWebcamButton);
            if (_activeWebcamButton != null)
            {
                _activeWebcamButton.onClick.AddListener(delegate
                {
                    CameraManager.Instance.ToggleWebcamDisplay(_switchWebcamButton.gameObject);
                    SetActiveButtonColor();
                });
            }

            _switchWebcamButton.gameObject.SetActive(WebCamTexture.devices.Length > 1);
            _switchWebcamButton.onClick.AddListener(delegate
            {
                CameraManager.Instance.SwitchWebcam();
            });

            _takePhotoButton.onClick.AddListener(delegate
            {
                OnPhoto();
            });

            _cancelWebcamButton.onClick.AddListener(delegate
            {
                //if (SceneManager.GetActiveScene().name == "MobileLogin")
                //{
                //    FichePanel.Instance.ChangeState("Close");
                //}
                Hide();
            });

            _validSelectionButton.onClick.AddListener(delegate
            {
                ValidSelection();
            });
        }
        /******************************************************/

        /// <summary>
        /// Valid taken photo selection
        /// </summary>
        void ValidSelection()
        {
            if (OnPhotosSelected != null)
            {
                List<Texture2D> photos = new List<Texture2D>();
                foreach (CapturePreviewUI capture in _capturePreviewUIs)
                {
                    if (capture != null /*&& capture.IsSelected*/)
                    {
                        photos.Add(capture.GetTexture);
                        //MediaManager.Instance.UploadFile(capture.GetTexture, ); // TODO : use callback to upload other videos
                    }
                }
                OnPhotosSelected(photos);
                OnPhotosSelected = null;
            }

            Hide();
        }
        /******************************************************/

        /// <summary>
        /// Launch a coroutine to take a photo
        /// </summary>
        void OnPhoto()
        {
            string date = DateTime.Now.ToString("yyMMdd_hhmmss");
            string photoName = "Photo_";
            photoName += date;
            StartCoroutine(Webcam.Instance.TakePhoto(screenshotPath + photoName));
        }
        /******************************************************/

        IEnumerator RequestUserAuthorization()
        {
            yield return Application.RequestUserAuthorization(UserAuthorization.WebCam);
            _authorized = Application.HasUserAuthorization(UserAuthorization.WebCam);
        }
        /******************************************************/

        /// <summary>
        ///  Add taken photo to next preview slot
        /// </summary>
        void AddPreview()
        {
            if (_capturePreviewUIs != null && _capturePreviewUIs.Count > 0)
            {
                if (_nextCapturePreview < _capturePreviewUIs.Count)
                {
                    Texture2D texture2D = CameraManager.Instance.GetTextureFromWebcam();
                    if (texture2D != null)
                    {
                        // Call back
                        if (OnPictureTaken != null)
                            OnPictureTaken(texture2D);

                        // Add taken photo to preview
                        _capturePreviewUIs[_nextCapturePreview].SetImage(texture2D);

                        // next slot
                        if (_nextCapturePreview + 1 >= _capturePreviewUIs.Count)
                            _nextCapturePreview = 0;
                        else
                            _nextCapturePreview++;

                        _validSelectionButton.interactable = true;
                    }
                }
            }
        }
        /******************************************************/
        
    ///////////////////////////////////////////////////////////////
    /// PUBLIC FUNCTIONS //////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        public void InitiatePhotoShot(Action<List<Texture2D>> a_callback)
        {
            if (_authorized)
            {
                OnPhotosSelected = a_callback;
                ToggleWebcamDisplay();
            }
        }
        /******************************************************/

        /// <summary>
        /// Toggles display of webcam UI with buttons
        /// </summary>
        public void ToggleWebcamDisplay()
        {
            //if (SceneManager.GetActiveScene().name == "MobileLogin")
            //{
            //    FichePanel.Instance.ChangeState("WebcamMode");
            //}

            CameraManager.Instance.ToggleWebcamDisplay(_switchWebcamButton.gameObject);
            Animator controller = GetComponent<Animator>();
            if (controller != null)
            {
                bool bOpen = controller.GetBool("Open");
                controller.SetBool("Open", !bOpen);
            }
            SetActiveButtonColor();
        }
        /******************************************************/

        /// <summary>
        /// Hide webcam UI
        /// </summary>
        public void Hide()
        {
            CameraManager.Instance.HideWebcamDisplay(_switchWebcamButton.gameObject);
            Animator controller = GetComponent<Animator>();
            if (controller != null)
            {
                controller.SetBool("Open", false);
            }
            _capturePreviewUIs[0].ResetImage();
            _validSelectionButton.interactable = false;
        }
        /******************************************************/

        /// <summary>
        /// Change color of activate button
        /// </summary>
        /// <param name="a_show">Nullable bool wich display or not the activate button, null means depending on webcam activation</param>
        public void SetActiveButtonColor(bool? a_show = null)
        {
            if (_activeWebcamButton)
            {
                if (a_show == null)
                {
                    _activeWebcamButton.GetComponent<Image>().color = CameraManager.Instance.IsWebcamOn() ? _pressedColor : Color.white;
                }
                else
                {
                    if ((bool)a_show)
                        _activeWebcamButton.GetComponent<Image>().color = _pressedColor;
                    else
                        _activeWebcamButton.GetComponent<Image>().color = Color.white;
                }
            }
        }
        /******************************************************/

        /// <summary>
        /// One of capture toggle has changed (selected or unselected)
        /// </summary>
        /*void CaptureSelectionChanged()
        {
            int nbSelectedCapture = 0;
            foreach (CapturePreviewUI capture in _capturePreviewUIs)
            {
                if (capture.IsSelected)
                    nbSelectedCapture++;
            }

            // if we selected at least one capture, allow validate
            _validSelectionButton.interactable = nbSelectedCapture > 0;
        }
        /******************************************************/

        /// <summary>
        /// Set webcamPlane on fullscreen, take a screenshot and set webcamPlane with its initial values
        /// </summary>
        /// <param name="a_screenshotName">Name of screenshot (with path)</param>
        /// <returns></returns>
        public IEnumerator TakePhoto(string a_screenshotName)
        {
            _textDebug.text = _authorized.ToString();
            if (WebCamTexture.devices.Length > 0 && _authorized)
            {
                RectTransform currentWebcamRT = _webcamPlane.GetComponent<RectTransform>();
                RectTransform currentWebcamBackgroundRT = _webcamPlane.transform.parent.GetComponent<RectTransform>();
                _textDebug.text += " -- " + currentWebcamRT.anchorMin + " -- " + currentWebcamRT.anchorMax;

                currentWebcamRT.sizeDelta = Vector2.zero;
                currentWebcamBackgroundRT.anchorMin = Vector2.zero;
                currentWebcamBackgroundRT.anchorMax = Vector2.one;
                currentWebcamBackgroundRT.localPosition = Vector3.zero;
                _lastPath = a_screenshotName + ".png";

                AddPreview();

                //Aplication.CaptureScreenshot(_lastPath);
                yield return new WaitForEndOfFrame();
                /*CameraManager.Instance.SaveToPNG(_lastPath);
                
                currentWebcamRT.sizeDelta = _initialWebcamSizeDelta;
                currentWebcamBackgroundRT.anchorMin = _initialWebcamBackgroundAnchorMin;
                currentWebcamBackgroundRT.anchorMax = _initialWebcamBackgroundAnchorMax;
                currentWebcamBackgroundRT.localPosition = _initialWebcamBackgroundLocalPosition;

                currentWebcamRT.parent.GetComponent<Image>().enabled = true;*/
            }
        }
        /******************************************************/
    }
}

