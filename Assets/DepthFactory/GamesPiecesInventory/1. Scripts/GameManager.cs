using System;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DepthFactory.UI;
using DepthFactory.Utils;
using SFB;
using UnityEngine.UI;

namespace DepthFactory.GamesPiecesInventory
{
    public class GameManager : A_FirstLevelManager
    {
        [SerializeField] TMP_Text _validationText;
        //[SerializeField] List<GameObject> _gamesGO = new List<GameObject>();

        [Header("Place Panel")]
        [SerializeField] TMP_Dropdown _placeTypeDD;
        [SerializeField] DF_EditableField _placeDetailsField;
        [SerializeField] GameObject _loanedRentedInformationsGO;
        [SerializeField] TMP_InputField _placeNameIF;

        [Header("Edit Panel")]
        [SerializeField] Toggle _descrToggle;
        [SerializeField] TMP_Text _scanText;
        [SerializeField] TMP_InputField _scanIF;
        [SerializeField] DF_EditableField _descrField;
        [SerializeField] TMP_InputField _ageLimitIF;
        [SerializeField] TMP_InputField _minPlayerLimitIF;
        [SerializeField] TMP_InputField _maxPlayerLimitIF;
        [SerializeField] TMP_InputField _durationIF;
        [SerializeField] TMP_InputField _customIdIF;

        public Action<GameData> OnUpdate;
        public Action<PieceElement, PieceData> OnInsert;

        Animator _animator;
        GameData _data;
        public GameData Data { get { return _data; } }
        Action<PieceData, PieceElement> OnSelect;

        const string VALIDATION_TEXT = "{0}";
        const string DEFAULT_SCAN_MESSAGE = "Appuyer pour scanner";
        
        
	///////////////////////////////////////////////////////////////
	/// GENERAL FUNCTIONS /////////////////////////////////////////
	///////////////////////////////////////////////////////////////
		void Awake()
		{
            _animator = GetComponent<Animator>();
            _placeTypeDD.ClearOptions();
			List<string> options = new List<string>{ "Rangement", "Pr�t", "Location" };
			_placeTypeDD.AddOptions(options);
        }
        /*********************************************************/
                
    ///////////////////////////////////////////////////////////////
    /// PROTECTED FUNCTIONS ///////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        protected override void DisplaySubObjects()
        {
            foreach (PieceData pieceData in _databaseManager.PiecesData)
            {
                GameObject gameGO = Instantiate(_subObjectPrefab, _contentRT);
                gameGO.name = pieceData.name;
                gameGO.GetComponent<PieceElement>().Init(pieceData, OnSelect);
            }
        }
        /*********************************************************/

        protected override void CB_AddSubObject()
        {
            PieceData data = new PieceData(_databaseManager.LastInsertedIndex, _data.id, string.Empty, string.Empty, 0);

            GameObject gameGO = Instantiate(_subObjectPrefab, _contentRT);
            gameGO.name = data.name;
            gameGO.GetComponent<PieceElement>().Init(data, OnSelect);

            OnInsert?.Invoke(gameGO.GetComponent<PieceElement>(), data);
        }
        /*********************************************************/

        protected override void CB_SetTitle()
        {
            _titleIF.text = _data.name;
            OnUpdate?.Invoke(_data);
        }
        /*********************************************************/

        protected override void CB_SetImage()
        {
            if (string.IsNullOrEmpty(_data.path) == false)
            {
                _imageDisplay.Init(MediaType.Type.Image, _data.path);
            }
            else
            {
                _imageDisplay.ResetDisplay();
            }

            OnUpdate?.Invoke(_data);
        }
        /*********************************************************/

        void CB_SetScan()
        {
            _scanText.text = _data.scan;
            DFBarcodeScanner.Instance.Btn_Close();
            OnUpdate?.Invoke(_data);
        }
        /*********************************************************/

        /// <summary>
        /// Add photo to the server and form
        /// </summary>
        /// <param name="a_texture"></param>
        protected override void CB_AddPhoto(List<Texture2D> a_textureList)
        {
            if (a_textureList != null && a_textureList.Count > 0)
            {
                foreach (Texture2D texture in a_textureList)
                {
                    if (texture != null)
                        MediaManager.Instance.UploadFile(texture, GPIConstants.URL_MEDIAS_PHP, _data.path, CB_UploadSelectedFile);
                }
            }
        }
        /*********************************************************/

        protected override void CB_UploadSelectedFile(string a_path)
        {
            if (a_path.StartsWith("Erreur:"))
            {
                Debug.LogError("erreur");
            }
            else
            {
                _data.path = a_path;
                _databaseManager.UpdateGame(_data, CB_SetImage);
            }
        }
        /*********************************************************/

        protected override void CB_Delete()
        {
            OnDelete?.Invoke();

            if (string.IsNullOrEmpty(_data.path) == false)
                MediaManager.Instance.DeleteFile(_data.path, GPIConstants.URL_MEDIAS_PHP, CB_DeleteImage);
        }
        /*********************************************************/

        protected override void OnConfirmDelete()
        {
            _databaseManager.DeleteGame(_data, CB_Delete);
        }
        /*********************************************************/

        void CB_DeleteImage() { }
		/*********************************************************/

        void CB_SetDescr()
		{
            _descrField.SetText(_data.descr);
            OnUpdate?.Invoke(_data);
        }
        /*********************************************************/

        void CB_SetThenUpdate()
        {
            OnUpdate?.Invoke(_data);
        }
        /*********************************************************/

        void OnScan(string a_value)
        {
            _data.scan = a_value;
            _databaseManager.UpdateGame(_data, CB_SetScan);
        }
        /*********************************************************/
        
    ///////////////////////////////////////////////////////////////
    /// PRIVATE FUNCTIONS /////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        void CB_SetValidationDate()
        {
            OnUpdate?.Invoke(_data);
        }
        /*********************************************************/

        void CB_SetPlaceDetails()
        {
            _placeDetailsField.SetText(_data.placeDetails);
            OnUpdate?.Invoke(_data);
        }
        /*********************************************************/
        void CB_SetPlaceName()
        {
            OnUpdate?.Invoke(_data);
        }
        /*********************************************************/

        void UpdatePlacePanel()
		{
            _loanedRentedInformationsGO.SetActive(_data.placeType != PlaceType.Tidy);
		}
        /*********************************************************/

    ///////////////////////////////////////////////////////////////
    /// PUBLIC FUNCTIONS //////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        public void Init(GameData a_gameData, Action<PieceData, PieceElement> a_selectPieceAction, bool a_newData = false)
        {
            UpdateData(a_gameData);
            OnSelect = a_selectPieceAction;

            //ClearSubObjects();
            if (a_newData)
            {
                UI_DisplayTitleIF(true);
                _descrField.UI_DisplayEdition(true);
                _descrToggle.isOn = true;
            } else
            {
                _databaseManager.GetPieces(_data.id, DisplaySubObjects);
            }
        }
        /*********************************************************/

        public void UpdateData(GameData a_gameData)
        {
            _data = a_gameData;
            _title.text = _data.name;
            _titleIF.text = _data.name;
            string validationDate = _data.lastValidationDate.AddYears(1) < DateTime.Now ? 
                "<color=red>" + _data.lastValidationDate.ToShortDateString()+"</color>" : _data.lastValidationDate.ToShortDateString();
            _validationText.text = string.Format(VALIDATION_TEXT, validationDate);

            _placeTypeDD.SetValueWithoutNotify((int)_data.placeType);
            _placeDetailsField.SetText(_data.placeDetails);
            UpdatePlacePanel();
            _placeNameIF.SetTextWithoutNotify(_data.placeName);

            _scanText.text = string.IsNullOrEmpty(_data.scan) ? DEFAULT_SCAN_MESSAGE : _data.scan;
            _scanIF.SetTextWithoutNotify(_data.scan);
            _descrField.SetText(_data.descr);
            _ageLimitIF.SetTextWithoutNotify(_data.ageLimit.ToString());
            _minPlayerLimitIF.SetTextWithoutNotify(_data.minPlayerLimit.ToString());
            _maxPlayerLimitIF.SetTextWithoutNotify(_data.maxPlayerLimit.ToString());
            _durationIF.SetTextWithoutNotify(_data.duration.ToString());
            _customIdIF.SetTextWithoutNotify(_data.customId);

            if (string.IsNullOrEmpty(_data.path) == false)
            {
                _imageDisplay.Init(MediaType.Type.Image, _data.path);
            }
            else
            {
                _imageDisplay.ResetDisplay();
            }
        }
        /*********************************************************/

        public override void UI_AddSubObject()
        {
            if (IsWrongType())
                return;

            PieceData data = new PieceData();
            data.idGame = _data.id;
            _databaseManager.AddPiece(data, CB_AddSubObject);
        }
        /*********************************************************/

        public void UI_SetPlaceType(int a_value)
        {
            if (IsWrongType())
                return;

            _data.placeType = (PlaceType)a_value;
            UpdatePlacePanel();
            _databaseManager.UpdateGame(_data, CB_SetPlaceDetails);
        }
        /*********************************************************/
        public void UI_SetPlaceDetails(string a_value)
        {
            if (IsWrongType())
                return;

            _data.placeDetails = a_value;
            _databaseManager.UpdateGame(_data, CB_SetPlaceDetails);
        }
        /*********************************************************/
        public void UI_SetPlaceName(string a_value)
        {
            if (IsWrongType())
                return;

            _data.placeName = a_value;
            _databaseManager.UpdateGame(_data, CB_SetPlaceName);
        }
        /*********************************************************/

        // Called by BackButton
        /*public void Btn_CleanPieces()
        {
            //ClearSubObjects();
        }
        /*********************************************************/

        public void Btn_UpdateValidationDate()
        {
            _data.lastValidationDate = DateTime.Now;
            _databaseManager.UpdateGame(_data, CB_SetValidationDate);
        }
        /*********************************************************/

        public void UI_DisplayPlacePanel(bool a_showPlacePanel)
        {
            _animator.SetBool("Show", a_showPlacePanel);
        }
        /*********************************************************/

        public override void UI_SetTitle(string a_value)
        {
            if (IsWrongType())
                return;

            _data.name = a_value;
            _databaseManager.UpdateGame(_data, CB_SetTitle);
        }
        /*********************************************************/

        public override void UI_BrowseImage()
        {
            if (LocalizationManager.Instance.CurrentType != LocalizationType.GameFR)
                return;

#if UNITY_STANDALONE_OSX || UNITY_STANDALONE_WIN || UNITY_EDITOR
            // Open file with filter
            ExtensionFilter[] extensions;
            extensions = new ExtensionFilter[]
            {
                new ExtensionFilter("Image Files", "png", "jpg", "jpeg", "PNG", "JPEG", "JPG" )
            };

            string[] paths = StandaloneFileBrowser.OpenFilePanel("Import media file", _mediaPath, extensions, false);
            if (paths != null && paths.Length > 0)
            {
                MediaManager.Instance.UploadFile(paths[0], GPIConstants.URL_MEDIAS_PHP, _data.path, CB_UploadSelectedFile);
            }
#elif UNITY_ANDROID || UNITY_IOS
            NativeGallery.Permission permission = NativeGallery.GetImageFromGallery((path) =>
            {
                Debug.Log("Image path: " + path);
                if (path != null)
                {
                    MediaManager.Instance.UploadFile(path, GPIConstants.URL_MEDIAS_PHP, _data.path, CB_UploadSelectedFile);
                }
            }, "Select a PNG image", "image/png");
#endif
        }
        /*********************************************************/

        public void UI_OpenScanner()
        {
            DFBarcodeScanner.Instance.Init(OnScan);
        }
        /*********************************************************/

        public void UI_EditScanIFVisibility(bool a_show)
        {
            _scanIF.gameObject.SetActive(a_show);
        }
        /*********************************************************/

        public void UI_EditScan(string a_scan)
        {
            OnScan(a_scan);
        }
        /*********************************************************/

        public void UI_SetDescr(string a_value)
        {
            _data.descr = a_value;
            _databaseManager.UpdateGame(_data, CB_SetDescr);
        }
        /*********************************************************/

        public void UI_SetAgeLimit(string a_value)
        {
            _data.ageLimit = int.Parse(a_value);
            _databaseManager.UpdateGame(_data, CB_SetThenUpdate);
        }
        /*********************************************************/
        public void UI_SetMinPlayerLimit(string a_value)
        {
            _data.minPlayerLimit = int.Parse(a_value);
            _databaseManager.UpdateGame(_data, CB_SetThenUpdate);
        }
        /*********************************************************/
        public void UI_SetMaxPlayerLimit(string a_value)
        {
            _data.maxPlayerLimit = int.Parse(a_value);
            _databaseManager.UpdateGame(_data, CB_SetThenUpdate);
        }
        /*********************************************************/
        public void UI_SetDuration(string a_value)
        {
            _data.duration = int.Parse(a_value);
            _databaseManager.UpdateGame(_data, CB_SetThenUpdate);
        }
        /*********************************************************/
        public void UI_SetCustomId(string a_value)
        {
            _data.customId = a_value;
            _databaseManager.UpdateGame(_data, CB_SetThenUpdate);
        }
        /*********************************************************/

        public override void UI_QuitPanel()
        {
            UI_CleanSubObjects();
            UI_DisplayPlacePanel(false);
            UI_EditScanIFVisibility(false);
            UI_DisplayTitleIF(false);
            _placeDetailsField.UI_DisplayEdition(false);
            _descrField.UI_DisplayEdition(false);
        }
        /*********************************************************/
    }
}
