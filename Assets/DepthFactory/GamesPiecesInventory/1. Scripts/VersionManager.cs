using UnityEngine;
using TMPro;

namespace DepthFactory.GamesPiecesInventory
{
    public class VersionManager : MonoBehaviour
    {
        [SerializeField] string _text;
        [SerializeField] TMP_Text[] _textElements;

        
    ///////////////////////////////////////////////////////////////
    /// GENERAL FUNCTIONS /////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        void Start()
        {
            foreach (TMP_Text textElement in _textElements)
                textElement.text = _text;
        }
        /*********************************************************/
    }
}
