using System;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DepthFactory.Utils;

namespace DepthFactory.GamesPiecesInventory
{
    public class CollectionElement : MonoBehaviour
    {
        [SerializeField] MediaDisplay _imageDisplay;
        [SerializeField] TMP_Text _title;
        [SerializeField] GameObject _completeGO;

        CollectionData _data;
        Action<CollectionData, CollectionElement> OnSelect;


    ///////////////////////////////////////////////////////////////
    /// PUBLIC FUNCTIONS //////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        public void Init(CollectionData a_data, Action<CollectionData, CollectionElement> a_selectAction)
        {
            DisplayData(a_data);
            OnSelect = a_selectAction;
        }
        /*********************************************************/

        public void DisplayData(CollectionData a_data)
        {
            _data = a_data;

            if (_imageDisplay != null)
            {
                if (string.IsNullOrEmpty(_data.path) == false)
                {
                    _imageDisplay.Init(MediaType.Type.Image, _data.path);
                }
                else
                {
                    _imageDisplay.ResetDisplay();
                }
            }
            _title.text = _data.name;
            if (_completeGO)
                _completeGO.SetActive(_data.complete);
        }
        /*********************************************************/

        public void Btn_Select()
        {
            OnSelect?.Invoke(_data, this);
        }
        /*********************************************************/
        public override string ToString()
        {
            return "[CollectionElement] " + _data.name + " (" + _data.id + ") siblingIndex: " + transform.GetSiblingIndex();
        }
        /*********************************************************/
    } // Class CollectionElement


    public class CollectionData
    {
        public int id;
        public string name;
        public string path;
        public string descr;
        public bool complete;
        
    ///////////////////////////////////////////////////////////////
    /// CONSTRUCTORS //////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        public CollectionData()
        {
            id = -1;
            name = string.Empty;
            path = string.Empty;
            descr = string.Empty;
            complete = false;
        }
        /*********************************************************/

        public CollectionData(CollectionData a_other)
        {
            id = a_other.id;
            name = a_other.name;
            path = a_other.path;
            descr = a_other.descr;
            complete = a_other.complete;
        }
        /*********************************************************/

        public CollectionData(int a_id, string a_path, string a_name = "", string a_descr = "")
        {
            id = a_id;
            name = a_name;
            path = a_path;
            descr = a_descr;
            complete = false;
        }
        /*********************************************************/
    } // Class CollectionData
}