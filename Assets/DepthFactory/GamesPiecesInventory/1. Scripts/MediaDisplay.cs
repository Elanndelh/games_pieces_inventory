﻿//using Iteca.Utils;
using SFB;
using System;
using System.Collections;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.Video;
using DepthFactory.Utils;

namespace DepthFactory.GamesPiecesInventory
{
    public class MediaDisplay : MonoBehaviour
    {
        MediaType.Type _mediaType;
        string _mediaPath;
        public string MediaPath { get { return _mediaPath; } }
        public int index;

        [Header("Common")]
        [SerializeField] Animator loadingAnimator;

        [Header("Image")]
        [SerializeField] Image image;

        [Header("Video")]
        [SerializeField] VideoPlayer videoPlayer;

        [SerializeField] RawImage rawImage;
        [SerializeField] Slider seekBar;
        [SerializeField] Text _seekText;
        [SerializeField] Sprite _defaultVideoImage;
        [SerializeField] Color _defaultVideoImageColor;
        [SerializeField] Button playButton;
        //[SerializeField] Button pauseButton;
        MediaThreadedJob _thread;
        
        bool _canSeek = false;
        
                
    ///////////////////////////////////////////////////////////////
    /// GENERAL FUNCTIONS /////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        void OnEnable()
        {
            if (videoPlayer)
            {
                videoPlayer.prepareCompleted += VideoPlayer_prepareCompleted;
                videoPlayer.loopPointReached += VideoPlayer_loopPointReached;
                videoPlayer.errorReceived += VideoPlayer_errorReceived;
            }
        }
        /*********************************************************/

        void OnDisable()
        {
            if (videoPlayer)
            {
                videoPlayer.prepareCompleted -= VideoPlayer_prepareCompleted;
                videoPlayer.loopPointReached -= VideoPlayer_loopPointReached;
                videoPlayer.errorReceived -= VideoPlayer_errorReceived;
            }
        }
        /*********************************************************/

        void Update()
        {
            if (_mediaType == MediaType.Type.Video && _canSeek == true)
            {
                if (seekBar)
                    seekBar.value = (float)videoPlayer.time;
                else if (_seekText)
                    _seekText.text = DurationTextFormat((int)videoPlayer.time, (int)videoPlayer.frameCount / (int)videoPlayer.frameRate);
            }
        }
        /*********************************************************/
        
    ///////////////////////////////////////////////////////////////
    /// PRIVATE FUNCTIONS /////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        IEnumerator LaunchThread(bool a_completeLoading)
        {
            if (_thread != null)
            {
                yield return StartCoroutine(_thread.WaitFor());
            }

            if (a_completeLoading == false && _mediaPath.Contains("_thumb.") == false)
            {
                //string extension = _mediaPath.Split('.').Last();
                //_mediaPath = _mediaPath.Insert(_mediaPath.Length - extension.Length - 1, "_thumb");
                _mediaPath = _mediaPath.GetThumbPath();
            }

            _thread = new MediaThreadedJob();
            _thread.path = _mediaPath.Contains("http") ? _mediaPath : _mediaPath.GetAbsolutePath(GPIConstants.URL);
            _thread.OnThreadFinished += OnThreadFinished;
            _thread.Start();

            yield return StartCoroutine(_thread.WaitFor());
        }
        /*********************************************************/
        void OnThreadFinished(byte[] a_imageBytes)
        {
            Texture2D texture = new Texture2D(1, 1, TextureFormat.RGB24, false);
            if (a_imageBytes != null)
            {
                texture.LoadImage(a_imageBytes);
            }

            if (image == null)
                UnityEngine.Debug.LogError("no image ui in imagePrefab");
            else
            {
                image.sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
                image.preserveAspect = true;
            }

            // download as ended when reaching here
            if (loadingAnimator)
                loadingAnimator.SetTrigger("Loaded");

            _thread = null;
        }
        /*********************************************************/

        void ChangeButtonsActivation(bool a_activePlayButton)
        {
            playButton.gameObject.SetActive(a_activePlayButton);
            //pauseButton.gameObject.SetActive(!a_activePlayButton);
        }
        /*********************************************************/

        string DurationTextFormat(int a_currentDuration, int a_maxDuration)
        {
            int currentMinutes = a_currentDuration / 60;
            int currentSeconds = a_currentDuration % 60;
            int maxMinutes = a_maxDuration / 60;
            int maxSeconds = a_maxDuration % 60;

            return currentMinutes + ":" + currentSeconds.ToString("D2") + " / " + maxMinutes + ":" + maxSeconds.ToString("D2");
        }
        /*********************************************************/

        #region Download Managment
        static bool TrustCertificate(object sender, X509Certificate x509Certificate, X509Chain x509Chain, SslPolicyErrors sslPolicyErrors)
        {
            // all Certificates are accepted
            return true;
        }
        /*********************************************************/

        /// <summary>
        /// Background load media, stops laoding animation at end
        /// </summary>
        /// <returns></returns>
        IEnumerator DownloadMediaCoroutine(bool a_completeLoading = true)
        {
            yield return new WaitUntil(delegate { return gameObject.activeInHierarchy; });
            Texture2D texture = new Texture2D(1,1, TextureFormat.RGB24, false);
            //Debug.Log(_mediaPath.GetAbsolutePath(GPIConstants.URL));

            string uri = _mediaPath.Contains("http") ? _mediaPath : _mediaPath.GetAbsolutePath(GPIConstants.URL);
            switch (_mediaType)
            {
                case MediaType.Type.Image:
                    {
                        if (a_completeLoading == false && _mediaPath.Contains("_thumb.") == false)
                        {
                            string extension = _mediaPath.Split('.').Last();
                            _mediaPath = _mediaPath.Insert(_mediaPath.Length - extension.Length - 1, "_thumb");
                        }
                        try
                        {
                            //Debug.Log(new Uri(_mediaPath.GetAbsolutePath(GPIConstants.URL)).IsFile);
                            if (new Uri(uri).IsFile)
                            {
                                byte[] bytes;
                                bytes = System.IO.File.ReadAllBytes(uri);
                                texture.LoadImage(bytes);
                            }
                            else
                            {
                                ServicePointManager.ServerCertificateValidationCallback = TrustCertificate;
                                ServicePointManager.Expect100Continue = true;

                                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(uri);
                                webRequest.ContentType = "application/x-www-form-urlencoded";
                                webRequest.Proxy = null;
                                webRequest.Method = "GET";
                                try
                                {
                                    HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse();
                                    int bytesRead;
                                    //int position = 0;
                                    int bufferSize = 512;
                                    byte[] buffer = new byte[bufferSize];
                                    using (MemoryStream ms = new MemoryStream())
                                    {
                                        using (BinaryReader reader = new BinaryReader(webResponse.GetResponseStream()))
                                        {
                                            ms.Position = 0;
                                            while ((bytesRead = reader.Read(buffer, 0, bufferSize)) != 0)
                                            {
                                                ms.Write(buffer, 0, bytesRead);
                                                //position += bytesRead;
                                            }
                                        }
                                        texture.LoadImage(ms.ToArray());
                                    }
                                    webResponse.Close();
                                }
                                catch
                                {
                                    texture = new Texture2D(1, 1);
                                }
//#endif
                            }
                            if (texture != null && texture.width == 8 && texture.height == 8)
                                texture = new Texture2D(1,1);
                        } catch { }

                        if (image == null)
                            UnityEngine.Debug.LogError("no image ui in imagePrefab");
                        else
                        {
                            image.sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
                            image.preserveAspect = true;
                        }

                        // download as ended when reaching here
                        if (loadingAnimator)
                            loadingAnimator.SetTrigger("Loaded");
                    }
                    break;
                case MediaType.Type.Video:
                    _canSeek = false;
                    if (videoPlayer == null || rawImage == null)
                    {
                        image.sprite = _defaultVideoImage;
                        image.color = _defaultVideoImageColor;
                        image.preserveAspect = true;
                        if (loadingAnimator)
                            loadingAnimator.SetTrigger("Loaded");
                    }
                    else if (!a_completeLoading)
                    {
                        if (loadingAnimator)
                            loadingAnimator.SetTrigger("Loaded");
                    }
                    else
                    {
                        Vector2 size = GetComponent<RectTransform>().rect.size;
                        float diff = 1.0f;
                        if (size.x > size.y)
                            diff = size.x / (float)size.y;
                        else
                            diff = size.y / (float)size.x;

                        RenderTexture renderTex = new RenderTexture((int)(768 * diff), 768, 24);
                        rawImage.texture = renderTex;

                        videoPlayer.renderMode = VideoRenderMode.RenderTexture;
                        videoPlayer.targetTexture = renderTex;
                        videoPlayer.url = uri;
                        videoPlayer.source = VideoSource.Url;
                        videoPlayer.aspectRatio = VideoAspectRatio.FitVertically;

                        videoPlayer.playOnAwake = false;
                        videoPlayer.waitForFirstFrame = false;
                        //audioSource.playOnAwake = false;

                        if (a_completeLoading)
                        {
                            videoPlayer.Prepare();
                        }
                        else
                        {
                            // download as ended when reaching here
                            if (loadingAnimator)
                                loadingAnimator.SetTrigger("Loaded");
                        }
                    }
                    break;
            }
        }
        /*********************************************************/
        #endregion
        
    ///////////////////////////////////////////////////////////////
    /// PUBLIC FUNCTIONS //////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        public void Init(MediaType.Type a_type, string a_path, bool a_completeLoading = true)
        {
            _mediaPath = a_path;
            _mediaType = a_type;

            //StartCoroutine(DownloadMediaCoroutine(a_completeLoading));
            if (_mediaType == MediaType.Type.Image)
                StartCoroutine(LaunchThread(a_completeLoading));
            else if (_mediaType == MediaType.Type.Video)
                StartCoroutine(DownloadMediaCoroutine(a_completeLoading));
        }
        /*********************************************************/

        public void ResetDisplay()
        {
            _mediaPath = string.Empty;
            switch (_mediaType)
            {
                case MediaType.Type.Image:
                    image.sprite = null;
                    break;

                case MediaType.Type.Video:
                    videoPlayer.Stop();
                    rawImage.texture = null;
                    videoPlayer.url = string.Empty;
                    break;
            }
        }
        /*********************************************************/

        public void DownloadMedia(bool a_completeLoading = true)
        {
            //StartCoroutine(DownloadMediaCoroutine(a_completeLoading));
            if (_mediaType == MediaType.Type.Image)
                StartCoroutine(LaunchThread(a_completeLoading));
            else if (_mediaType == MediaType.Type.Video)
                StartCoroutine(DownloadMediaCoroutine(a_completeLoading));
        }
        /*********************************************************/
        #region Video Management
        public void PlayVideo()
        {
            //Debug.Log("Play " + videoPlayer.frameCount);
            videoPlayer.Play();
            ChangeButtonsActivation(false);
        }
        /*********************************************************/

        public void PauseVideo()
        {
            if (videoPlayer.isPlaying)
            {
                videoPlayer.Pause();
                ChangeButtonsActivation(true);
            }
        }
        /*********************************************************/

        void VideoPlayer_errorReceived(VideoPlayer a_source, string a_message)
        {
            UnityEngine.Debug.LogError("Error with video : " + a_message);

            // download as ended when reaching here
            if (loadingAnimator)
                loadingAnimator.SetTrigger("Loaded");
            playButton.gameObject.SetActive(false);
            //pauseButton.gameObject.SetActive(false);
        }
        /*********************************************************/

        void VideoPlayer_prepareCompleted(VideoPlayer a_source)
        {
            _canSeek = true;
            if (seekBar)
            {
                seekBar.minValue = 0;
                seekBar.maxValue = videoPlayer.frameCount / (float)videoPlayer.frameRate;
            }
            else if (_seekText)
                _seekText.text = DurationTextFormat((int)videoPlayer.time, (int)videoPlayer.frameCount / (int)videoPlayer.frameRate);

            // download as ended when reaching here
            if (loadingAnimator)
                loadingAnimator.SetTrigger("Loaded");
        }
        /*********************************************************/

        void VideoPlayer_loopPointReached(VideoPlayer a_source)
        {
            if (!videoPlayer.isLooping && playButton != null /*&& pauseButton != null*/)
            {
                playButton.gameObject.SetActive(true);
                //pauseButton.gameObject.SetActive(false);
            }
        }
        /*********************************************************/
        #endregion

        public void SetIndex(int a_index)
        {
            index = a_index;
        }
        /*********************************************************/

        public Graphic GetGraphic()
        {
            switch (_mediaType)
            {
                case MediaType.Type.Image:
                    return image;
                case MediaType.Type.Video:
                    return rawImage;
                default:
                    return null;
            }
        }
        /*********************************************************/
    }


    [Serializable]
    public class MediaTypeOverlaySprite
    {
        public MediaType.Type type;
        public Sprite sprite;
    }
}