using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace DepthFactory
{
    [Serializable]
    public enum LocalizationType { GameFR, BookFR }

    [ExecuteInEditMode]
    public class LocalizationManager : Singleton<LocalizationManager>
    {
        public List<LocalizationPhrase> Phrases { get { return _data.phrases; } }
        [SerializeField] LocalizationData _data;

        LocalizationType _currentType;
        public LocalizationType CurrentType { get { return _currentType; } set { _currentType = value; OnTypeUpdate?.Invoke(); } }

        public static Action OnLocalizationUpdate;
        public static Action OnTypeUpdate;

        
    ///////////////////////////////////////////////////////////////
    /// GENERAL FUNCTIONS /////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        void Awake()
        {
            //Debug.Log("Awake");
            Constants.Init();
            //Debug.Log(Constants.LOCALIZATION_PATH);

            // Load localization
            if (File.Exists(Constants.LOCALIZATION_PATH))
            {
                string localizationStr = File.ReadAllText(Constants.LOCALIZATION_PATH);
                //_data = JsonUtility.FromJson<LocalizationData>(localizationStr);
                _data = JsonConvert.DeserializeObject<LocalizationData>(localizationStr);
                //Debug.Log(_data.phrases[0].ToJSON());
            }
            else
            {
                _data = new LocalizationData();

                string locStr = string.Empty;
                if (Resources.Load<TextAsset>(Constants.DEFAULT_LOCALIZATION_FILE) != null)
                    locStr = Resources.Load<TextAsset>(Constants.DEFAULT_LOCALIZATION_FILE).text;

                //Debug.Log(locStr);
                //_data = JsonUtility.FromJson<LocalizationData>(locStr);
                _data = JsonConvert.DeserializeObject<LocalizationData>(locStr);

                //Debug.Log(_data.phrases[0].ToJSON());

                //string skinStr = JsonUtility.ToJson(_data);
                locStr = JsonConvert.SerializeObject(_data);
                //Debug.Log(skinStr);
                File.WriteAllText(Constants.LOCALIZATION_PATH, locStr);
            }

            OnTypeUpdate += () => OnLocalizationUpdate?.Invoke();
        }
        /*********************************************************/

        void OnValidate()
        {
            //Debug.Log(_data.phrases[0].ToJSON());
            string skinStr = JsonUtility.ToJson(_data);
            //Debug.Log(Constants.LOCALIZATION_PATH);
            if (Constants.LOCALIZATION_PATH != "")
                File.WriteAllText(Constants.LOCALIZATION_PATH, skinStr);
        }
        /*********************************************************/
        
    ///////////////////////////////////////////////////////////////
    /// PUBLIC FUNCTIONS //////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        public string GetLocalization(string a_key)
        {
            foreach (LocalizationPhrase phrase in _data.phrases)
                if (phrase.key == a_key)
                    return phrase.GetTranslation(_currentType);
            return string.Empty;
        }
        /*********************************************************/

        public string GetLocalization(string a_key, LocalizationType a_type)
        {
            foreach (LocalizationPhrase phrase in _data.phrases)
                if (phrase.key == a_key)
                    return phrase.GetTranslation(a_type);
            return string.Empty;
        }
        /*********************************************************/

        public void ChangeLocalization(string a_key, string a_newValue)
        {
            foreach (LocalizationPhrase phrase in _data.phrases)
            {
                if (phrase.key == a_key)
                {
                    phrase.SetTranslation(_currentType, a_newValue);
                    break;
                }
            }
            //Debug.Log(_data.phrases[0].ToJSON());

            string skinStr = JsonUtility.ToJson(_data);
            //Debug.Log(skinStr);
            File.WriteAllText(Constants.LOCALIZATION_PATH, skinStr);
        }
        /*********************************************************/

        public void ChangeLocalization(LocalizationPhrase a_phrase)
        {
            int phraseIndex = 0;
            LocalizationPhrase updatedPhrase = new LocalizationPhrase();
            foreach (LocalizationPhrase phrase in _data.phrases)
            {
                if (phrase.key == a_phrase.key)
                {
                    updatedPhrase = new LocalizationPhrase(a_phrase);
                    break;
                }
                phraseIndex++;
            }
            _data.phrases[phraseIndex] = updatedPhrase;

            //Debug.Log(_data.phrases[phraseIndex].ToJSON());

            string skinStr = JsonUtility.ToJson(_data);
            File.WriteAllText(Constants.LOCALIZATION_PATH, skinStr);

            OnLocalizationUpdate?.Invoke();
        }
		/*********************************************************/

        public void ResetLocalization()
        {
            string locStr = string.Empty;
            if (Resources.Load<TextAsset>(Constants.DEFAULT_LOCALIZATION_FILE) != null)
                locStr = Resources.Load<TextAsset>(Constants.DEFAULT_LOCALIZATION_FILE).text;
            _data = JsonConvert.DeserializeObject<LocalizationData>(locStr);
            //Debug.Log("ResetLocalization: " + locStr);
            File.WriteAllText(Constants.LOCALIZATION_PATH, locStr);
            OnLocalizationUpdate?.Invoke();
        }
        /*********************************************************/
    } // class LocalizationManager


    [Serializable]
    public class LocalizationData
    {
        public List<LocalizationPhrase> phrases = new List<LocalizationPhrase>();
    } // class LocalizationData


    [Serializable]
    public class LocalizationTranslation : ICloneable
	{
        public LocalizationType type;
        public string value;

        public object Clone()
        {
            LocalizationTranslation temp = new LocalizationTranslation
            {
                type = type,
                value = value
            };
            return temp;
        }
    } // class LocalizationTranslation


    [Serializable]
    public class LocalizationPhrase
    {
        public string key;
        public List<LocalizationTranslation> translations = new List<LocalizationTranslation>();


        public LocalizationPhrase()
        {
            key = string.Empty;
            translations = new List<LocalizationTranslation>();
        }
        /*********************************************************/

        public LocalizationPhrase(string a_key, List<LocalizationTranslation> a_translations)
        {
            key = a_key;
            translations = new List<LocalizationTranslation>();
            foreach (LocalizationTranslation translation in a_translations)
                translations.Add(translation);
        }
        /*********************************************************/

        public LocalizationPhrase(LocalizationPhrase a_other)
        {
            key = a_other.key;
            translations = new List<LocalizationTranslation>();
            foreach (LocalizationTranslation translation in a_other.translations)
                translations.Add(translation);
        }
        /*********************************************************/

        public string GetTranslation(LocalizationType a_type)
		{
            return translations.Find(x => x.type == a_type).value;
        }
        /*********************************************************/

        public void SetTranslation(LocalizationType a_type, string a_newValue)
        {
            int translationId = translations.FindIndex(x => x.type == a_type);
            translations[translationId].value = a_newValue;
        }
        /*********************************************************/

        public string ToJSON()
		{
            LocalizationPhrase phrase = new LocalizationPhrase();
            phrase.key = key;
            phrase.translations = new List<LocalizationTranslation>(translations);
            string writeToFile = JsonUtility.ToJson(phrase);
            return writeToFile;
        }
        /*********************************************************/
    } // class LocalizationPhrase
}
