using UnityEngine;


/// <summary>
/// Be aware this will not prevent a non singleton constructor
///   such as `T myT = new T();`
/// To prevent that, add `protected T () {}` to your singleton class.
/// 
/// As a note, this is made as MonoBehaviour because we need Coroutines.
/// </summary>
[ExecuteInEditMode]
public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    //static int _count;
    //static bool _applicationIsQuitting = false;

    static T _instance = null;
    public static T Instance
    {
        get { if (_instance == null)
            {
                _instance = (T)FindObjectOfType(typeof(T));
                if (_instance == null)
                    _instance = new GameObject(typeof(T).Name).AddComponent<T>();
            }
            return _instance; 
        }
        private set { _instance = value; }
    }

    protected virtual void Awake()
    {
        //DontDestroyOnLoad(gameObject);
    }
    /*public static T Instance
    {
        get
        {
            if (_instance == null)
            {
                //_count = 0;
                _instance = (T)FindObjectOfType(typeof(T));

                if (_instance == null)
                {
                    if (!_applicationIsQuitting)
                    {
                        GameObject singleton = new GameObject();
                        singleton.name = typeof(T).Name;
                        _instance = singleton.AddComponent<T>();
                    }
                }
            }
            return _instance;
        }
    }*/


    /*public virtual void OnEnable()
    {
        CheckMultipleInstance();
    }*/

    /*static void CheckMultipleInstance()
    {
        if (_count > 1)
        {
            Debug.LogError("Something's wrong, here. Multiple singleton is not allowed.");
        }
    }*/

    /*public virtual void OnApplicationQuit()
    {
        _applicationIsQuitting = true;
    }*/
}
