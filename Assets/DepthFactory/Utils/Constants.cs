using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constants
{
    public static string SKIN_PATH = string.Empty;
    public static readonly string PHP_TOKEN = "dfzea�('2dfg54thr";
    public static string LOCALIZATION_PATH = string.Empty;
    public static readonly string DEFAULT_LOCALIZATION_FILE = "Localization";

    #region DEFAULT SKIN
    public static readonly Color DEFAULT_MAIN_COLOR = new Color(0.8980393f, 0.09803922f, 0.09803922f, 1f);
    public static readonly Color DEFAULT_COMPLEMENTARY_COLOR = new Color(0.937255f, 0.9137256f, 0.8274511f, 1f);
    public static readonly Color DEFAULT_SECONDARY_COLOR = new Color(0.2862745f, 0.2862745f, 0.2862745f, 1f);
    #endregion


    public static void Init()
    {
        SKIN_PATH = Application.persistentDataPath + "/Skin.json";
        LOCALIZATION_PATH = Application.persistentDataPath + "/Localization.json";
    }
    /*********************************************************/
}
