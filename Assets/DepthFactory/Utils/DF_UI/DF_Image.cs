using UnityEngine;
using UnityEngine.UI;

namespace DepthFactory.UI
{
    [AddComponentMenu("DepthFactoryUI/DF Image")]
    public class DF_Image : Image, IDFUIComponent
    {
        [HideInInspector] [SerializeField] bool _useSkin;
        public bool UseSkin { get { return _useSkin; } set { _useSkin = value; } }

        [HideInInspector] [SerializeField] int _currentColorIndex;
        public int CurrentColorIndex { get { return _currentColorIndex; } set { _currentColorIndex = value; } }


    ///////////////////////////////////////////////////////////////
    /// INHERITED FUNCTIONS ///////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        protected override void Start()
        {
            base.Start();
            OnSkinUpdate();
            SkinHelper.OnSkinUpdate += OnSkinUpdate;
        }
        /*********************************************************/

        protected override void OnDestroy()
        {
            base.OnDestroy();
            SkinHelper.OnSkinUpdate -= OnSkinUpdate;
        }
        /*********************************************************/

    ///////////////////////////////////////////////////////////////
    /// PUBLIC FUNCTIONS //////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        #region Interfaces
        public void OnSkinUpdate()
        {
            if (UseSkin && Application.isPlaying)
            {
                if (_currentColorIndex > 0)
                    color = SkinHelper.GetColor(_currentColorIndex - 1);
            }
        }
        /*********************************************************/
        #endregion
    }
}
