#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Collections.Generic;
using System;
using System.Linq;
using UnityEngine.EventSystems;

namespace DepthFactory.UI
{
    [AddComponentMenu("DepthFactoryUI/DF TMP Text")]
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class DF_TMPText : MonoBehaviour, IDFUIComponent, IPointerClickHandler
    {
#if UNITY_EDITOR
        [MenuItem("CONTEXT/DF TMP Text/ClearlyResize")]
        static void ClearlyResize()
        {
            GameObject go = (GameObject)Selection.activeObject;
            Text component = go.GetComponent<Text>();
            RectTransform rt = go.GetComponent<RectTransform>();

            component.fontSize *= 3;
            component.resizeTextMinSize *= 3;
            component.resizeTextMaxSize *= 3;
            rt.localScale /= 3;
            Vector2 newAnchorMin = new Vector2(rt.anchorMin.x - (rt.anchorMax.x - rt.anchorMin.x), rt.anchorMin.y - (rt.anchorMax.y - rt.anchorMin.y));
            Vector2 newAnchorMax = new Vector2(rt.anchorMax.x + (rt.anchorMax.x - rt.anchorMin.x), rt.anchorMax.y + (rt.anchorMax.y - rt.anchorMin.y));
            rt.anchorMin = newAnchorMin;
            rt.anchorMax = newAnchorMax;
            rt.sizeDelta *= 3;
        }
        /*********************************************************/

        [MenuItem("CONTEXT/DF TMP Text/RevertClearlyResize")]
        static void RevertClearlyResize()
        {
            GameObject go = (GameObject)Selection.activeObject;
            Text component = go.GetComponent<Text>();
            RectTransform rt = go.GetComponent<RectTransform>();

            component.fontSize /= 3;
            component.resizeTextMinSize /= 3;
            component.resizeTextMaxSize /= 3;
            rt.localScale *= 3;
            Vector2 newAnchorMin = new Vector2(2 * rt.anchorMin.x / 3 + rt.anchorMax.x / 3, 2 * rt.anchorMin.y / 3 + rt.anchorMax.y / 3);
            Vector2 newAnchorMax = new Vector2(2 * rt.anchorMax.x / 3 + rt.anchorMin.x / 3, 2 * rt.anchorMax.y / 3 + rt.anchorMin.y / 3);
            rt.anchorMin = newAnchorMin;
            rt.anchorMax = newAnchorMax;
            rt.sizeDelta /= 3;
        }
        /*********************************************************/

        [MenuItem("CONTEXT/DF TMP Text/ResetTextWithoutCase")]
        static void ResetTextWithoutCase()
        {
            GameObject go = (GameObject)Selection.activeObject;
            DF_TMPText component = go.GetComponent<DF_TMPText>();
            component.TextWithoutCase = null;
        }
        /*********************************************************/
#endif

        #region Skin
        [HideInInspector] [SerializeField] bool _useSkin;
        public bool UseSkin { get { return _useSkin; } set { _useSkin = value; } }

        [HideInInspector] [SerializeField] int _currentFontIndex;
        public int CurrentFontIndex { get { return _currentFontIndex; } set { _currentFontIndex = value; } }
        [HideInInspector] [SerializeField] int _currentColorIndex;
        public int CurrentColorIndex { get { return _currentColorIndex; } set { _currentColorIndex = value; } }
        #endregion

        #region Format
        [HideInInspector] [SerializeField] bool _isUpper;
        public bool IsUpper { get { return _isUpper; } set { _isUpper = value; } }
        [HideInInspector] [SerializeField] bool _isLower;
        public bool IsLower { get { return _isLower; } set { _isLower = value; } }
        [HideInInspector] [SerializeField] string _textWithoutCase;
        public string TextWithoutCase { get { return _textWithoutCase; } set { _textWithoutCase = value; } }
        #endregion

        [HideInInspector] [SerializeField] TMP_Text _textComponent;
        public TMP_Text TextComponent { get { return _textComponent; } set { _textComponent = value; } }

        #region Hyperlink
        public bool UseHyperlink { get { return _usehyperlink; } set { _usehyperlink = value; } }
        [SerializeField] bool _usehyperlink;

        public bool DoesColorChangeOnHover { get { return _doesColorChangeOnHover; } set { _doesColorChangeOnHover = value; } }
        [SerializeField] bool _doesColorChangeOnHover = true;
        public int HoverColorIndex { get { return _hoverColorIndex; } set { _hoverColorIndex = value; } }
        [SerializeField] int _hoverColorIndex;
        public Color HoverColor { get { return _hoverColor; } set { _hoverColor = value; } }
        [SerializeField] Color _hoverColor = new Color(60f / 255f, 120f / 255f, 1f);

        Canvas _canvas;
        Camera _camera;

        public bool IsLinkHighlighted { get { return _currentLink != -1; } }

        int _currentLink = -1;
        List<Color32[]> _originalVertexColors = new List<Color32[]>();
        #endregion

        #region Localization
        [HideInInspector] [SerializeField] bool _useLocalization;
        public bool UseLocalization { get { return _useLocalization; } set { _useLocalization = value; } }
        [HideInInspector] [SerializeField] int _localizationIndex;
        public int LocalizationIndex { get { return _localizationIndex; } set { _localizationIndex = value; } }

        [SerializeField] string _originalText;
        #endregion


    ///////////////////////////////////////////////////////////////
    /// INHERITED FUNCTIONS ///////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        void Awake()
        {
            _textComponent = GetComponent<TMP_Text>();
            _canvas = GetComponentInParent<Canvas>();
            _originalText = _textComponent.text;

            if (_canvas != null)
            {
                // Get a reference to the camera if Canvas Render Mode is not ScreenSpace Overlay.
                if (_canvas.renderMode == RenderMode.ScreenSpaceOverlay)
                    _camera = null;
                else
                    _camera = _canvas.worldCamera;
            }
        }
        /*********************************************************/

        void OnValidate()
        {
            _textComponent = GetComponent<TMP_Text>();
        }
        /*********************************************************/

        void Start()
        {
            OnSkinUpdate();
            SkinHelper.OnSkinUpdate += OnSkinUpdate;
            LocalizationManager.OnLocalizationUpdate += LocalizeText;
        }
        /*********************************************************/

        void LateUpdate()
        {
            if (_usehyperlink)
            {
                // is the cursor in the correct region (above the text area) and furthermore, in the link region?
                var isHoveringOver = TMP_TextUtilities.IsIntersectingRectTransform(_textComponent.rectTransform, Input.mousePosition, _camera);
                int linkIndex = isHoveringOver ? TMP_TextUtilities.FindIntersectingLink(_textComponent, Input.mousePosition, _camera)
                    : -1;

                // Clear previous link selection if one existed.
                if (_currentLink != -1 && linkIndex != _currentLink)
                {
                    // Debug.Log("Clear old selection");
                    SetLinkToColor(_currentLink, (linkIdx, vertIdx) => _originalVertexColors[linkIdx][vertIdx]);
                    _originalVertexColors.Clear();
                    _currentLink = -1;
                }

                // Handle new link selection.
                if (linkIndex != -1 && linkIndex != _currentLink)
                {
                    // Debug.Log("New selection");
                    _currentLink = linkIndex;
                    if (_doesColorChangeOnHover)
                        _originalVertexColors = SetLinkToColor(linkIndex, (_linkIdx, _vertIdx) => _hoverColor);
                }

                // Debug.Log(string.Format("isHovering: {0}, link: {1}", isHoveringOver, linkIndex));
            }
        }
        /*********************************************************/

        void OnDestroy()
        {
            SkinHelper.OnSkinUpdate -= OnSkinUpdate;
            LocalizationManager.OnLocalizationUpdate -= LocalizeText;
        }
        /*********************************************************/

    ///////////////////////////////////////////////////////////////
    /// PRIVATE FUNCTIONS /////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        List<Color32[]> SetLinkToColor(int a_linkIndex, Func<int, int, Color32> a_colorForLinkAndVert)
        {
            TMP_LinkInfo linkInfo = _textComponent.textInfo.linkInfo[a_linkIndex];

            var oldVertColors = new List<Color32[]>(); // store the old character colors

            for (int i = 0; i < linkInfo.linkTextLength; i++) // for each character in the link string
            {
                int characterIndex = linkInfo.linkTextfirstCharacterIndex + i; // the character index into the entire text
                var charInfo = _textComponent.textInfo.characterInfo[characterIndex];
                int meshIndex = charInfo.materialReferenceIndex; // Get the index of the material / sub text object used by this character.
                int vertexIndex = charInfo.vertexIndex; // Get the index of the first vertex of this character.

                Color32[] vertexColors = _textComponent.textInfo.meshInfo[meshIndex].colors32; // the colors for this character
                oldVertColors.Add(vertexColors.ToArray());

                if (charInfo.isVisible)
                {
                    vertexColors[vertexIndex + 0] = a_colorForLinkAndVert(i, vertexIndex + 0);
                    vertexColors[vertexIndex + 1] = a_colorForLinkAndVert(i, vertexIndex + 1);
                    vertexColors[vertexIndex + 2] = a_colorForLinkAndVert(i, vertexIndex + 2);
                    vertexColors[vertexIndex + 3] = a_colorForLinkAndVert(i, vertexIndex + 3);
                }
            }

            // Update Geometry
            _textComponent.UpdateVertexData(TMP_VertexDataUpdateFlags.All);

            return oldVertColors;
        }
        /*********************************************************/

    ///////////////////////////////////////////////////////////////
    /// PUBLIC FUNCTIONS //////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        #region Interfaces
        public void OnSkinUpdate()
        {
            if (UseSkin && Application.isPlaying)
            {
                if (_currentColorIndex > 0)
                    _textComponent.color = SkinHelper.GetColor(_currentColorIndex - 1);
                /*if (_currentFontIndex >= 0)
                {
                    if (_currentFontIndex == 0 && fontSize >= 60)
                    {
                        if (fontSize >= 120)
                        {
                            if (fontSize >= 180)
                            {
                                if (fontSize >= 240)
                                    font = SkinHelper.GetGeneralFont(3);
                                else
                                    font = SkinHelper.GetGeneralFont(2);
                            }
                            else
                                font = SkinHelper.GetGeneralFont(1);
                        }
                        else
                            font = SkinHelper.GetGeneralFont(0);
                    }
                    else
                    {
                        font = SkinHelper.GetFont(_currentFontIndex);
                    }
                }*/
            }

            LocalizeText();
            UpdateCase();
        }
        /*********************************************************/

        public void OnPointerClick(PointerEventData a_eventData)
        {
            // Debug.Log("Click at POS: " + a_eventData.position + "  World POS: " + a_eventData.worldPosition);
            if (_usehyperlink)
            {
                int linkIndex = TMP_TextUtilities.FindIntersectingLink(_textComponent, Input.mousePosition, _camera);
                if (linkIndex != -1) // was a link clicked?
                {
                    TMP_LinkInfo linkInfo = _textComponent.textInfo.linkInfo[linkIndex];

                    // Debug.Log(string.Format("id: {0}, text: {1}", linkInfo.GetLinkID(), linkInfo.GetLinkText()));
                    // open the link id as a url, which is the metadata we added in the text field
                    Application.OpenURL(linkInfo.GetLinkID());
                }
            }
        }
        /*********************************************************/
        #endregion

        public void UpdateCase()
        {
            if (_isUpper)
            {
                _textWithoutCase = _textComponent.text;
                _textComponent.text = _textComponent.text.ToUpper();
            }
            else if (_isLower)
            {
                _textWithoutCase = _textComponent.text;
                _textComponent.text = _textComponent.text.ToLower();
            }
        }
        /*********************************************************/

        public void LocalizeText()
        {
            if (_useLocalization)
            {
                _textComponent.text = _originalText;
                foreach (LocalizationPhrase phrase in LocalizationManager.Instance.Phrases)
                    _textComponent.text = _textComponent.text.Replace("{{" + phrase.key + "}}", LocalizationManager.Instance.GetLocalization(phrase.key));
            }
        }
        /*********************************************************/

        public void LocalizeText(string a_newText)
        {
            _originalText = a_newText;
            if (_useLocalization)
            {
                _textComponent.text = _originalText;
                foreach (LocalizationPhrase phrase in LocalizationManager.Instance.Phrases)
                    _textComponent.text = _textComponent.text.Replace("{{" + phrase.key + "}}", LocalizationManager.Instance.GetLocalization(phrase.key));
            }
        }
        /*********************************************************/
    }
}
