using DepthFactory.Editor;
using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace DepthFactory
{
    public static class SkinHelper
    {
        // SKIN COLROS
        public static readonly string COLOR_MAIN = "main";
        public static readonly string COLOR_COMPLEMENTARY = "complementary";
        public static readonly string COLOR_SECONDARY = "secondary";
        public static readonly string COLOR_ERROR = "error";

        static SkinData _skinData;
        static Dictionary<string, Color> _skinnedColorsByName;
        static List<Color> _skinnedColorsByIndex;
        static List<Font> _skinnedFontsByIndex;
        static List<Font> _skinnedGeneralFontsBySize;

        public static Action OnSkinUpdate;


        /// <summary>
        /// Initialize SkinHelper and it's dictionary and list from a skin
        /// </summary>
        /// <param name="a_activeSkinName">Name of skin to initialize</param>
        public static void Init()
        {
            _skinnedGeneralFontsBySize = new List<Font>();
            LoadSkin();
            OnSkinUpdate = LoadSkin;
        }
        /*********************************************************/

        /// <summary>
        /// Get color from skin with it's name
        /// </summary>
        /// <param name="a_colorName">Name of color from skin</param>
        /// <returns></returns>
        public static Color GetColor(string a_colorName)
        {
            if (_skinnedColorsByName.ContainsKey(a_colorName))
                return _skinnedColorsByName[a_colorName];

            Debug.LogError("This color (" + a_colorName + ") isn't skinned");
            return Color.clear;
        }
        /*********************************************************/

        /// <summary>
        /// Get color from skin with it's index
        /// </summary>
        /// <param name="a_colorIndex">Index of color from skin</param>
        /// <returns></returns>
        public static Color GetColor(int a_colorIndex)
        {
            if (_skinnedColorsByIndex.Count > a_colorIndex)
                return _skinnedColorsByIndex[a_colorIndex];

            Debug.LogError("This color's index (" + a_colorIndex + ") isn't skinned");
            return Color.clear;
        }
        /*********************************************************/

        /// <summary>
        /// Get font from skin with it's index
        /// </summary>
        /// <param name="a_fontIndex">Index of font from skin</param>
        /// <returns></returns>
        public static Font GetFont(int a_fontIndex)
        {
            if (_skinnedFontsByIndex.Count > a_fontIndex)
                return _skinnedFontsByIndex[a_fontIndex];

            Debug.LogError("This font's index (" + a_fontIndex + ") isn't skinned");
            return default;
        }
        /*********************************************************/
        /// <summary>
        /// Get general font from skin with it's size
        /// </summary>
        /// <param name="a_fontIndex">Index of general font from skin</param>
        /// <returns></returns>
        public static Font GetGeneralFont(int a_generalFontIndex)
        {
            if (_skinnedGeneralFontsBySize.Count > a_generalFontIndex)
                return _skinnedGeneralFontsBySize[a_generalFontIndex];

            Debug.LogError("This general font's index (" + a_generalFontIndex + ") isn't skinned");
            return GetFont(0);
        }
        /*********************************************************/

        /// <summary>
        /// Update skin of every elements on scene
        /// </summary>
        /// <param name="a_newSkinName">Name of the new skin</param>
        /*public static void UpdateSkin(string a_newSkinFileName)
        {
            Init(a_newSkinFileName);
            OnSkinUpdate?.Invoke();
        }
        /*********************************************************/

        /*public static void SaveSkin()
        {
            string skinStr = JsonUtility.ToJson(_skinData);
            Debug.Log(skinStr);
            //File.WriteAllText(Constants.SETTINGS_PATH, skinStr);
        }
        /*********************************************************/

        static void LoadSkin()
        {
            _skinnedColorsByName = new Dictionary<string, Color>();
            _skinnedColorsByIndex = new List<Color>();
            _skinnedFontsByIndex = new List<Font>();

            if (File.Exists(Constants.SKIN_PATH))
            {
                string skinStr = File.ReadAllText(Constants.SKIN_PATH);
                _skinData = JsonUtility.FromJson<SkinData>(skinStr);
            }
            else
            {
                _skinData = new SkinData()
                {
                    mainColor = Constants.DEFAULT_MAIN_COLOR,
                    complementaryColor = Constants.DEFAULT_COMPLEMENTARY_COLOR,
                    secondaryColor = Constants.DEFAULT_SECONDARY_COLOR
                };

                string skinStr = JsonUtility.ToJson(_skinData);
                File.WriteAllText(Constants.SKIN_PATH, skinStr);
            }
            //_skinData.FromJSON(skinFile);

            // Main
            _skinnedColorsByName.Add(COLOR_MAIN, _skinData.mainColor);
            _skinnedColorsByIndex.Add(_skinData.mainColor);
            // Opposite
            _skinnedColorsByName.Add(COLOR_COMPLEMENTARY, _skinData.complementaryColor);
            _skinnedColorsByIndex.Add(_skinData.complementaryColor);
            // Secondary
            _skinnedColorsByName.Add(COLOR_SECONDARY, _skinData.secondaryColor);
            _skinnedColorsByIndex.Add(_skinData.secondaryColor);
            // Error
            _skinnedColorsByName.Add(COLOR_ERROR, _skinData.errorColor);
            _skinnedColorsByIndex.Add(_skinData.errorColor);

            _skinnedFontsByIndex.Add(_skinData.generalFont);
        }
        /*********************************************************/
    }
}
