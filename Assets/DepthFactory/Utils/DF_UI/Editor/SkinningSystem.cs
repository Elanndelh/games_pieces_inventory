// Created by Tristan Gassies, 2018/08/14
// Last modification : 2018/10/30

#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEditor;
using DepthFactory.UI;
using DepthFactory.Utils;
using UnityEditor.SceneManagement;
using System.IO;
//using DepthFactory.Utils.Editor;

//using Lean.Localization;
using System.Linq;
#region Notes
/* Version 0.2: 2018/08/23
 * Add automatic scene repaint
 */
/* Version 0.1: 2018/08/14
 * First version
 */
#endregion

namespace DepthFactory.Editor
{
    public class SkinningSystem : EditorWindow
    {
        enum RepaintType { Texts, Images, Both }
        enum InfoType { visualName, endMessage }
        enum ErrorType { directoryInit, logoSpritePathNotInResources, iconSpritePathNotInResources, fontPathNotInResources }


        GUIStyle _titleHStyle;
        GUIStyle _subtitleHStyle;
        Texture2D _texWrong;
        Texture2D _texRight;
        GUIStyle _rightApplyBtnStyle;
        GUIStyle _wrongApplyBtnStyle;
        Vector2 _scrollPos_1;
        Vector2 _scrollPos_2;
        Vector2 _scrollPos_3;
        Vector2 _scrollPos_4;
        Dictionary<InfoType, string> _infos;
        Dictionary<ErrorType, string> _errors;
        SkinningSystem _instance;
        // Options
        string[] _skinsNames;
        string _lastSkinNameApplied;
        string _currentSkinName;
        int _currentSkinIndex;
        SkinData _data;
        bool _skinVisuals; // Numac option
        bool _skinPrefabs;

        // Colors
        Color _mainColor;
        Color _oppositeColor;
        Color _secondaryColor;
        Color _errorColor;
        int _numberOfAdditionalColors;

        // Fonts
        Font _generalFont;
        Font _titleFont;
        Font _secondaryFont;

        // Sprites
        int _numberOfIcons;

        // Localization
        string _currentNewPhrase;
        string _currentNewFrenchTranslation;
        string _currentPhraseButtonName;
        //LeanLocalization _localization;

        bool _needRepaintTexts;
        bool _needRepaintImages;

        int _numberSceneLoaded;
        RepaintType _repaintType;
        SkinManager _skinManager;
        #region Stats
        System.TimeSpan _timeRender;
        #endregion

        static string project;
        static string target;

        [MenuItem("DepthFactory/Edit skin")]
        static void MenuCreateRequestsFiles()
        {
            SkinningSystem window = (SkinningSystem)GetWindow(typeof(SkinningSystem), false, "Skinning system");
            window.minSize = new Vector2(1200, 600);
            window.Show();
        }

        static void SetContinousIntegrationSettings()
        {
            string[] commandLineArgs = System.Environment.GetCommandLineArgs();
            for (int i = 0; i < commandLineArgs.Length; i++)
            {
                if (commandLineArgs[i].Contains("project"))
                {
                    project = commandLineArgs[i + 1];
                }
                else if (commandLineArgs[i].Contains("target"))
                {
                    target = commandLineArgs[i + 1];
                }
            }
            EditorSceneManager.OpenScene("Assets/common/1. Scenes/MainV3.unity", OpenSceneMode.Single);
            MenuCreateRequestsFiles();
        }
        /*********************************************************/
        public static bool isCliInstanciated = false;
        void OnEnable()
        {
            //ScriptingSettings.Initialize();
            Selection.activeGameObject = null;
            _skinManager = SkinManager.Instance;
            //_localization = FindObjectOfType<LeanLocalization>();
           /* if (_localization == null)
            {
                GameObject lean = new GameObject("LeanLocalization");
                _localization = lean.AddComponent<LeanLocalization>();
                _localization.AddLanguage("French");
                _localization.DefaultLanguage = "French";
                _localization.AddCulture("French", "fr");
                _localization.AddCulture("French", "fr-FR");
                _localization.AddCulture("French", "fr-BE");
                _localization.AddCulture("French", "fr-CA");
                _localization.AddCulture("French", "fr-CH");
                _localization.AddCulture("French", "fr-LU");
                _localization.AddCulture("French", "fr-MC");
            }*/

            UpdateSkinsNameList();
            if (!string.IsNullOrEmpty(project))
            {
                /*string projectskin = project;
                Dictionary<string, OptionsData> options = new Dictionary<string, OptionsData>();

                List<Numac.OptionsData> pOptionsData = PersistentData.Instance.data.options;

                for (int u = 0; u < pOptionsData.Count; u++)
                {
                    options.Add(pOptionsData[u].name, pOptionsData[u]);
                }

                if (projectskin.Contains("Iteca"))
                    projectskin = "Iteca";
                else if (projectskin.Contains("Hennessy"))
                    projectskin = "Hennessy";
                else if (projectskin.Contains("MBDA"))
                    projectskin = "MBDA";
                switch (project)
                {
                    case "MBDA":
                        PersistentData.Instance.data.host = options.ElementAt(4).Value.url;
                        PersistentData.Instance.data.currentOptions = options["MBDA"];
                        break;
                    case "MBDA-Test":
                        PersistentData.Instance.data.host = options.ElementAt(4).Value.url;
                        PersistentData.Instance.data.currentOptions = options["MBDA Test"];
                        break;
                    case "Hennessy-Test":
                        PersistentData.Instance.data.host = options.ElementAt(2).Value.url;
                        PersistentData.Instance.data.currentOptions = options["Hennessy Test"];
                        break;
                    case "Hennessy-Dev":
                        PersistentData.Instance.data.host = options.ElementAt(1).Value.url;
                        PersistentData.Instance.data.currentOptions = options["Hennessy"];
                        break;
                    case "Hennessy-PreProd":
                        PersistentData.Instance.data.host = options.ElementAt(6).Value.url;
                        PersistentData.Instance.data.currentOptions = options["Hennessy PreProd"];
                        break;
                    case "Hennessy-Prod":
                        PersistentData.Instance.data.host = options.ElementAt(8).Value.url;
                        PersistentData.Instance.data.currentOptions = options["Hennessy Prod"];
                        break;
                    case "Iteca-Commercial":
                        PersistentData.Instance.data.host = options.ElementAt(5).Value.url;
                        PersistentData.Instance.data.currentOptions = options["Iteca commercial"];
                        break;
                    case "Iteca-Demo":
                        PersistentData.Instance.data.host = options.ElementAt(7).Value.url;
                        PersistentData.Instance.data.currentOptions = options["Iteca Demo"];
                        break;
                    case "Iteca-Interne":
                        PersistentData.Instance.data.host = options.ElementAt(9).Value.url;
                        PersistentData.Instance.data.currentOptions = options["Iteca Interne"];
                        break;
                }
                PersistentData.Instance.updateVersion();
                PersistentData.Instance.saveData();
                for (int i = 0; i < _skinsNames.Length; i++)
                {
                    if (projectskin == _skinsNames[i])
                    {

                        _currentSkinIndex = i;
                        isCliInstanciated = true;
                        break;
                    }
                }*/
            }
            InitializeSkinData();

            /*List<string> tempLocalizationPhraseNames = new List<string>();
            foreach (LeanPhrase translation in _localization.Phrases)
                tempLocalizationPhraseNames.Add(translation.Name);

            for (int i = 0; i < _data.translations.Count; i++)
            {
                string dataPhraseName = _data.translations[i].phraseName;
                string dataTranslation = _data.translations[i].translation;

                if (tempLocalizationPhraseNames.Contains(dataPhraseName))
                {
                    tempLocalizationPhraseNames.Remove(dataPhraseName);
                } //else
                //{
                    LeanPhrase leanPhrase = _localization.AddPhrase(dataPhraseName);
                    LeanTranslation leanTranslation = leanPhrase.AddTranslation("French");
                    leanTranslation.Text = dataTranslation;
               // }
            }*/

            _scrollPos_1 = new Vector2();
            _scrollPos_2 = new Vector2();
            _scrollPos_3 = new Vector2();
            _scrollPos_4 = new Vector2();
            _infos = new Dictionary<InfoType, string>
            {
                { InfoType.visualName, "Test" },
                { InfoType.endMessage, string.Empty }
            };
            _errors = new Dictionary<ErrorType, string>
            {
                { ErrorType.directoryInit, string.Empty },
                { ErrorType.logoSpritePathNotInResources, string.Empty },
                { ErrorType.iconSpritePathNotInResources, string.Empty },
                { ErrorType.fontPathNotInResources, string.Empty }
            };

            _needRepaintTexts = false;
            _needRepaintImages = false;

            _texWrong = new Texture2D(1, 1, TextureFormat.RGBA32, false);
            _texWrong.SetPixel(0, 0, new Color(149f / 255f, 43f / 255f, 43f / 255f));
            _texWrong.Apply();

            _texRight = new Texture2D(1, 1, TextureFormat.RGBA32, false);
            _texRight.SetPixel(0, 0, new Color(66f / 255f, 209f / 255f, 90f / 255f));
            _texRight.Apply();

            _currentPhraseButtonName = "Cr�er nouvelle phrase";

            /*if (Directory.Exists(ScriptingSettings.SkinningSystemResourcesFolderPath) == false)
            {
                _errors[ErrorType.directoryInit] = string.Format("Le dossier de ressources \"{0}\" n'existe pas. Checkez le path dans ScriptingSettings.", ScriptingSettings.SkinningSystemResourcesFolderPath);
            }*/
        }

        public static void BuildAndroid()
        {
            string[] levels = { "Assets/common/1. Scenes/Mobile/MobileLogin.unity", "Assets/common/1. Scenes/Mobile/AppMobile.unity" };
            BuildPipeline.BuildPlayer(levels, "build.apk", BuildTarget.Android, BuildOptions.None);
        }

        /*void ContinousIntegrationStart()
        {
            _repaintType = RepaintType.Both;
            //this.StartCoroutine(WaitCoroutine());
            //this.StartCoroutine(InstantiateVisualsCoroutine(RepaintType.Both));
        }
        /*********************************************************/

        void OnGUI()
        {
            CreateStyles();

            EditorGUILayout.BeginVertical();
            {
                EditorGUILayout.LabelField("<b>GESTIONNAIRE DE SKINS</b>", _titleHStyle, GUILayout.Height(50));
                DisplayMessage(_errors[ErrorType.directoryInit], MessageType.Error);
                //DisplayApplyButton();
                SaveButton();
                SkinSelection();

                GUISeparatorH();

                EditorGUILayout.BeginHorizontal();
                {
                    EditorGUILayout.BeginVertical(GUILayout.Height(position.height), GUILayout.Width(position.width / 4 - 20));
                    {
                        GUILayout.Space(10);
                        EditorGUILayout.LabelField("<b>POLICES</b>", _subtitleHStyle);
                        GUILayout.Space(10);
                        ChooseFonts();

                        GUISeparatorH();

                        GUILayout.Space(10);
                        EditorGUILayout.LabelField("<b>COULEURS</b>", _subtitleHStyle);
                        GUILayout.Space(10);
                        ChooseColors();
                    }
                    EditorGUILayout.EndVertical();
                }
                EditorGUILayout.EndHorizontal();
            }
            EditorGUILayout.EndVertical();
        }
        /*********************************************************/

        void OnDisable()
        {
            //UnityEngine.Debug.Log(_data.ToJSON());
            //File.WriteAllText(_skinManager.ActiveSkinJSONFile, _data.ToJSON());
            _skinManager.ActiveSkinName = _lastSkinNameApplied;
        }
        /*********************************************************/

        void InitializeSkinData()
        {
            _currentSkinName = _skinManager.ActiveSkinName;
            _data = _skinManager.SkinData;
        }
        /*********************************************************/

        /*void DisplayApplyButton()
        {
            if (_needRepaintTexts || _needRepaintImages)
            {
                if (_needRepaintTexts && _needRepaintImages == false)
                {
                    if (string.IsNullOrEmpty(_errors[ErrorType.iconSpritePathNotInResources]) && string.IsNullOrEmpty(_errors[ErrorType.iconSpritePathNotInResources])
                        && string.IsNullOrEmpty(_errors[ErrorType.fontPathNotInResources]))
                    {
                        if (GUI.Button(new Rect(position.width - 85, 5, 80, 50), "Save&Apply\ntexts"))
                        {
                            _repaintType = RepaintType.Texts;
                            this.StartCoroutine(InstantiateVisualsCoroutine(RepaintType.Texts));
                        }
                    }
                    else
                    {
                        if (GUI.Button(new Rect(position.width - 85, 5, 80, 50), "Save&Apply\ntexts", _wrongApplyBtnStyle)) { }
                    }
                }
                else if (_needRepaintImages && _needRepaintTexts == false)
                {
                    if (string.IsNullOrEmpty(_errors[ErrorType.iconSpritePathNotInResources]) && string.IsNullOrEmpty(_errors[ErrorType.iconSpritePathNotInResources])
                        && string.IsNullOrEmpty(_errors[ErrorType.fontPathNotInResources]))
                    {
                        if (GUI.Button(new Rect(position.width - 85, 5, 80, 50), "Save&Apply\nimages"))
                        {
                            _repaintType = RepaintType.Images;
                            this.StartCoroutine(InstantiateVisualsCoroutine(RepaintType.Images));
                        }
                    }
                    else
                    {
                        if (GUI.Button(new Rect(position.width - 85, 5, 80, 50), "Save&Apply\nimages", _wrongApplyBtnStyle)) { }
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(_errors[ErrorType.iconSpritePathNotInResources]) && string.IsNullOrEmpty(_errors[ErrorType.iconSpritePathNotInResources])
                        && string.IsNullOrEmpty(_errors[ErrorType.fontPathNotInResources]))
                    {
                        if (GUI.Button(new Rect(position.width - 85, 5, 80, 50), "Save&Apply\ncomponents"))
                        {
                            _repaintType = RepaintType.Both;
                            this.StartCoroutine(InstantiateVisualsCoroutine(RepaintType.Both));
                        }
                    }
                    else
                    {
                        if (GUI.Button(new Rect(position.width - 85, 5, 80, 50), "Save&Apply\ncomponents", _wrongApplyBtnStyle)) { }
                    }
                }
            }
        }
        /*********************************************************/

        /*void ApplyButton(RepaintType a_type)
        {
            _lastSkinNameApplied = _skinManager.ActiveSkinName;

            switch (a_type)
            {
                case RepaintType.Texts:
                    //UnityEngine.Debug.Log("Repaint every texts!");
                    _skinManager.SelectEveryTexts();
                    break;
                case RepaintType.Images:
                    //UnityEngine.Debug.Log("Repaint every images!");
                    _skinManager.SelectEveryImages();
                    break;
                case RepaintType.Both:
                    //UnityEngine.Debug.Log("Repaint all components!");
                    _skinManager.SelectAllComponents();
                    break;
            }

            if (a_type == RepaintType.Texts || a_type == RepaintType.Both)
                _needRepaintTexts = false;
            if (a_type == RepaintType.Images || a_type == RepaintType.Both)
                _needRepaintImages = false;

            //UnityEngine.Debug.Log(_data.ToJSON());
            File.WriteAllText(_skinManager.ActiveSkinJSONFile, _data.ToJSON());

            Close();
        }
        /*********************************************************/

        void SaveButton()
        {
            if (GUI.Button(new Rect(5, 5, 80, 50), "Save"))
            {
                //UnityEngine.Debug.Log(_data.ToJSON());
                //File.WriteAllText(_skinManager.ActiveSkinJSONFile, _data.ToJSON());
            }
        }
        /*********************************************************/
        IEnumerator WaitCoroutine()
        {
            yield return new WaitForSeconds(5.0f);
        }

        /*IEnumerator InstantiateVisualsCoroutine(RepaintType a_type)
        {
            GameObject canvasGO = new GameObject("Prefabs canvas");
            Canvas canvas = canvasGO.AddComponent<Canvas>();

            if (_skinPrefabs)
            {
                canvas.renderMode = RenderMode.ScreenSpaceOverlay;
                canvasGO.AddComponent<UnityEngine.UI.CanvasScaler>();

                for (int i = 0; i < _data.prefabs.Count; i++)
                {
                    GameObject prefab = _data.prefabs[i];
                    if (prefab != null)
                    {
                        GameObject prefabInstance = PrefabUtility.InstantiatePrefab(prefab) as GameObject;
                        prefabInstance.transform.SetParent(canvasGO.transform);
                    }
                }
            }

            _skinManager.DisplayProgressWindow();
            if (_skinVisuals)
            {
                _skinManager.ResetNumberOfVisuals();
                _skinManager.LaunchVisualState();
                for (int i = 0; i < VisualObject.visualObjects.visuals.Count; i++)
                {
                    if (VisualObject.visualObjects.visuals[i].id == -1)
                    {
                        _skinManager.IncNumberOfVisual(VisualObject.visualObjects.visuals[i].name);
                        continue;
                    }

                    GameObject visualPrefab = VisualObject.visualObjects.visuals[i].prefab;
                    if (visualPrefab == null)
                    {
                        Debug.LogError("Missing visual prefab for : " + VisualObject.visualObjects.visuals[i].name, VisualObject.visualObjects);
                    }
                    GameObject prefab = PrefabUtility.InstantiatePrefab(visualPrefab) as GameObject;
                    prefab.transform.SetParent(canvasGO.transform);
                    prefab.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
                    prefab.GetComponent<RectTransform>().sizeDelta = Vector2.zero;
                    //UnityEngine.Debug.Log(prefab.name);
                    PrefabUtility.ReplacePrefab(prefab, PrefabUtility.GetPrefabParent(prefab), ReplacePrefabOptions.ConnectToPrefab);
                    _skinManager.IncNumberOfVisual(prefab.name);
                    yield return new WaitForSeconds(0.2f);
                }

                EditorSceneManager.OpenScene("Assets/common/1. Scenes/BackOffice/BO.unity", OpenSceneMode.Additive);
            }

            //for (int i = 0; i < EditorBuildSettings.scenes.Length; i++)
            //    if (EditorBuildSettings.scenes[i].enabled)
            //       EditorSceneManager.OpenScene(EditorBuildSettings.scenes[i].path, OpenSceneMode.Additive);

            yield return new WaitForFixedUpdate();

            ApplyButton(a_type);
        }
        /*********************************************************/

        /*IEnumerator OpenSceneCoroutine(string a_path)
        {
            EditorSceneManager.OpenScene(a_path, OpenSceneMode.Additive);
            yield return null;
            _numberSceneLoaded++;
        }
        /*********************************************************/

        void SkinSelection()
        {
            EditorGUILayout.BeginVertical();
            {
                EditorGUILayout.BeginHorizontal();
                {
                    _currentSkinIndex = EditorGUILayout.Popup(_currentSkinIndex, _skinsNames);
                    if (_skinsNames[_currentSkinIndex] != _skinManager.ActiveSkinName)
                    {
                        _skinManager.ActiveSkinName = _skinsNames[_currentSkinIndex];
                        InitializeSkinData();
                        _needRepaintTexts = true;
                        _needRepaintImages = true;
                    }

                    if (GUILayout.Button("Add new skin"))
                    {
                        File.Create(Path.Combine(Application.streamingAssetsPath, "Skins/skin_" + (_skinsNames.Length + 1).ToString("00") + ".json")).Dispose();

                        //this.StartCoroutine(WaitAndUpdateSkinNames());
                    }
                }
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                {
                    //EditorGUILayout.LabelField("Base de donn�es s�lectionn�e : " + PersistentData.Instance.data.currentOptions.name);
                    _skinPrefabs = EditorGUILayout.Toggle("Skinner les prefabs", _skinPrefabs);
                    _skinVisuals = EditorGUILayout.Toggle("Skinner les visuels", _skinVisuals);
                }
                EditorGUILayout.EndHorizontal();
                // Part to rename a skin, didn't work well
                /*EditorGUILayout.BeginHorizontal();
                _currentSkinName = EditorGUILayout.TextField("New name skin: ", _currentSkinName);
                if (GUILayout.Button("Rename skin"))
                {
                    File.Create(Application.streamingAssetsPath + "/Skins/" + _currentSkinName + ".json").Dispose();
                    File.Create(Application.streamingAssetsPath + "/Skins/" + SkinManager.Instance.ActiveSkinName + "_backup.json").Dispose();
                    File.Replace(SkinManager.Instance.ActiveSkinJSONFile, Application.streamingAssetsPath + "/Skins/" + _currentSkinName + ".json", Application.streamingAssetsPath + "/Skins/" + SkinManager.Instance.ActiveSkinName + "_backup.json");
                    File.Delete(SkinManager.Instance.ActiveSkinJSONFile);
                    File.Delete(Application.streamingAssetsPath + "/Skins/" + SkinManager.Instance.ActiveSkinName + ".json.meta");
                    File.Delete(Application.streamingAssetsPath + "/Skins/" + SkinManager.Instance.ActiveSkinName + "_backup.json");

                    SkinManager.Instance.ActiveSkinName = _currentSkinName;
                    InitializeSkinData();
                }
                EditorGUILayout.EndHorizontal();*/
            }
            EditorGUILayout.EndVertical();
        }
        /*********************************************************/

        void ChooseColors()
        {
            Color tempColor;

            tempColor = _data.mainColor;
            _data.mainColor = EditorGUILayout.ColorField("Couleur dominante", _data.mainColor);
            if (tempColor != _data.mainColor)
            {
                _needRepaintTexts = true;
                _needRepaintImages = true;
            }
            tempColor = _data.complementaryColor;
            _data.complementaryColor = EditorGUILayout.ColorField("Couleur oppos�e", _data.complementaryColor);
            if (tempColor != _data.complementaryColor)
            {
                _needRepaintTexts = true;
                _needRepaintImages = true;
            }
            tempColor = _data.secondaryColor;
            _data.secondaryColor = EditorGUILayout.ColorField("Couleur secondaire", _data.secondaryColor);
            if (tempColor != _data.secondaryColor)
            {
                _needRepaintTexts = true;
                _needRepaintImages = true;
            }
            tempColor = _data.errorColor;
            _data.errorColor = EditorGUILayout.ColorField("Couleur d'erreur", _data.errorColor);
            if (tempColor != _data.errorColor)
            {
                _needRepaintTexts = true;
                _needRepaintImages = true;
            }
            GUILayout.Space(10);

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Nombre de couleurs additionnelles: " + _numberOfAdditionalColors);
            if (GUILayout.Button(new GUIContent("+"), GUILayout.Width(20)))
                _numberOfAdditionalColors++;
            EditorGUILayout.EndHorizontal();

            _scrollPos_2 = GUILayout.BeginScrollView(_scrollPos_2, GUILayout.Height(position.height - 460));
            GUILayout.EndScrollView();
        }
        /*********************************************************/

        void ChooseFonts()
        {
            Font tempFont;
            _errors[ErrorType.fontPathNotInResources] = string.Empty;

            tempFont = _data.generalFont;
            _data.generalFont = EditorGUILayout.ObjectField("Police principale", _data.generalFont, typeof(Font)) as Font;
            if (AssetDatabase.GetAssetPath(_data.generalFont).Contains("Resources") == false && _data.generalFont != null)
                _errors[ErrorType.fontPathNotInResources] = string.Format("La font \"{0}\" n'est pas dans les Resources. Utilisez une font situ�e dans les dossiers Resources.", _data.generalFont.name);
            if (tempFont != _data.generalFont)
                _needRepaintTexts = true;

            DisplayMessage(_errors[ErrorType.fontPathNotInResources], MessageType.Error);
        }
        /*********************************************************/

        IEnumerator WaitAndUpdateSkinNames()
        {
            yield return new WaitForSeconds(1f); // TODO maybe this time can be reduced, need tests
            UpdateSkinsNameList();

            SkinData previousSkinData = _skinManager.SkinData;
            //UnityEngine.Debug.Log("01/ "  + _skinManager.SkinData.ToJSON());
            _skinManager.ActiveSkinName = "skin_" + (_skinsNames.Length).ToString("00");
            for (int i = 0; i < _skinsNames.Length; i++)
            {
                if (_skinsNames[i] == _skinManager.ActiveSkinName)
                {
                    _currentSkinIndex = i;
                    break;
                }
            }

            //UnityEngine.Debug.Log("02/ " + _skinManager.SkinData.ToJSON());
            InitializeSkinData();
        }
        /*********************************************************/

        void UpdateSkinsNameList()
        {
            _skinsNames = Directory.GetFiles(Path.Combine(Application.streamingAssetsPath, "Skins"));
            List<string> tempSkinNames = new List<string>();
            for (int i = 0; i < _skinsNames.Length; i++)
            {
                if (_skinsNames[i].Contains(".meta") == false && _skinsNames[i].Contains("Manager") == false)
                {
                    tempSkinNames.Add(Path.GetFileNameWithoutExtension(_skinsNames[i]));
                }
            }
            _skinsNames = new string[tempSkinNames.Count];
            _currentSkinIndex = 0;
            for (int j = 0; j < tempSkinNames.Count; j++)
            {
                if (tempSkinNames[j] == _skinManager.ActiveSkinName)
                {
                    _currentSkinIndex = j;
                    _lastSkinNameApplied = _skinManager.ActiveSkinName;
                }
                _skinsNames[j] = tempSkinNames[j];
            }
        }
        /*********************************************************/

        /// <summary>
        /// Create styles if they're not
        /// </summary>
        void CreateStyles()
        {
            if (_titleHStyle == null)
            {
                _titleHStyle = new GUIStyle("Label")
                {
                    alignment = TextAnchor.MiddleCenter,
                    fontSize = 20,
                    richText = true
                };
            }
            if (_subtitleHStyle == null)
            {
                _subtitleHStyle = new GUIStyle("Label")
                {
                    alignment = TextAnchor.MiddleCenter,
                    fontSize = 14,
                    richText = true
                };
            }

            if (_rightApplyBtnStyle == null)
            {
                _rightApplyBtnStyle = new GUIStyle(GUI.skin.button);
                _rightApplyBtnStyle.normal.background = _texRight;
            }
            if (_wrongApplyBtnStyle == null)
            {
                _wrongApplyBtnStyle = new GUIStyle(GUI.skin.button);
                _wrongApplyBtnStyle.normal.background = _texWrong;
            }
        }
        /*********************************************************/

        /// <summary>
        /// Draw a hozizontal separator
        /// </summary>
        void GUISeparatorH()
        {
            EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
        }
        /*********************************************************/

        /// <summary>
        /// Draw a vertical separator
        /// </summary>
        void GUISeparatorV()
        {
            EditorGUILayout.LabelField("", GUI.skin.verticalSlider, GUILayout.Height(position.height - 100), GUILayout.Width(20));
        }
        /*********************************************************/

        /// <summary>
        /// Display a message if it's not empty
        /// </summary>
        /// <param name="a_message"></param>
        /// <param name="a_type"></param>
        void DisplayMessage(string a_message, MessageType a_type)
        {
            if (string.IsNullOrEmpty(a_message) == false)
                EditorGUILayout.HelpBox(a_message, a_type);
        }
        /*********************************************************/
    }
}
#endif
