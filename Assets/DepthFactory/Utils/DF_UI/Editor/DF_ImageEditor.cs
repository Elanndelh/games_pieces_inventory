using DepthFactory.Editor;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEditor.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace DepthFactory.UI
{
    [CustomEditor(typeof(DF_Image))]
    [CanEditMultipleObjects]
    public class ItecaImageEditor : ImageEditor
    {
        SerializedProperty _useSkin;
        string[] _skinnedColorNames;
        int _currentSkinnedColorIndex;

        DF_Image DFComponent
        {
            get { return (DF_Image)target; }
        }


        protected override void OnEnable()
        {
            base.OnEnable();
            _useSkin = serializedObject.FindProperty("_useSkin");

            _skinnedColorNames = new string[SkinManager.Instance.SkinnedColors.Count + 1];
            _skinnedColorNames[0] = " --None-- ";
            int i = 1;
            foreach (KeyValuePair<string, Color> pair in SkinManager.Instance.SkinnedColors)
            {
                _skinnedColorNames[i] = pair.Key;
                i++;
            }
        }

        public override void OnInspectorGUI()
        {
            _currentSkinnedColorIndex = DFComponent.CurrentColorIndex;

            EditorGUILayout.PropertyField(_useSkin);
            DFComponent.UseSkin = _useSkin.boolValue;
            if (_useSkin.boolValue)
            {
                _currentSkinnedColorIndex = EditorGUILayout.Popup("Skinned Color", _currentSkinnedColorIndex, _skinnedColorNames, EditorStyles.popup);
                if (_currentSkinnedColorIndex > _skinnedColorNames.Length)
                {
                    DFComponent.UseSkin = false;
                    DFComponent.CurrentColorIndex = 0;
                }
                else
                {
                    if (_currentSkinnedColorIndex > 0)
                        DFComponent.color = SkinManager.Instance.SkinnedColors[_skinnedColorNames[_currentSkinnedColorIndex]];
                    if (DFComponent.CurrentColorIndex != _currentSkinnedColorIndex)
                        EditorSceneManager.MarkSceneDirty(SceneManager.GetActiveScene());
                    DFComponent.CurrentColorIndex = _currentSkinnedColorIndex;
                }

                UnityEditor.EditorUtility.SetDirty(DFComponent);
            }

            EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
            base.OnInspectorGUI();
            base.serializedObject.ApplyModifiedProperties();
        }
    }
}
