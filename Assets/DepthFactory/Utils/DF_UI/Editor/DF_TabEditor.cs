using DepthFactory.Editor;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEditor.UI;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace DepthFactory.UI
{
    [CustomEditor(typeof(DF_Tab))]
    [CanEditMultipleObjects]
    public class DF_TabEditor : GraphicEditor
    {
        SerializedProperty _useSkin;
        SerializedProperty _changeSubColor;
        SerializedProperty _subGraphicComponent;
        string[] _skinnedColorNames;
        int _standardSkinnedColorIndex;
        int _selectedSkinnedColorIndex;
        int _standardSubSkinnedColorIndex;
        int _selectedSubSkinnedColorIndex;

        DF_Tab DFComponent
        {
            get { return (DF_Tab)target; }
        }


        protected override void OnEnable()
        {
            base.OnEnable();

            _useSkin = serializedObject.FindProperty("_useSkin");
            _changeSubColor = serializedObject.FindProperty("_changeSubColor");
            _subGraphicComponent = serializedObject.FindProperty("_subGraphic");
            _skinnedColorNames = new string[SkinManager.Instance.SkinnedColors.Count + 1];
            _skinnedColorNames[0] = " --None-- ";
            int i = 1;
            foreach (KeyValuePair<string, Color> pair in SkinManager.Instance.SkinnedColors)
            {
                _skinnedColorNames[i] = pair.Key;
                i++;
            }
        }

        public override void OnInspectorGUI()
        {
            _standardSkinnedColorIndex = DFComponent.StandardColorIndex;
            _selectedSkinnedColorIndex = DFComponent.SelectedColorIndex;
            _standardSubSkinnedColorIndex = DFComponent.StandardSubColorIndex;
            _selectedSubSkinnedColorIndex = DFComponent.SelectedSubColorIndex;

            EditorGUILayout.PropertyField(_useSkin);
            DFComponent.UseSkin = _useSkin.boolValue;
            if (_useSkin.boolValue)
            {
                _standardSkinnedColorIndex = EditorGUILayout.Popup("Standard Skinned Color", _standardSkinnedColorIndex, _skinnedColorNames, EditorStyles.popup);
                if (_standardSkinnedColorIndex > _skinnedColorNames.Length)
                {
                    DFComponent.UseSkin = false;
                    DFComponent.StandardColorIndex = 0;
                }
                else
                {
                    if (_standardSkinnedColorIndex > 0 && DFComponent.ToggleComponent.isOn == false)
                        DFComponent.ImageComponent.color = SkinManager.Instance.SkinnedColors[_skinnedColorNames[_standardSkinnedColorIndex]];
                    if (DFComponent.StandardColorIndex != _standardSkinnedColorIndex)
                        EditorSceneManager.MarkSceneDirty(SceneManager.GetActiveScene());
                    DFComponent.StandardColorIndex = _standardSkinnedColorIndex;
                }

                _selectedSkinnedColorIndex = EditorGUILayout.Popup("Selected Skinned Color", _selectedSkinnedColorIndex, _skinnedColorNames, EditorStyles.popup);
                if (_selectedSkinnedColorIndex > _skinnedColorNames.Length)
                {
                    DFComponent.UseSkin = false;
                    DFComponent.SelectedColorIndex = 0;
                }
                else
                {
                    if (_selectedSkinnedColorIndex > 0 && DFComponent.ToggleComponent.isOn)
                        DFComponent.ImageComponent.color = SkinManager.Instance.SkinnedColors[_skinnedColorNames[_selectedSkinnedColorIndex]];
                    if (DFComponent.SelectedColorIndex != _selectedSkinnedColorIndex)
                        EditorSceneManager.MarkSceneDirty(SceneManager.GetActiveScene());
                    DFComponent.SelectedColorIndex = _selectedSkinnedColorIndex;
                }

                EditorGUILayout.PropertyField(_changeSubColor);
                DFComponent.ChangeSubColor = _changeSubColor.boolValue;
                if (_changeSubColor.boolValue)
                {
                    _subGraphicComponent.objectReferenceValue = EditorGUILayout.ObjectField("Sub Graphic", _subGraphicComponent.objectReferenceValue, typeof(Graphic), true);
                    DFComponent.SubGraphic = (Graphic)_subGraphicComponent.objectReferenceValue;

                    _standardSubSkinnedColorIndex = EditorGUILayout.Popup("Standard Sub Skinned Color", _standardSubSkinnedColorIndex, _skinnedColorNames, EditorStyles.popup);
                    if (_standardSubSkinnedColorIndex > _skinnedColorNames.Length)
                    {
                        DFComponent.UseSkin = false;
                        DFComponent.StandardSubColorIndex = 0;
                    }
                    else
                    {
                        if (_standardSubSkinnedColorIndex > 0 && DFComponent.ToggleComponent.isOn == false)
                            DFComponent.SubGraphic.color = SkinManager.Instance.SkinnedColors[_skinnedColorNames[_standardSubSkinnedColorIndex]];
                        if (DFComponent.StandardSubColorIndex != _standardSubSkinnedColorIndex)
                            EditorSceneManager.MarkSceneDirty(SceneManager.GetActiveScene());
                        DFComponent.StandardSubColorIndex = _standardSubSkinnedColorIndex;
                    }

                    _selectedSubSkinnedColorIndex = EditorGUILayout.Popup("Selected Sub Skinned Color", _selectedSubSkinnedColorIndex, _skinnedColorNames, EditorStyles.popup);
                    if (_selectedSubSkinnedColorIndex > _skinnedColorNames.Length)
                    {
                        DFComponent.UseSkin = false;
                        DFComponent.SelectedSubColorIndex = 0;
                    }
                    else
                    {
                        if (_selectedSubSkinnedColorIndex > 0 && DFComponent.ToggleComponent.isOn)
                            DFComponent.SubGraphic.color = SkinManager.Instance.SkinnedColors[_skinnedColorNames[_selectedSubSkinnedColorIndex]];
                        if (DFComponent.SelectedSubColorIndex != _selectedSubSkinnedColorIndex)
                            EditorSceneManager.MarkSceneDirty(SceneManager.GetActiveScene());
                        DFComponent.SelectedSubColorIndex = _selectedSubSkinnedColorIndex;
                    }
                }

                UnityEditor.EditorUtility.SetDirty(DFComponent);
            }

            EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
            base.serializedObject.ApplyModifiedProperties();
        }
    }
}
