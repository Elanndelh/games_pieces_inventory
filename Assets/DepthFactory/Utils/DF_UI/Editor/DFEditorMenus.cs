using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEditor;
using DepthFactory.UI;
using UnityEngine.UI;

namespace DepthFactory.Editor
{

    public class GameObjectMenus
    {
        const string c_StandardSpritePath = "UI/Skin/UISprite.psd";
        const string c_BackgroundSpritePath = "UI/Skin/Background.psd";
        const string c_InputFieldBackgroundPath = "UI/Skin/InputFieldBackground.psd";
        const string c_KnobPath = "UI/Skin/Knob.psd";
        const string c_CheckmarkPath = "UI/Skin/Checkmark.psd";
        const string c_DropdownArrowPath = "UI/Skin/DropdownArrow.psd";
        const string c_MaskPath = "UI/Skin/UIMask.psd";


        /*static Object GetPrefab(string path)
        {
            return AssetDatabase.LoadAssetAtPath<Object>("Assets/ItecaGUI/Prefabs/" + path + ".prefab");
        }

        static void AddPrefabToScene(string path, MenuCommand menuCommand)
        {
            GameObject go = (GameObject)PrefabUtility.InstantiatePrefab(GetPrefab(path));
            GameObjectUtility.SetParentAndAlign(go, menuCommand.context as GameObject);

            string uniqueName = GameObjectUtility.GetUniqueNameForSibling(go.transform.parent, path);
            go.name = uniqueName;

            Undo.RegisterCreatedObjectUndo(go, "Create " + go.name);
            Selection.activeGameObject = go;
        }*/

        [MenuItem("GameObject/DepthFactoryUI/DF_TMPText", false, 6)]
        public static void DF_Text(MenuCommand menuCommand)
        {
            CreateDF_Text(menuCommand);
        }
        static void CreateDF_Text(MenuCommand menuCommand)
        {
            GameObject go = new GameObject();
            GameObjectUtility.SetParentAndAlign(go, menuCommand.context as GameObject);

            go.name = "DF_TMPText";
            go.AddComponent<CanvasRenderer>();

            TMPro.TextMeshProUGUI tmp = go.AddComponent<TMPro.TextMeshProUGUI>();
            DF_TMPText component = go.AddComponent<DF_TMPText>();

            component.TextComponent = tmp;
            tmp.text = "New Text";
            tmp.richText = false;
            tmp.raycastTarget = false;

            RectTransform rt = go.GetComponent<RectTransform>();
            rt.sizeDelta = new Vector2(160, 30);

            Undo.RegisterCreatedObjectUndo(go, "Create " + go.name);
            Selection.activeGameObject = go;
        }

        [MenuItem("GameObject/DepthFactoryUI/DF_Image", false, 7)]
        public static void DF_Image(MenuCommand menuCommand)
        {
            CreateDF_Image(menuCommand);
        }
        static void CreateDF_Image(MenuCommand menuCommand)
        {
            GameObject go = new GameObject();
            GameObjectUtility.SetParentAndAlign(go, menuCommand.context as GameObject);

            go.name = "DF_Image";
            DF_Image component = go.AddComponent<DF_Image>();
            component.raycastTarget = false;

            RectTransform rt = go.GetComponent<RectTransform>();
            rt.sizeDelta = new Vector2(100, 100);

            Undo.RegisterCreatedObjectUndo(go, "Create " + go.name);
            Selection.activeGameObject = go;
        }

        /*[MenuItem("GameObject/DepthFactoryUI/ItecaRawImage", false, 8)]
        public static void ItecaRawImage(MenuCommand menuCommand)
        {
            CreateItecaRawImage(menuCommand);
        }
        static void CreateItecaRawImage(MenuCommand menuCommand)
        {
            GameObject go = new GameObject();
            GameObjectUtility.SetParentAndAlign(go, menuCommand.context as GameObject);

            go.name = "ItecaRawImage";
            ItecaRawImage component = go.AddComponent<ItecaRawImage>();
            component.raycastTarget = false;

            RectTransform rt = go.GetComponent<RectTransform>();
            rt.sizeDelta = new Vector2(100, 100);

            Undo.RegisterCreatedObjectUndo(go, "Create " + go.name);
            Selection.activeGameObject = go;
        }*/

        /*[MenuItem("GameObject/DepthFactoryUI/ItecaButtonText", false, 30)]
        public static void ButtonText(MenuCommand menuCommand)
        {
            CreateItecaButtonText(menuCommand);
        }
        static void CreateItecaButtonText(MenuCommand menuCommand)
        {
            GameObject go = new GameObject();
            GameObjectUtility.SetParentAndAlign(go, menuCommand.context as GameObject);

            go.name = "ItecaButton";
            DF_Image image = go.AddComponent<DF_Image>();
            image.raycastTarget = true;
            image.sprite = AssetDatabase.GetBuiltinExtraResource<Sprite>(c_StandardSpritePath);
            image.type = Image.Type.Sliced;
            image.fillCenter = true;

            RectTransform rt = go.GetComponent<RectTransform>();
            rt.sizeDelta = new Vector2(160, 30);

            ItecaButton button = go.AddComponent<ItecaButton>();
            button.targetGraphic = image;

            GameObject child = new GameObject();
            GameObjectUtility.SetParentAndAlign(child, go);

            child.name = "ButtonText";
            DF_Text childText = child.AddComponent<DF_Text>();
            childText.text = "Button";
            childText.color = Color.black;
            childText.alignment = TextAnchor.MiddleCenter;
            childText.supportRichText = false;
            childText.raycastTarget = false;

            RectTransform childRT = child.GetComponent<RectTransform>();
            childRT.anchorMin = Vector2.zero;
            childRT.anchorMax = Vector2.one;
            childRT.sizeDelta = Vector2.zero;

            button.SetTextComponent(childText);


            Undo.RegisterCreatedObjectUndo(go, "Create " + go.name);
            Selection.activeGameObject = go;
        }*/
        /*[MenuItem("GameObject/DepthFactoryUI/ItecaButtonImage", false, 30)]
        public static void ButtonImage(MenuCommand menuCommand)
        {
            CreateItecaButtonImage(menuCommand);
        }
        static void CreateItecaButtonImage(MenuCommand menuCommand)
        {
            GameObject go = new GameObject();
            GameObjectUtility.SetParentAndAlign(go, menuCommand.context as GameObject);

            go.name = "ItecaButton";
            DF_Image image = go.AddComponent<DF_Image>();
            image.raycastTarget = true;
            image.sprite = AssetDatabase.GetBuiltinExtraResource<Sprite>(c_StandardSpritePath);
            image.type = Image.Type.Sliced;
            image.fillCenter = true;

            RectTransform rt = go.GetComponent<RectTransform>();
            rt.sizeDelta = new Vector2(160, 30);

            ItecaButton button = go.AddComponent<ItecaButton>();
            button.targetGraphic = image;

            GameObject child = new GameObject();
            GameObjectUtility.SetParentAndAlign(child, go);

            child.name = "Icon";
            DF_Image childImage = child.AddComponent<DF_Image>();
            childImage.raycastTarget = false;

            RectTransform childRT = child.GetComponent<RectTransform>();
            childRT.anchorMin = Vector2.zero;
            childRT.anchorMax = Vector2.one;
            childRT.sizeDelta = Vector2.zero;

            button.SetIcon(childImage);


            Undo.RegisterCreatedObjectUndo(go, "Create " + go.name);
            Selection.activeGameObject = go;
        }*/

        /*[MenuItem("GameObject/DepthFactoryUI/ItecaInputField", false, 31)]
        public static void InputField(MenuCommand menuCommand)
        {
            CreateItecaInputField(menuCommand);
        }
        // only basic inputfield
        // TODO: add virtual keyboard options and dropdown option
        static void CreateItecaInputField(MenuCommand menuCommand)
        {
            GameObject go = new GameObject();
            GameObjectUtility.SetParentAndAlign(go, menuCommand.context as GameObject);
            go.name = "ItecaInputField";

            DF_Image image = go.AddComponent<DF_Image>();
            image.raycastTarget = true;
            image.sprite = AssetDatabase.GetBuiltinExtraResource<Sprite>(c_InputFieldBackgroundPath);
            image.type = Image.Type.Sliced;
            image.fillCenter = true;

            RectTransform rt = go.GetComponent<RectTransform>();
            rt.sizeDelta = new Vector2(160, 30);

            ItecaInputField component = go.AddComponent<ItecaInputField>();
            component.targetGraphic = image;
            component.enabled = true;

            GameObject child_1 = new GameObject();
            GameObjectUtility.SetParentAndAlign(child_1, go);
            child_1.name = "Placeholder";
            DF_Text childText = child_1.AddComponent<DF_Text>();
            childText.text = "Enter text...";
            childText.color = Color.grey;
            childText.alignment = TextAnchor.MiddleLeft;
            childText.fontStyle = FontStyle.Italic;
            childText.supportRichText = false;
            childText.raycastTarget = false;
            RectTransform childRT = child_1.GetComponent<RectTransform>();
            childRT.anchorMin = Vector2.zero;
            childRT.anchorMax = Vector2.one;
            childRT.sizeDelta = new Vector2(-20, -12);

            component.Placeholder = childText;

            GameObject child_2 = new GameObject();
            GameObjectUtility.SetParentAndAlign(child_2, go);
            child_2.name = "Text";
            DF_Text childText_2 = child_2.AddComponent<DF_Text>();
            childText_2.text = string.Empty;
            childText_2.color = Color.black;
            childText_2.alignment = TextAnchor.MiddleLeft;
            childText_2.supportRichText = false;
            childText_2.raycastTarget = false;
            childRT = child_2.GetComponent<RectTransform>();
            childRT.anchorMin = Vector2.zero;
            childRT.anchorMax = Vector2.one;
            childRT.sizeDelta = new Vector2(-20, -20);

            component.textComponent = childText_2;

            Undo.RegisterCreatedObjectUndo(go, "Create " + go.name);
            Selection.activeGameObject = go;
        }*/

        /*[MenuItem("GameObject/DepthFactoryUI/ItecaToggle", false, 32)]
        public static void ItecaToggle(MenuCommand menuCommand)
        {
            CreateItecaToggle(menuCommand);
        }
        static void CreateItecaToggle(MenuCommand menuCommand)
        {
            GameObject go = new GameObject();
            GameObjectUtility.SetParentAndAlign(go, menuCommand.context as GameObject);
            go.name = "ItecaToggle";

            RectTransform rt = go.AddComponent<RectTransform>();
            rt.sizeDelta = new Vector2(160, 20);

            ItecaToggle component = go.AddComponent<ItecaToggle>();
            component.enabled = true;

            GameObject child_1 = new GameObject();
            GameObjectUtility.SetParentAndAlign(child_1, go);
            child_1.name = "Background";
            DF_Image image = child_1.AddComponent<DF_Image>();
            image.raycastTarget = true;
            image.sprite = AssetDatabase.GetBuiltinExtraResource<Sprite>(c_StandardSpritePath);
            image.type = Image.Type.Sliced;
            image.fillCenter = true;
            RectTransform childRT = child_1.GetComponent<RectTransform>();
            childRT.localPosition = new Vector2(-70, 0);
            childRT.anchorMin = new Vector2(0, 1);
            childRT.anchorMax = new Vector2(0, 1);
            childRT.anchoredPosition = new Vector2(10, -10);
            childRT.sizeDelta = new Vector2(20, 20);

            component.targetGraphic = image;

            GameObject child_1_1 = new GameObject();
            GameObjectUtility.SetParentAndAlign(child_1_1, child_1);
            child_1_1.name = "Checkmark";
            DF_Image childImage_2 = child_1_1.AddComponent<DF_Image>();
            childImage_2.raycastTarget = false;
            childImage_2.sprite = AssetDatabase.GetBuiltinExtraResource<Sprite>(c_CheckmarkPath);
            childRT = child_1_1.GetComponent<RectTransform>();
            childRT.sizeDelta = new Vector2(20, 20);

            component.graphic = childImage_2;

            GameObject child_2 = new GameObject();
            GameObjectUtility.SetParentAndAlign(child_2, go);
            child_2.name = "Label";
            DF_Text childText_2 = child_2.AddComponent<DF_Text>();
            childText_2.text = "Toggle";
            childText_2.color = Color.black;
            childText_2.alignment = TextAnchor.MiddleLeft;
            childText_2.supportRichText = false;
            childText_2.raycastTarget = false;
            childRT = child_2.GetComponent<RectTransform>();
            childRT.localPosition = new Vector2(9, -0.5f);
            childRT.anchorMin = Vector2.zero;
            childRT.anchorMax = Vector2.one;
            childRT.anchoredPosition = new Vector2(9, -0.5f);
            childRT.sizeDelta = new Vector2(-28, -3);


            Undo.RegisterCreatedObjectUndo(go, "Create " + go.name);
            Selection.activeGameObject = go;
        }*/

        /*[MenuItem("GameObject/DepthFactoryUI/ItecaScrollbar", false, 33)]
        public static void ItecaScrollbar(MenuCommand menuCommand)
        {
            CreateItecaScrollbar(menuCommand);
        }
        static void CreateItecaScrollbar(MenuCommand menuCommand)
        {
            GameObject go = new GameObject();
            GameObjectUtility.SetParentAndAlign(go, menuCommand.context as GameObject);

            go.name = "ItecaScrollbar";
            DF_Image image = go.AddComponent<DF_Image>();
            image.sprite = AssetDatabase.GetBuiltinExtraResource<Sprite>(c_BackgroundSpritePath);
            image.raycastTarget = true;
            image.type = Image.Type.Sliced;
            image.fillCenter = true;
            image.raycastTarget = true;
            RectTransform rt = go.GetComponent<RectTransform>();
            rt.sizeDelta = new Vector2(160, 20);
            ItecaScrollbar component = go.AddComponent<ItecaScrollbar>();
            component.targetGraphic = image;

            GameObject child_1 = new GameObject();
            GameObjectUtility.SetParentAndAlign(child_1, go);
            child_1.name = "Sliding Area";
            RectTransform childRT = child_1.AddComponent<RectTransform>();
            childRT.anchorMin = Vector2.zero;
            childRT.anchorMax = Vector2.one;
            childRT.sizeDelta = new Vector2(-20, -20);

            GameObject child_1_1 = new GameObject();
            GameObjectUtility.SetParentAndAlign(child_1_1, child_1);
            child_1_1.name = "Handle";
            RectTransform child_1_1RT = child_1_1.AddComponent<RectTransform>();
            child_1_1RT.anchorMin = Vector2.zero;
            child_1_1RT.anchorMax = new Vector2(0.2f, 1);
            child_1_1RT.sizeDelta = new Vector2(20, 20);
            DF_Image child_1_1Image = child_1_1.AddComponent<DF_Image>();
            child_1_1Image.sprite = AssetDatabase.GetBuiltinExtraResource<Sprite>(c_StandardSpritePath);
            child_1_1Image.raycastTarget = true;
            child_1_1Image.type = Image.Type.Sliced;
            child_1_1Image.fillCenter = true;
            component.handleRect = child_1_1RT;

            Undo.RegisterCreatedObjectUndo(go, "Create " + go.name);
            Selection.activeGameObject = go;
        }*/

        /*[MenuItem("GameObject/DepthFactoryUI/ItecaSlider", false, 34)]
        public static void ItecaSlider(MenuCommand menuCommand)
        {
            CreateItecaSlider(menuCommand);
        }
        static void CreateItecaSlider(MenuCommand menuCommand)
        {
            GameObject go = new GameObject();
            GameObjectUtility.SetParentAndAlign(go, menuCommand.context as GameObject);

            go.name = "ItecaSlider";
            RectTransform rt = go.AddComponent<RectTransform>();
            rt.sizeDelta = new Vector2(160, 20);
            ItecaSlider component = go.AddComponent<ItecaSlider>();

            GameObject child_1 = new GameObject();
            GameObjectUtility.SetParentAndAlign(child_1, go);
            child_1.name = "Background";
            RectTransform childRT = child_1.AddComponent<RectTransform>();
            childRT.anchorMin = new Vector2(0, 0.25f);
            childRT.anchorMax = new Vector2(1, 0.75f);
            childRT.sizeDelta = Vector2.zero;
            DF_Image child_1Image = child_1.AddComponent<DF_Image>();
            child_1Image.sprite = AssetDatabase.GetBuiltinExtraResource<Sprite>(c_BackgroundSpritePath);
            child_1Image.raycastTarget = true;
            child_1Image.type = Image.Type.Sliced;
            child_1Image.fillCenter = true;
            child_1Image.raycastTarget = true;

            GameObject child_2 = new GameObject();
            GameObjectUtility.SetParentAndAlign(child_2, go);
            child_2.name = "Fill Area";
            RectTransform child_2RT = child_2.AddComponent<RectTransform>();
            child_2RT.anchorMin = new Vector2(0, 0.25f);
            child_2RT.anchorMax = new Vector2(1, 0.75f);
            child_2RT.anchoredPosition = new Vector2(-5, 0);
            child_2RT.sizeDelta = new Vector2(-20, 0);

            GameObject child_2_1 = new GameObject();
            GameObjectUtility.SetParentAndAlign(child_2_1, child_2);
            child_2_1.name = "Fill";
            RectTransform child_2_1RT = child_2_1.AddComponent<RectTransform>();
            child_2_1RT.anchorMin = Vector2.zero;
            child_2_1RT.anchorMax = new Vector2(0, 1);
            child_2_1RT.sizeDelta = new Vector2(10, 0);
            DF_Image child_2_1Image = child_2_1.AddComponent<DF_Image>();
            child_2_1Image.sprite = AssetDatabase.GetBuiltinExtraResource<Sprite>(c_StandardSpritePath);
            child_2_1Image.raycastTarget = true;
            child_2_1Image.type = Image.Type.Sliced;
            child_2_1Image.fillCenter = true;
            component.fillRect = child_2_1RT;

            GameObject child_3 = new GameObject();
            GameObjectUtility.SetParentAndAlign(child_3, go);
            child_3.name = "Handle Slide Area";
            RectTransform child_3RT = child_3.AddComponent<RectTransform>();
            child_3RT.anchorMin = Vector2.zero;
            child_3RT.anchorMax = Vector2.one;
            child_3RT.sizeDelta = new Vector2(-20, 0);

            GameObject child_3_1 = new GameObject();
            GameObjectUtility.SetParentAndAlign(child_3_1, child_3);
            child_3_1.name = "Handle";
            RectTransform child_3_1RT = child_3_1.AddComponent<RectTransform>();
            child_3_1RT.anchorMin = Vector2.zero;
            child_3_1RT.anchorMax = new Vector2(0, 1);
            child_3_1RT.sizeDelta = new Vector2(20, 0);
            DF_Image child_3_1Image = child_3_1.AddComponent<DF_Image>();
            child_3_1Image.sprite = AssetDatabase.GetBuiltinExtraResource<Sprite>(c_KnobPath);
            child_3_1Image.raycastTarget = true;
            child_3_1Image.type = Image.Type.Simple;
            child_3_1Image.preserveAspect = true;
            component.targetGraphic = child_3_1Image;
            component.handleRect = child_3_1RT;

            Undo.RegisterCreatedObjectUndo(go, "Create " + go.name);
            Selection.activeGameObject = go;
        }*/

        /*[MenuItem("GameObject/DepthFactoryUI/ItecaScrollView", false, 35)]
        public static void ItecaScrollView(MenuCommand menuCommand)
        {
            AddPrefabToScene("ItecaScrollView", menuCommand);
            // Tr -2020-09-07: Lors de la cr�ation, penser � ajouter un Canvas et un GraphicRaycaster
        }*/

        /*[MenuItem("GameObject/DepthFactoryUI/ItecaDropdown", false, 36)]
        public static void ItecaDropdown(MenuCommand menuCommand)
        {
            AddPrefabToScene("ItecaDropdown", menuCommand);
        }*/

        /*[MenuItem("GameObject/DepthFactoryUI/ItecaAutoCompleteDropdown", false, 37)]
        public static void ItecaAutoCompleteDropdown(MenuCommand menuCommand)
        {
            AddPrefabToScene("ItecaAutoCompleteDropdown", menuCommand);
        }*/
    }
}
