#if UNITY_EDITOR
using DepthFactory.Editor;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEditor.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace DepthFactory.UI
{
    [CustomEditor(typeof(DF_TMPText))]
    [CanEditMultipleObjects]
    public class DF_TMPTextEditor : GraphicEditor
    {
        SerializedProperty _useSkin;
        string[] _skinnedColorNames;
        int _currentSkinnedColorIndex;
        //string[] _skinnedFontNames;
        //int _currentSkinnedFontIndex;
        bool _isUpper;
        bool _isLower;
        string _textWithoutCase;
        bool _useHyperlink;
        bool _doesColorChangeOnHover;
        int _hoverSkinnedColorIndex;
        Color _hoverColor;

        SerializedProperty _useLocalization;
        string[] _localizationKeys;
        int _localizationIndex;

        DF_TMPText DFComponent
        {
            get { return (DF_TMPText)target; }
        }


        protected override void OnEnable()
        {
            base.OnEnable();

            _useSkin = serializedObject.FindProperty("_useSkin");
            _skinnedColorNames = new string[SkinManager.Instance.SkinnedColors.Count + 1];
            _skinnedColorNames[0] = " --None-- ";
            int i = 1;
            foreach (KeyValuePair<string, Color> pair in SkinManager.Instance.SkinnedColors)
            {
                _skinnedColorNames[i] = pair.Key;
                i++;
            }

            _useLocalization = serializedObject.FindProperty("_useLocalization");
            /*_localizationKeys = new string[LocalizationManager.Instance.Phrases.Count + 1];
            _localizationKeys[0] = " --None-- ";
            int j = 1;
            foreach (LocalizationPhrase phrase in LocalizationManager.Instance.Phrases)
            {
                _localizationKeys[j] = phrase.key;
                j++;
            }*/

            /*int j = 0;
            _skinnedFontNames = new string[SkinManager.Instance.SkinnedFonts.Count];
            foreach (KeyValuePair<string, Font> pair in SkinManager.Instance.SkinnedFonts)
            {
                _skinnedFontNames[j] = pair.Key;
                j++;
            }*/
        }

        public override void OnInspectorGUI()
        {
            _currentSkinnedColorIndex = DFComponent.CurrentColorIndex;
            //_currentSkinnedFontIndex = DFComponent.CurrentFontIndex;
            _isUpper = DFComponent.IsUpper;
            _isLower = DFComponent.IsLower;
            _textWithoutCase = DFComponent.TextWithoutCase;
            _useHyperlink = DFComponent.UseHyperlink;
            _doesColorChangeOnHover = DFComponent.DoesColorChangeOnHover;
            _hoverSkinnedColorIndex = DFComponent.HoverColorIndex;
            _hoverColor = DFComponent.HoverColor;

            EditorGUILayout.PropertyField(_useSkin);
            DFComponent.UseSkin = _useSkin.boolValue;
            if (_useSkin.boolValue)
            {
                _currentSkinnedColorIndex = EditorGUILayout.Popup("Text Skinned Color", _currentSkinnedColorIndex, _skinnedColorNames, EditorStyles.popup);
                if (_currentSkinnedColorIndex > _skinnedColorNames.Length)
                {
                    DFComponent.UseSkin = false;
                    DFComponent.CurrentColorIndex = 0;
                }
                else
                {
                    if (_currentSkinnedColorIndex > 0)
                        DFComponent.TextComponent.color = SkinManager.Instance.SkinnedColors[_skinnedColorNames[_currentSkinnedColorIndex]];
                    if (DFComponent.CurrentColorIndex != _currentSkinnedColorIndex)
                        EditorSceneManager.MarkSceneDirty(SceneManager.GetActiveScene());
                    DFComponent.CurrentColorIndex = _currentSkinnedColorIndex;

                    /*_currentSkinnedFontIndex = EditorGUILayout.Popup("Skinned Font", _currentSkinnedFontIndex, _skinnedFontNames, EditorStyles.popup);
                    DFComponent.font = SkinManager.Instance.SkinnedFonts[_skinnedFontNames[_currentSkinnedFontIndex]];

                    if (_currentSkinnedFontIndex == 0 && DFComponent.fontSize >= 60)
                    {
                        if (DFComponent.fontSize >= 120)
                        {
                            if (DFComponent.fontSize >= 180)
                            {
                                if (DFComponent.fontSize >= 240)
                                    DFComponent.font = SkinManager.Instance.GeneralFont_240;
                                else
                                    DFComponent.font = SkinManager.Instance.GeneralFont_180;
                            }
                            else
                                DFComponent.font = SkinManager.Instance.GeneralFont_120;
                        }
                        else
                            DFComponent.font = SkinManager.Instance.GeneralFont_60;
                    }
                    if (DFComponent.CurrentFontIndex != _currentSkinnedFontIndex)
                        EditorSceneManager.MarkSceneDirty(SceneManager.GetActiveScene());
                    DFComponent.CurrentFontIndex = _currentSkinnedFontIndex;*/
                }

                UnityEditor.EditorUtility.SetDirty(DFComponent);
            }

            EditorGUILayout.BeginHorizontal();
            _isUpper = EditorGUILayout.Toggle("Is Upper", _isUpper);
            _isLower = EditorGUILayout.Toggle("Is Lower", _isLower);
            EditorGUILayout.EndHorizontal();

            //Debug.Log(DFComponent.text + " -- " + _textWithoutCase);
            if ((string.IsNullOrEmpty(_textWithoutCase) || (_textWithoutCase.ToLowerInvariant() != DFComponent.TextComponent.text.ToLowerInvariant() /*&& !_useLocalization*/)) && _isUpper == false && _isLower == false)
            {
                DFComponent.TextWithoutCase = DFComponent.TextComponent.text;
                //Debug.Log(" ## " + ItecaComponent.text + " -- " + ItecaComponent.TextWithoutCase);
            }
            else
            {
                if (_isUpper == false && _isLower == false)
                {
                    DFComponent.TextComponent.text = DFComponent.TextWithoutCase;
                }
                else if (_isUpper)
                {
                    DFComponent.TextComponent.text = DFComponent.TextComponent.text.ToUpper();
                }
                else if (_isLower)
                {
                    DFComponent.TextComponent.text = DFComponent.TextComponent.text.ToLower();
                }
                UnityEditor.EditorUtility.SetDirty(DFComponent);
            }
            DFComponent.IsUpper = _isUpper;
            DFComponent.IsLower = _isLower;

            EditorGUILayout.Space(10);

            EditorGUILayout.LabelField("Hyperlink", EditorStyles.boldLabel);
            DFComponent.UseHyperlink = EditorGUILayout.Toggle("Use Hyperlink", _useHyperlink);
            if (_useHyperlink)
            {
                DFComponent.DoesColorChangeOnHover = EditorGUILayout.Toggle("Does Color Change On Hover", _doesColorChangeOnHover);

                if (_doesColorChangeOnHover)
                {
                    if (_useSkin.boolValue)
                    {
                        _hoverSkinnedColorIndex = EditorGUILayout.Popup("Hover Skinned Color", _hoverSkinnedColorIndex, _skinnedColorNames, EditorStyles.popup);
                        if (_hoverSkinnedColorIndex > _skinnedColorNames.Length)
                        {
                            DFComponent.UseSkin = false;
                            DFComponent.HoverColorIndex = 0;
                        }
                        else
                        {
                            if (_hoverSkinnedColorIndex > 0)
                            {
                                DFComponent.HoverColor = SkinManager.Instance.SkinnedColors[_skinnedColorNames[_hoverSkinnedColorIndex]];
                                GUI.enabled = false;
                                GUIContent content = new GUIContent("Hover Color");
                                EditorGUILayout.ColorField(content, _hoverColor, false, false, false);
                                GUI.enabled = true;
                            }
                            else
                            {
                                DFComponent.HoverColor = EditorGUILayout.ColorField("Hover Color", _hoverColor);
                            }

                            if (DFComponent.HoverColorIndex != _hoverSkinnedColorIndex)
                                EditorSceneManager.MarkSceneDirty(SceneManager.GetActiveScene());
                            DFComponent.HoverColorIndex = _hoverSkinnedColorIndex;
                        }

                    } else
                    {
                        DFComponent.HoverColor = EditorGUILayout.ColorField("Hover Color", _hoverColor);
                    }
                }
                
            }

            EditorGUILayout.PropertyField(_useLocalization);
            DFComponent.UseLocalization = _useLocalization.boolValue;
            /*if (_useLocalization.boolValue)
            {
                _localizationIndex = EditorGUILayout.Popup("Localization Phrase", _localizationIndex, _localizationKeys, EditorStyles.popup);
                if (_localizationIndex > _localizationKeys.Length)
                {
                    DFComponent.UseLocalization = false;
                    DFComponent.LocalizationIndex = 0;
                }
                else
                {
                    if (_localizationIndex > 0)
                        DFComponent.TextComponent.text = LocalizationManager.Instance.Phrases[_localizationIndex-1].localization_fr;
                    if (DFComponent.LocalizationIndex != _localizationIndex)
                        EditorSceneManager.MarkSceneDirty(SceneManager.GetActiveScene());
                    DFComponent.LocalizationIndex = _localizationIndex;
                }
            }*/

            EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
            base.serializedObject.ApplyModifiedProperties();
        }
    }
}
#endif
