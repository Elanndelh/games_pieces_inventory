using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

namespace DepthFactory.Editor
{
    [ExecuteInEditMode]
    public class SkinManager : Singleton<SkinManager>
    {
        SkinData _skinData;
        public SkinData SkinData
        {
            get { return _skinData; }
        }
        Dictionary<string, Color> _skinnedColors;
        public Dictionary<string, Color> SkinnedColors
        {
            get
            {
                if (_skinnedColors == null)
                    _skinnedColors = new Dictionary<string, Color>();

                _skinnedColors.Clear();
                _skinnedColors.Add("main", _skinData.mainColor);
                _skinnedColors.Add("complementary", _skinData.complementaryColor);
                _skinnedColors.Add("secondary", _skinData.secondaryColor);
                _skinnedColors.Add("error", _skinData.errorColor);

                return _skinnedColors;
            }
        }

        Dictionary<string, Font> _skinnedFonts;
        public Dictionary<string, Font> SkinnedFonts
        {
            get
            {
                if (_skinnedFonts == null)
                    _skinnedFonts = new Dictionary<string, Font>();

                _skinnedFonts.Clear();
                _skinnedFonts.Add("general", _skinData.generalFont);

                return _skinnedFonts;
            }
        }

        public Action<string> OnActiveSkinChanged;
        string _activeSkinName;
        public string ActiveSkinName
        {
            get { return _activeSkinName; }
            set
            {
                _activeSkinName = value;
                OnActiveSkinChanged?.Invoke(_activeSkinName);
            }
        }


    ///////////////////////////////////////////////////////////////
    /// GENERAL FUNCTIONS /////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        void Start()
        {
#if UNITY_EDITOR
            if (Application.isEditor)
            {
                Constants.Init();
                LoadSkin();
                OnActiveSkinChanged += (s) => LoadSkin();
            }
#endif
        }
        /*********************************************************/

#if UNITY_EDITOR
    ///////////////////////////////////////////////////////////////
    /// PRIVATE FUNCTIONS /////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        void LoadSkin()
        {
            if (File.Exists(Constants.SKIN_PATH) == false)
                File.Create(Constants.SKIN_PATH);

            string tmpStr = File.ReadAllText(Constants.SKIN_PATH);
            if (string.IsNullOrEmpty(tmpStr) == false)
            {
                _skinData = JsonUtility.FromJson<SkinData>(tmpStr);
            }
            else
                _skinData = new SkinData();
        }
        /*********************************************************/
#endif
    }
}