using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace DepthFactory.UI
{
    [RequireComponent(typeof(Image), typeof(Toggle))]
    public class DF_Tab : MonoBehaviour, IDFUIComponent
    {
        #region Skin
        public bool UseSkin { get { return _useSkin; } set { _useSkin = value; } }
        [HideInInspector] [SerializeField] bool _useSkin;
        public int StandardColorIndex { get { return _standardColorIndex; } set { _standardColorIndex = value; } }
        [HideInInspector] [SerializeField] int _standardColorIndex;
        public int SelectedColorIndex { get { return _selectedColorIndex; } set { _selectedColorIndex = value; } }
        [HideInInspector] [SerializeField] int _selectedColorIndex;

        public bool ChangeSubColor { get { return _changeSubColor; } set { _changeSubColor = value; } }
        [HideInInspector] [SerializeField] bool _changeSubColor;
        public int StandardSubColorIndex { get { return _standardSubColorIndex; } set { _standardSubColorIndex = value; } }
        [HideInInspector] [SerializeField] int _standardSubColorIndex;
        public int SelectedSubColorIndex { get { return _selectedSubColorIndex; } set { _selectedSubColorIndex = value; } }
        [HideInInspector] [SerializeField] int _selectedSubColorIndex;
        #endregion

        public Graphic SubGraphic { get { return _subGraphic; } set { _subGraphic = value; } }
        [SerializeField] Graphic _subGraphic;

        //public bool IsOn { get { return _isOn; } set { _isOn = value; OnSkinUpdate(); } }
        //[SerializeField] bool _isOn;

        public Image ImageComponent
        {
            get
            {
                if (_imageComponent == null)
                    _imageComponent = GetComponent<Image>();
                return _imageComponent;
            }
            set { _imageComponent = value; }
        }
        Image _imageComponent;

        public Toggle ToggleComponent
        {
            get
            {
                if (_toggleComponent == null)
                    _toggleComponent = GetComponent<Toggle>();
                return _toggleComponent;
            }
            set { _toggleComponent = value; }
        }
        Toggle _toggleComponent;


        void Awake()
		{
            _imageComponent = GetComponent<Image>();
        }
        /*********************************************************/

        void Start()
        {
            OnSkinUpdate();
            SkinHelper.OnSkinUpdate += OnSkinUpdate;
        }
        /*********************************************************/

        void OnDestroy()
        {
            SkinHelper.OnSkinUpdate -= OnSkinUpdate;
        }
        /*********************************************************/

        public void OnSkinUpdate()
        {
            if (UseSkin && Application.isPlaying)
            {
                if (ToggleComponent.isOn)
                {
                    if (_selectedColorIndex > 0)
                        _imageComponent.color = SkinHelper.GetColor(_selectedColorIndex - 1);
                    if (_changeSubColor && _selectedSubColorIndex > 0)
                        _subGraphic.color = SkinHelper.GetColor(_selectedSubColorIndex - 1);
                } else
                {
                    if (_standardColorIndex > 0)
                        _imageComponent.color = SkinHelper.GetColor(_standardColorIndex - 1);
                    if (_changeSubColor && _standardSubColorIndex > 0)
                        _subGraphic.color = SkinHelper.GetColor(_standardSubColorIndex - 1);
                }
            }
        }
        /*********************************************************/

        /*public void UI_Press()
		{
            if (ToggleComponent.isOn)
                return;

            IsOn = true;
        }
        /*********************************************************/
    }
}
