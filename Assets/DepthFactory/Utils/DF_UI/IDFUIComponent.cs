namespace DepthFactory.UI
{
    public interface IDFUIComponent
    {
        bool UseSkin { get; set; }

        void OnSkinUpdate();
    }
}
