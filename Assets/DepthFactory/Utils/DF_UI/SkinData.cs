using UnityEngine;

namespace DepthFactory
{
    [System.Serializable]
    public class SkinData
    {
        public Color mainColor;
        public Color complementaryColor;
        public Color secondaryColor;
        public Color errorColor;

        public Font generalFont;


    ///////////////////////////////////////////////////////////////
    /// CONSTRUCTORS //////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        public SkinData()
        {
            mainColor = Color.black;
            complementaryColor = Color.black;
            secondaryColor = Color.black;
            errorColor = Color.black;

            generalFont = null;
        }
        /*********************************************************/

        public SkinData(SkinData a_skinData)
        {
            mainColor = a_skinData.mainColor;
            complementaryColor = a_skinData.complementaryColor;
            secondaryColor = a_skinData.secondaryColor;
            errorColor = a_skinData.errorColor;

            generalFont = a_skinData.generalFont;
        }
        /*********************************************************/

    ///////////////////////////////////////////////////////////////
    /// PUBLIC FUNCTIONS //////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        public string GetColorName(int a_index)
        {
            return a_index switch
            {
                0 => "couleur principale",
                1 => "couleur complémentaire",
                2 => "couleur secondaire",
                _ => "couleur inconnue",
            };
        }
        /*********************************************************/
    }
}
