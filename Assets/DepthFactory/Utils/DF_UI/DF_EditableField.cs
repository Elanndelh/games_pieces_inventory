using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace DepthFactory.UI
{
    public class DF_EditableField : MonoBehaviour
    {
        [SerializeField] Toggle _toggle;
        [SerializeField] TMP_Text _text;
        [SerializeField] TMP_InputField _inputfield;
		[SerializeField] bool _initWithEdit;

		
    ///////////////////////////////////////////////////////////////
    /// GENERAL FUNCTIONS /////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
		void Awake()
		{
			_toggle.onValueChanged.SetPersistentListenerState(0, UnityEngine.Events.UnityEventCallState.Off);
			_toggle.isOn = _initWithEdit;
			_toggle.onValueChanged.SetPersistentListenerState(0, UnityEngine.Events.UnityEventCallState.RuntimeOnly);
			UI_DisplayEdition(_initWithEdit);
		}
        /*********************************************************/
		
    ///////////////////////////////////////////////////////////////
    /// PUBLIC FUNCTIONS //////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
		public void UI_DisplayEdition(bool a_showEdition)
		{
			_text.enabled = !a_showEdition;
			_inputfield.gameObject.SetActive(a_showEdition);
		}
		/*********************************************************/

		public void SetText(string a_value)
		{
			_text.text = a_value;
			_inputfield.SetTextWithoutNotify(a_value);
		}
		/*********************************************************/
	}
}
