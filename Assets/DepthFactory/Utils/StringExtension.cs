// Created by Tristan Gassies, 2018/02/19
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;

namespace DepthFactory.Utils
{
    public static class StringExtension
    {
        #region Formalization
        public static string AddLine(this string a_string, string a_newLine, bool a_lastLine = false)
        {
            a_string += a_newLine + (a_lastLine ? string.Empty : "\n");
            return a_string;
        }
        /*********************************************************/

        public static string Tabs(uint a_number)
        {
            string tabs = string.Empty;

            for (int i = 0; i < a_number; i++)
                tabs += "\t";
            return tabs;
        }
        /*********************************************************/

        public static string FormalizeSQLName(this string a_string)
        {
            if (string.IsNullOrEmpty(a_string))
                return string.Empty;

            char[] fieldCharName = a_string.ToCharArray();
            List<char> sqlChars = new List<char>();
            bool previousCharWasUpper = false;
            for (int i = 0; i < fieldCharName.Length; i++)
            {
                char c = fieldCharName[i];
                if (char.IsUpper(c))
                {
                    if (i > 0 && fieldCharName[i - 1] != '_' && previousCharWasUpper == false)
                        sqlChars.Add('_');
                    previousCharWasUpper = true;
                }
                else
                {
                    previousCharWasUpper = false;
                }
                sqlChars.Add(char.ToLower(c));
            }
            string sqlCharName = string.Empty;
            for (int i = 0; i < sqlChars.Count; i++)
                sqlCharName += sqlChars[i].ToString();

            return sqlCharName;
        }
        /*********************************************************/

        public static string FormalizeCSharpName(this string a_string)
        {
            if (string.IsNullOrEmpty(a_string))
                return string.Empty;

            char[] fieldCharName = a_string.ToCharArray();
            List<char> csharpChars = new List<char>();
            for (int i = 0; i < fieldCharName.Length; i++)
            {
                char c = fieldCharName[i];
                if (c == '_')
                {
                    csharpChars.Add(char.ToUpper(fieldCharName[i + 1]));
                    i++;
                }
                else
                    csharpChars.Add(c);
            }
            string csharpCharName = string.Empty;
            for (int i = 0; i < csharpChars.Count; i++)
                csharpCharName += csharpChars[i].ToString();

            return csharpCharName;
        }
        /*********************************************************/

        public static string FormalizeVariable(this string a_variableName)
        {
            if (a_variableName.Contains("."))
                a_variableName = a_variableName.Replace('.', '_');
            if (a_variableName.Contains(" "))
                a_variableName = a_variableName.Replace(' ', '_');

            if (a_variableName[0] == '_')
            {
                string removeChar = a_variableName[1].ToString();
                a_variableName = a_variableName.Remove(1, 1);
                a_variableName = a_variableName.Insert(1, removeChar.ToLower());
            }
            else if (char.IsNumber(a_variableName[0]))
            {
                a_variableName = "notANumberOnFirstChar_" + a_variableName;
            }
            else
            {
                string removeChar = a_variableName[0].ToString();
                a_variableName = a_variableName.Remove(0, 1);
                a_variableName = removeChar.ToLower() + a_variableName;
            }

            return a_variableName;
        }
        /*********************************************************/

        public static string FormalizeCSharpVariable(this string a_variableName)
        {
            return a_variableName.FormalizeVariable().FormalizeCSharpName();
        }
        /*********************************************************/

        public static string FormalizeMethod(this string a_name)
        {
            if (a_name.Contains("."))
                a_name = a_name.Replace('.', '_');
            if (a_name.Contains(" "))
                a_name = a_name.Replace(' ', '_');

            if (char.IsLower(a_name[0]))
            {
                string removeChar = a_name[0].ToString();
                a_name = a_name.Remove(0, 1);
                a_name = removeChar.ToUpper() + a_name;
            }
            else if (char.IsNumber(a_name[0]))
            {
                a_name = "NotANumberOnFirstChar_" + a_name;
            }

            return a_name;
        }
        /*********************************************************/

        public static string FormalizeFirstWord(this string a_word)
        {
            char firstLetter = a_word[0];
            firstLetter = char.ToUpper(firstLetter);

            a_word = a_word.ToLower();
            string newWord = firstLetter + a_word.Substring(1);
            return newWord;
        }
        /*********************************************************/

        /// <summary>
        /// Try to formalize each words that is a name
        /// </summary>
        /// <param name="a_word"></param>
        /// <returns></returns>
        public static string FormalizeNames(this string a_string)
        {
            if (string.IsNullOrEmpty(a_string))
                return string.Empty;

            return Regex.Replace(a_string.ToLower(), @"(?<=^|\b(?!(?:en|le|la|les|de|du|des|sur|sous)\b))([a-z])", m => m.Value.ToUpper());
        }
        /*********************************************************/

        public static string RemoveDiacritics(this string a_string)
        {
            string stFormD = a_string.Normalize(NormalizationForm.FormD);
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < stFormD.Length; i++)
            {
                UnicodeCategory uc = CharUnicodeInfo.GetUnicodeCategory(stFormD[i]);
                if (uc != UnicodeCategory.NonSpacingMark)
                {
                    sb.Append(stFormD[i]);
                }
            }

            return (sb.ToString().Normalize(NormalizationForm.FormC));
        }
        /*********************************************************/

        public static int CompareInvariant(this string a_string, string a_other)
        {
            return string.Compare(a_string, a_other, CultureInfo.CurrentCulture, CompareOptions.IgnoreNonSpace | CompareOptions.IgnoreCase);
        }
        /*********************************************************/

        public static string NoNullValue(this string a_string)
        {
            if (a_string == null)
                return string.Empty;
            else return a_string;
        }
        /*********************************************************/
        #endregion

        #region Paths
        public static string GetRelativePath(this string a_path, string a_urlPath)
        {
            if (a_path.Contains(a_urlPath))
                a_path = "/" + a_path.TrimStart(a_urlPath.ToCharArray());
            return a_path;
        }
        /*********************************************************/

        public static string GetAbsolutePath(this string a_path, string a_urlPath)
        {
            if (string.IsNullOrEmpty(a_path))
                return a_urlPath;
            if (a_path.Contains(a_urlPath) == false)
                a_path = a_urlPath + a_path;
            return a_path;
        }
        /*********************************************************/

        public static string GetThumbPath(this string a_path)
        {
            string[] array = a_path.Split('.');
            string thumbPath = string.Empty;
            for (int i = 0; i < array.Length; i++)
            {
                if (i == 0)
                    thumbPath += array[0];
                else if (i != array.Length - 1)
                    thumbPath += "." + array[i];
                else
                    thumbPath += "_thumb." + array[i];
            }
            return thumbPath;
        }
        /*********************************************************/

        public static string GetMediaName(this string a_path)
        {
            if (a_path.Contains("/") && a_path.Contains("."))
                return a_path.Split('/')[a_path.Split('/').Length - 1].Split('.')[0];
            else
                return a_path;
        }
        /*********************************************************/
        #endregion
    }
}