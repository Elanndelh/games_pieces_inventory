using BarcodeScanner;
using BarcodeScanner.Scanner;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace DepthFactory.Utils
{
    public class DFBarcodeScanner : Singleton<DFBarcodeScanner>
    {
		Scanner _barcodeScanner;
		[SerializeField] Text _textHeader;
		[SerializeField] RawImage _image;
		[SerializeField] AudioSource _audio;
		//[SerializeField] RectTransform _scanZoneRT;
		float _restartTime;
		bool _isActive;
		Canvas _canvas;

		Action<string> OnScan;


		protected override void Awake()
		{
			base.Awake();
			// Disable Screen Rotation on that screen
			Screen.autorotateToPortrait = false;
			Screen.autorotateToPortraitUpsideDown = false;
			_canvas = GetComponent<Canvas>();
		}
		/*********************************************************/

		public void Init(Action<string> a_onScan)
		{
			// Create a basic scanner
			_barcodeScanner = new Scanner();
			/*if (_scanZoneRT != null)
				_barcodeScanner.ScanZone = _scanZoneRT.rect;*/
			_barcodeScanner.Camera.Play();

			// Display the camera texture through a RawImage
			_barcodeScanner.OnReady += (sender, arg) => {
				// Set Orientation & Texture
				_image.transform.localEulerAngles = _barcodeScanner.Camera.GetEulerAngles();
				_image.transform.localScale = _barcodeScanner.Camera.GetScale();
				_image.texture = _barcodeScanner.Camera.Texture;

				// Keep Image Aspect Ratio
				var rect = _image.GetComponent<RectTransform>();
				var newHeight = rect.sizeDelta.x * _barcodeScanner.Camera.Height / _barcodeScanner.Camera.Width;
				rect.sizeDelta = new Vector2(rect.sizeDelta.x, newHeight);

				_restartTime = Time.realtimeSinceStartup;
			};
			_isActive = true;
			_canvas.enabled = true;

			OnScan = a_onScan;
		}
		/*********************************************************/

		/// <summary>
		/// The Update method from unity need to be propagated
		/// </summary>
		void Update()
		{
			if (_isActive == false)
				return;

			if (_barcodeScanner != null)
			{
				_barcodeScanner.Update();
			}

			// Check if the Scanner need to be started or restarted
			if (_restartTime != 0 && _restartTime < Time.realtimeSinceStartup)
			{
				StartScanner();
				_restartTime = 0;
			}
		}
		/*********************************************************/

		/// <summary>
		/// Start a scan and wait for the callback (wait 1s after a scan success to avoid scanning multiple time the same element)
		/// </summary>
		void StartScanner()
		{
			_barcodeScanner.Scan((barCodeType, barCodeValue) => {
				_barcodeScanner.Stop();
				if (_textHeader)
				{
					if (_textHeader.text.Length > 250)
					{
						_textHeader.text = string.Empty;
					}
					_textHeader.text += "Found: " + barCodeType + " / " + barCodeValue + "\n";
				}
				_restartTime += Time.realtimeSinceStartup + 1f;
				OnScan?.Invoke(barCodeValue);

				// Feedback
				_audio.Play();

#if UNITY_ANDROID || UNITY_IOS
				Handheld.Vibrate();
#endif
			});
		}
		/*********************************************************/

		/// <summary>
		/// This coroutine is used because of a bug with unity (http://forum.unity3d.com/threads/closing-scene-with-active-webcamtexture-crashes-on-android-solved.363566/)
		/// Trying to stop the camera in OnDestroy provoke random crash on Android
		/// </summary>
		/// <param name="a_callback"></param>
		/// <returns></returns>
		IEnumerator StopCamera(Action a_callback)
		{
			// Stop Scanning
			if (_barcodeScanner != null)
				_barcodeScanner.Destroy();
			_barcodeScanner = null;
			_restartTime = 0;
			_isActive = false;
			_canvas.enabled = false;

			// Wait a bit
			yield return new WaitForSeconds(0.1f);

			a_callback?.Invoke();
		}
		/*********************************************************/

		public void Btn_Close()
		{
			// Try to stop the camera before loading another scene
			StartCoroutine(StopCamera(null));
		}
		/*********************************************************/
	}
}
