using System.Collections.Generic;
using UnityEngine;

namespace DepthFactory.Utils
{
    public class PHPForm
    {
        Dictionary<string, string> _parameters = new Dictionary<string, string>();

        string Token { get
            {
                string token = string.Empty;

                foreach (KeyValuePair<string, string> parameter in _parameters)
                    token += parameter.Value;

                token += Constants.PHP_TOKEN;
                token = Crypto.ComputeSha256Hash(token);
                return token;
            } 
        }

        
    ///////////////////////////////////////////////////////////////
    /// PUBLIC FUNCTIONS //////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        public void AddField(string a_name, string a_value)
        {
            if (_parameters.ContainsKey(a_name) == false)
                _parameters.Add(a_name, a_value);
        }
        /*********************************************************/

        public void AddField(string a_name, int a_value)
        {
            AddField(a_name, a_value.ToString());
        }
        /*********************************************************/

        public WWWForm CreateForm()
        {
            WWWForm form = new WWWForm();
            foreach (KeyValuePair<string, string> parameter in _parameters)
			{
                //Debug.Log(parameter.Key + " -- " + parameter.Value);
                form.AddField(parameter.Key, parameter.Value);
            }
            form.AddField("token", Token);
            return form;
        }
        /*********************************************************/
    }
}
