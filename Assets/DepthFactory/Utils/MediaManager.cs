﻿using SimpleJSON;
using System;
using System.Collections;
using System.IO;
using System.Net;
using UnityEngine;

namespace DepthFactory.Utils
{
	public class MediaType
	{
		public enum Type
		{
			Image,
			Video,
			Document
		}


		public static Type GetMediaType(string a_completePath)
		{
			MediaType.Type type = MediaType.Type.Image;

			string loweredPath = a_completePath.ToLower();
			// TODO : is image or video
			if (loweredPath.EndsWith(".png") || loweredPath.EndsWith(".bmp") || loweredPath.EndsWith(".jpg"))
				type = Type.Image;
			else if (loweredPath.EndsWith(".avi") || loweredPath.EndsWith(".mp4") || loweredPath.EndsWith(".mov") || loweredPath.EndsWith("mpg") || loweredPath.EndsWith("mkv"))
				type = Type.Video;
			else if (loweredPath.EndsWith("txt") || loweredPath.EndsWith("doc") || loweredPath.EndsWith("docx") || loweredPath.EndsWith("odt") || loweredPath.EndsWith("pdf") || loweredPath.EndsWith("zip") || loweredPath.EndsWith("rar"))
				type = Type.Document;
			return type;
		}
		/*********************************************************/
	}

	public class MediaManager : MonoBehaviour
	{
		#region Singleton
		static MediaManager _instance;
		public static MediaManager Instance
		{
			get
			{
				if (_instance == null)
				{
					var instances = FindObjectsOfType<MediaManager>();
					if (instances.Length == 0)
					{
						throw new NullReferenceException("There is no MediaManager but a script depends on it");
					}
					else if (instances.Length > 1)
					{
						UnityEngine.Debug.LogError("There is more than one MediaManager, this should not happen");
						_instance = instances[0];
					}
					else
					{
						_instance = instances[0];
					}
				}
				return _instance;
			}
		}
		/*********************************************************/
		#endregion

		public Action<string> OnUploadEvent = null;

		/*public bool GetFrameOfVideo(string a_localFileName)
		{
			bool success = false;

			float randFrameIndex = UnityEngine.Random.Range(0f, 1f);

			OpenCVForUnity.VideoCapture videoCapture = new OpenCVForUnity.VideoCapture();
			success = videoCapture.open(Path.GetFullPath(a_localFileName));
			if (success && videoCapture.isOpened())
			{
				OpenCVForUnity.Mat frameMat = new OpenCVForUnity.Mat();
				videoCapture.set(OpenCVForUnity.Videoio.CAP_PROP_POS_AVI_RATIO, randFrameIndex);
				success = videoCapture.read(frameMat);

				FileInfo fileInfo = new FileInfo(a_localFileName);
				string thumbPath = fileInfo.Directory.ToString() + fileInfo.Name + "_thumb.jpg";

				success = OpenCVForUnity.Imgcodecs.imwrite(thumbPath, frameMat);
				videoCapture.release();

			}

			return success;
		}*/

		/// <summary>
		/// Upload media from texture
		/// </summary>
		/// <param name="a_texture2D">Texture2d to be uploaded</param>
		/// <returns></returns>
		IEnumerator UploadFileCo(Texture2D a_texture2D, string a_url, string a_previousPath)
		{
			MediaType.Type type = MediaType.Type.Image; // from texture2D
			string typeName = string.Empty;
			string mime_type = string.Empty;

			string date = DateTime.Now.ToString("yyMMdd_hhmmss");
			string desiredFileName = type.ToString()[0] + "_";
			desiredFileName += date;
			desiredFileName += ".jpg";

			byte[] bytesArray = new byte[0];
			switch (type)
			{
				case MediaType.Type.Image:
					typeName = "Images";
					mime_type = "image/";

					if (a_texture2D.width > 1920 || a_texture2D.height > 1080)
					{
						float diff = (float)a_texture2D.width / (float)a_texture2D.height;
						//Debug.Log(a_texture2D.width + "/" + a_texture2D.height + " = " + diff);

						if (diff <= 1f) // Width <= Height
						{
							Texture2D tempTex = Instantiate(a_texture2D);
							TextureScale.Bilinear(tempTex, ((int)(1080 * diff)), 1080);
							bytesArray = tempTex.EncodeToJPG(100);
						}
						else // Width > Height
						{
							Texture2D tempTex = a_texture2D;
							TextureScale.Bilinear(tempTex, 1920, ((int)(1920 / diff)));
							bytesArray = tempTex.EncodeToJPG(100);
						}
					}
					else
						bytesArray = a_texture2D.EncodeToJPG(100);
					break;
			}

			string boundary = "----------------------------" + DateTime.Now.Ticks.ToString("x");
			string entete = "Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}\n--" + boundary + "\r\n";

			HttpWebRequest httpWebRequest2 = (HttpWebRequest)WebRequest.Create(a_url);
			httpWebRequest2.ContentType = "multipart/form-data; boundary=" + boundary;
			httpWebRequest2.Method = "POST";
			httpWebRequest2.KeepAlive = true;
			httpWebRequest2.Credentials = System.Net.CredentialCache.DefaultCredentials;

			Stream memStream = new System.IO.MemoryStream();

			byte[] boundarybytes = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "\r\n");
			memStream.Write(boundarybytes, 0, boundarybytes.Length);

			string fileTemplate = "Content-Disposition: attachment; name=\"medias\"; filename=\"{0}\";\r\n Content-Type: application/octet-stream\r\n\r\n";
			byte[] fileBytes;

			byte[] headerbytes = System.Text.Encoding.ASCII.GetBytes(string.Format(entete, "visual", "medias"));
			memStream.Write(headerbytes, 0, headerbytes.Length);
			headerbytes = System.Text.Encoding.ASCII.GetBytes(string.Format(entete, "up", string.Empty));
			memStream.Write(headerbytes, 0, headerbytes.Length);
			headerbytes = System.Text.Encoding.ASCII.GetBytes(string.Format(entete, "type", typeName));
			memStream.Write(headerbytes, 0, headerbytes.Length);
			headerbytes = System.Text.Encoding.ASCII.GetBytes(string.Format(entete, "path", desiredFileName));
			memStream.Write(headerbytes, 0, headerbytes.Length);
			headerbytes = System.Text.Encoding.ASCII.GetBytes(string.Format(entete, "media", string.Empty));
			memStream.Write(headerbytes, 0, headerbytes.Length);
			headerbytes = System.Text.Encoding.ASCII.GetBytes(string.Format(entete, "previousPath", a_previousPath));
			memStream.Write(headerbytes, 0, headerbytes.Length);

			fileBytes = System.Text.Encoding.ASCII.GetBytes(string.Format(fileTemplate, desiredFileName));
			memStream.Write(fileBytes, 0, fileBytes.Length);
			memStream.Write(bytesArray, 0, bytesArray.Length);
			memStream.Write(boundarybytes, 0, boundarybytes.Length);

			httpWebRequest2.ContentLength = memStream.Length;
			Stream requestStream = httpWebRequest2.GetRequestStream();
			memStream.Position = 0;
			byte[] tempBuffer = new byte[memStream.Length];
			memStream.Read(tempBuffer, 0, tempBuffer.Length);
			memStream.Close();
			requestStream.Write(tempBuffer, 0, tempBuffer.Length);
			requestStream.Close();

			HttpWebResponse webResponse2 = (HttpWebResponse)httpWebRequest2.GetResponse();
			Stream stream2 = webResponse2.GetResponseStream();
			StreamReader reader2 = new StreamReader(stream2);
			var result = reader2.ReadToEnd();
			JSONNode json = JSON.Parse(result);
			if (webResponse2.StatusCode == HttpStatusCode.OK)
			{
				if (OnUploadEvent != null)
				{
					//Debug.Log(json["path"]);
					OnUploadEvent(json["path"]);
				}
				else
					DisplayError(json["error"]);
			}
			else
				DisplayError(webResponse2.StatusDescription + " " + json["error"]);
			webResponse2.Close();
			OnUploadEvent = null;

			yield return null;
		}
		/*********************************************************/


		/// <summary>
		/// Coroutine for uploading media from file
		/// </summary>
		/// <param name="a_localFileName">file path of media</param>
		/// <returns></returns>
		IEnumerator UploadFileCo(string a_localFileName, string a_url, string a_previousPath)
		{
			//Debug.Log("loading file :" + a_localFileName);
			WWW localFile = new WWW("file:///" + a_localFileName);

			yield return localFile;

			if (localFile.error == null)
				Debug.Log("Loaded file successfully : " + localFile.progress);
			else
			{
				//Debug.Log("Open file error: " + localFile.error);
				yield break; // stop the coroutine here
			}

			while (!localFile.isDone)
			{
				//Debug.Log("loading file :" + localFile.progress);
				yield return null;
			}

			MediaType.Type type = MediaType.GetMediaType(a_localFileName);
			string typeName = string.Empty;
			string mime_type = string.Empty;
			string extension = a_localFileName.Substring(a_localFileName.LastIndexOf(".") + 1).ToLower();

			string date = DateTime.Now.ToString("yyMMdd_hhmmss");
			string desiredFileName = type.ToString()[0] + "_";
			desiredFileName += date;
			desiredFileName += "_" + a_localFileName.Substring(a_localFileName.LastIndexOf("/") + 1);
			//Debug.Log("desiredFileName: " + desiredFileName);
			//desiredFileName += ".jpg";

			byte[] bytesArray = new byte[0];
			switch (type)
			{
				case MediaType.Type.Image:
					typeName = "Images";
					mime_type = "image/";
					switch (extension)
					{
						case "jpg":
						case "jpeg":
							mime_type += "jpeg";
							break;
						case "png":
							mime_type += "png";
							break;
					}

					float diff = (float)localFile.texture.width / (float)localFile.texture.height;

					if (diff <= 1f) // Width <= Height
					{
						Texture2D tempTex = Instantiate(localFile.texture);//Scaled(localFile.texture, ((int)(1080 * diff)), 1080);
																		   //bool isResize = tempTex.Resize(((int)(1080 * diff)), 1080);
						if (localFile.texture.height > 1080)
							TextureScale.Bilinear(tempTex, ((int)(1080 * diff)), 1080);

						switch (extension.ToLower())
						{
							case "jpg":
							case "jpeg":
								bytesArray = tempTex.EncodeToJPG(100);
								break;
							case "png":
								bytesArray = tempTex.EncodeToPNG();
								break;
							default:
								bytesArray = tempTex.EncodeToJPG(100);
								break;
						}
					}
					else // Width > Height
					{
						Texture2D tempTex = localFile.texture;//Scaled(localFile.texture, 1920, ((int)(1920 / diff)));
															  //bool isResize = tempTex.Resize(1920, ((int)(1920 / diff)));
						if (localFile.texture.width > 1920)
							TextureScale.Bilinear(tempTex, 1920, ((int)(1920 / diff)));

						switch (extension.ToLower())
						{
							case "jpg":
							case "jpeg":
								bytesArray = tempTex.EncodeToJPG(100);
								break;
							case "png":
								bytesArray = tempTex.EncodeToPNG();
								break;
							default:
								bytesArray = tempTex.EncodeToJPG(100);
								break;
						}
					}
					break;
			}
			string boundary = "----------------------------" + DateTime.Now.Ticks.ToString("x");
			string entete = "Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}\n--" + boundary + "\r\n";

			HttpWebRequest httpWebRequest2 = (HttpWebRequest)WebRequest.Create(a_url);
			httpWebRequest2.ContentType = "multipart/form-data; boundary=" + boundary;
			httpWebRequest2.Method = "POST";
			httpWebRequest2.KeepAlive = true;
			httpWebRequest2.Credentials = System.Net.CredentialCache.DefaultCredentials;

			Stream memStream = new System.IO.MemoryStream();

			byte[] boundarybytes = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "\r\n");
			memStream.Write(boundarybytes, 0, boundarybytes.Length);

			string fileTemplate = "Content-Disposition: attachment; name=\"medias\"; filename=\"{0}\";\r\n Content-Type: application/octet-stream\r\n\r\n";
			byte[] fileBytes;

			byte[] headerbytes = System.Text.Encoding.ASCII.GetBytes(string.Format(entete, "visual", "medias"));
			memStream.Write(headerbytes, 0, headerbytes.Length);
			headerbytes = System.Text.Encoding.ASCII.GetBytes(string.Format(entete, "up", string.Empty));
			memStream.Write(headerbytes, 0, headerbytes.Length);
			headerbytes = System.Text.Encoding.ASCII.GetBytes(string.Format(entete, "type", typeName));
			memStream.Write(headerbytes, 0, headerbytes.Length);
			headerbytes = System.Text.Encoding.ASCII.GetBytes(string.Format(entete, "path", desiredFileName));
			memStream.Write(headerbytes, 0, headerbytes.Length);
			headerbytes = System.Text.Encoding.ASCII.GetBytes(string.Format(entete, "media", string.Empty));
			memStream.Write(headerbytes, 0, headerbytes.Length);
			headerbytes = System.Text.Encoding.ASCII.GetBytes(string.Format(entete, "previousPath", a_previousPath));
			memStream.Write(headerbytes, 0, headerbytes.Length);

			fileBytes = System.Text.Encoding.ASCII.GetBytes(string.Format(fileTemplate, desiredFileName));
			memStream.Write(fileBytes, 0, fileBytes.Length);
			memStream.Write(bytesArray, 0, bytesArray.Length);
			memStream.Write(boundarybytes, 0, boundarybytes.Length);

			httpWebRequest2.ContentLength = memStream.Length;
			Stream requestStream = httpWebRequest2.GetRequestStream();
			memStream.Position = 0;
			byte[] tempBuffer = new byte[memStream.Length];
			memStream.Read(tempBuffer, 0, tempBuffer.Length);
			memStream.Close();
			requestStream.Write(tempBuffer, 0, tempBuffer.Length);
			requestStream.Close();

			HttpWebResponse webResponse2 = (HttpWebResponse)httpWebRequest2.GetResponse();
			Stream stream2 = webResponse2.GetResponseStream();
			StreamReader reader2 = new StreamReader(stream2);
			var result = reader2.ReadToEnd();
			JSONNode json = JSON.Parse(result);
			if (webResponse2.StatusCode == HttpStatusCode.OK)
			{
				if (OnUploadEvent != null)
				{
					//Debug.Log(json["path"]);
					OnUploadEvent(json["path"]);
				}
				else
					DisplayError(json["error"]);
			}
			else
				DisplayError(webResponse2.StatusDescription + " " + json["error"]);
			webResponse2.Close();
			OnUploadEvent = null;
		}
		/*********************************************************/

		IEnumerator DeleteFileCo(string a_path, string a_url, Action a_callback)
		{
			WWWForm postForm = new WWWForm();
			postForm.AddField("visual", "medias");
			postForm.AddField("media", string.Empty);
			postForm.AddField("delete", string.Empty);
			postForm.AddField("path", a_path);

			WWW request = new WWW(a_url, postForm);
			yield return request;

			if (string.IsNullOrEmpty(request.error) == false)
				Debug.LogError(request.error);

			if (a_callback != null)
				a_callback();
		}
		/*********************************************************/

		public void DisplayError(string error)
		{
			Debug.LogError(error);
			// TODO : display message over media
			OnUploadEvent("Erreur: " + error);
		}
		/*********************************************************/

		public void UploadFile(string a_localFileName, string a_url, string a_previousPath, Action<string> a_callback)
		{
			OnUploadEvent += a_callback;
			StartCoroutine(UploadFileCo(a_localFileName, a_url, a_previousPath));
		}
		/*********************************************************/

		public void UploadFile(Texture2D texture2D, string a_url, string a_previousPath, Action<string> a_callback)
		{
			OnUploadEvent += a_callback;
			StartCoroutine(UploadFileCo(texture2D, a_url, a_previousPath));
		}
		/*********************************************************/

		public void DeleteFile(string a_path, string a_url, Action a_callback)
        {
			StartCoroutine(DeleteFileCo(a_path, a_url, a_callback));
		}
		/*********************************************************/
	}

	/*public class RequestAddMedia : Requests
	{
		public string path;
		public string error;
		public int teamId;

		public RequestAddMedia(string _url, Action<Requests> callBack) : base(_url, callBack)
		{
			path = "";
		}

		public RequestAddMedia(string _url, Action<Requests> callBack, WWWForm form) : base(_url, callBack, form)
		{
			path = "";
		}

		public override void SetData(JSONNode jsonData)
		{
			if (jsonData != null)
			{
				path = jsonData["path"];
			}
			else
			{
				error = jsonData["error"];
				Debug.Log(error);
			}
		}

		public override void ResetData()
		{
		}
	}*/
}
