﻿using UnityEditor;
using UnityEngine;
using UnityEditor.Animations;

namespace DepthFactory.Utils.Editor
{
    public class MakeSimpleAnimator : EditorWindow
    {
        string _folderPath = "DepthFactory/DepthFactoryUtils/4. Anims";
        string _animatorName = "animatorName";
        bool _initialBool;


    ///////////////////////////////////////////////////////////////
    /// STATIC FUNCTIONS //////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        [MenuItem("DepthFactory/Animator/Make Simple Animator")]
        static void MenuMakeSimpleAnimator()
        {
            MakeSimpleAnimator window = (MakeSimpleAnimator)GetWindow(typeof(MakeSimpleAnimator));
            window.Show();
        }
        /*********************************************************/

    ///////////////////////////////////////////////////////////////
    /// GENERAL FUNCTIONS /////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        void OnGUI()
        {
            _folderPath = EditorGUILayout.TextField("Folder path : Assets/", _folderPath);
            _animatorName = EditorGUILayout.TextField("Animator's name", _animatorName);
            _initialBool = EditorGUILayout.Toggle("Initial object's visibility", _initialBool);

            if (GUILayout.Button(new GUIContent("Create simple animator")) && string.IsNullOrEmpty(_folderPath) == false && string.IsNullOrEmpty(_animatorName) == false)
            {
                CreateSimpleAnimator("Assets/" + _folderPath, _animatorName, _initialBool);
            }
        }
        /*********************************************************/
        
    ///////////////////////////////////////////////////////////////
    /// PRIVATE FUNCTIONS /////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        void CreateSimpleAnimator(string a_folderPath, string a_animatorName, bool a_initialBool)
        {
            System.IO.Directory.CreateDirectory(a_folderPath + "/" + a_animatorName);

            // Create Animations
            AnimationClip initClip = new AnimationClip();
            initClip.name = "init_" + a_animatorName;
            AssetDatabase.CreateAsset(initClip, a_folderPath + "/" + a_animatorName + "/" + initClip.name + ".anim");
            AnimationClip showClip = new AnimationClip();
            showClip.name = "show_" + a_animatorName;
            AssetDatabase.CreateAsset(showClip, a_folderPath + "/" + a_animatorName + "/" + showClip.name + ".anim");
            AnimationClip hideClip = new AnimationClip();
            hideClip.name = "hide_" + a_animatorName;
            AssetDatabase.CreateAsset(hideClip, a_folderPath + "/" + a_animatorName + "/" + hideClip.name + ".anim");

            // Create controller
            var controller = AnimatorController.CreateAnimatorControllerAtPath(a_folderPath + "/" + a_animatorName + "/" + a_animatorName + ".controller");

            // Add parameters
            AnimatorControllerParameter showParam = new AnimatorControllerParameter();
            showParam.name = "Show";
            showParam.type = AnimatorControllerParameterType.Bool;
            showParam.defaultBool = a_initialBool;
            controller.AddParameter(showParam);

            // Select base layer
            var rootStateMachine = controller.layers[0].stateMachine;
            
            // Add states
            var initState = rootStateMachine.AddState("init_" + a_animatorName);
            initState.motion = initClip;
            var showState = rootStateMachine.AddState("show_" + a_animatorName);
            showState.motion = showClip;
            var hideState = rootStateMachine.AddState("hide_" + a_animatorName);
            hideState.motion = hideClip;
            
            // Add transitions
            if (a_initialBool == false)
            {
                var firstShowTransition = initState.AddTransition(showState);
                firstShowTransition.AddCondition(AnimatorConditionMode.If, 0, "Show");
                CleanTransition(ref firstShowTransition);
            } else
            {
                var firstShowTransition = initState.AddTransition(hideState);
                firstShowTransition.AddCondition(AnimatorConditionMode.IfNot, 0, "Show");
                CleanTransition(ref firstShowTransition);
            }

            var showTransition = hideState.AddTransition(showState);
            showTransition.AddCondition(AnimatorConditionMode.If, 0, "Show");
            CleanTransition(ref showTransition);

            var hideTransition = showState.AddTransition(hideState);
            hideTransition.AddCondition(AnimatorConditionMode.IfNot, 0, "Show");
            CleanTransition(ref hideTransition);
        }
        /*********************************************************/

        void CleanTransition(ref AnimatorStateTransition a_transition)
        {
            a_transition.exitTime = 1f;
            a_transition.duration = 0f;
        }
        /*********************************************************/
    } // Class
} // Namespace Iteca.Utils.Editor
