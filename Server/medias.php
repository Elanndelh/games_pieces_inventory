<?php

// get path to thumbnail
function getThumbPath($src)
{
	$path_parts = pathinfo ( $src );
	
	return $path_parts['dirname'] . "/" . $path_parts['filename'] . "_thumb." . $path_parts['extension'];
}
/*********************************************************/

// get path to origin
function getOrigPath($src)
{
	$path_parts = pathinfo ( $src );
	
	return $path_parts['dirname'] . "/" . $path_parts['filename'] . "_orig." . $path_parts['extension'];
}
/*********************************************************/


// create thumbnail with max width or height
function make_thumb($src, $dest, $max_size) {

	/* read the source image */
	$type = strtolower(substr(strrchr($src,"."),1));
	if($type == 'jpeg') $type = 'jpg';
	switch($type){
		case 'bmp': $img = imagecreatefromwbmp($src); break;
		case 'gif': $img = imagecreatefromgif($src); break;
		case 'jpg': $img = imagecreatefromjpeg($src); break;
		case 'png': $img = imagecreatefrompng($src); break;
		default : return "Unsupported picture type!";
	}
	
	$width = imagesx($img);
	$height = imagesy($img);
	
	/* find the other "desired size" of this thumbnail, relative to the desired max_size and respect ratio */
	if ($width > $height)
	{
		$desired_width = $max_size;
		$desired_height = floor($height * ($max_size / $width));
	}
	else
	{
		$desired_height = $max_size;
		$desired_width = floor($width * ($max_size / $height));
	}
	
	/* create a new, "virtual" image */
	$virtual_image = imagecreatetruecolor($desired_width, $desired_height);
	if ($virtual_image !== FALSE) 
	{
		// preserve transparency
		if($type == "gif" or $type == "png"){
			imagecolortransparent($virtual_image, imagecolorallocatealpha($virtual_image, 0, 0, 0, 127));
			imagealphablending($virtual_image, false);
			imagesavealpha($virtual_image, true);
		}
		
		/* copy source image at a resized size */
		$error = (imagecopyresampled($virtual_image, $img, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height) === FALSE) ? "cannot resize" : "";
		
		/* create the physical thumbnail image to its destination */
		switch($type){
			case 'bmp': imagewbmp($virtual_image, $dest); break;
			case 'gif': imagegif($virtual_image, $dest); break;
			case 'jpg': imagejpeg($virtual_image, $dest); break;
			case 'png': imagepng($virtual_image, $dest); break;
		}
		imagedestroy($virtual_image);
	}
	else
		$error = "cannot create true color image";
	
	return $error;
}
/*********************************************************/


if (!isset($_POST['media']))
{
	if (isset($_GET['action'])) 
	{
		switch($_GET['action'])
		{
			case 'thumbs':
				echo CreateThumbToExistingMedia();
				break;
		}
	} else 
	{
		echo "No medias parameters !";
	}
}
else
{
	//clean special chars from $string
	function clean($string) {
	   $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
	   $string = preg_replace('/[^A-Za-z0-9\-\_\.]/', '', $string); // Removes special chars.

	   return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
	}


	$target_dir = "/img";//"/Medias/";
	
	// create main dir if not already done
	if (!file_exists(getcwd().$target_dir))
	{
		mkdir (getcwd().$target_dir, 0777, true);
	}
	if (isset($_POST['rotateImage']))
	{
		$path = $_POST['path'];
		$id = $_POST['id'];
		$angle = $_POST['clockwise'] == 0 ? 90 : 270;
		$mediaFileInfo = pathinfo($path);
		$original_extension = strtolower($mediaFileInfo['extension']);
		
		print_r($_POST);
		
		$original_file = getcwd()."/". $path;
		
		// load the image
		if($original_extension == "jpg" or $original_extension == "jpeg"){
			$original_image = imagecreatefromjpeg($original_file);
		}
		if($original_extension == "gif"){
			$original_image = imagecreatefromgif($original_file);
		}
		if($original_extension == "png"){
			$original_image = imagecreatefrompng($original_file);
		}
		$rotation = imagerotate($original_image, $angle, imagecolorallocatealpha( $original_image,0,0,0,127 ));
		if ($original_extension == "png")
		{
			imagealphablending($rotation, false);
			imagesavealpha($rotation, true);
		}
		
		if($original_extension == "jpg" or $original_extension == "jpeg"){
			imagejpeg($rotation, $original_file, 100);
		}
		if($original_extension == "gif"){
			imagegif($rotation, $original_file);
		}
		if($original_extension == "png"){
			imagepng($rotation, $original_file);
		}
		
		// clean up the garbage
		imagedestroy( $original_image );
		imagedestroy( $rotation );
		
		$array = array('id'=> $id, 'name' => $path );
		echo json_encode($array); 
	}
	else if (isset($_POST['get']))
	{
		$path = $_POST['path'];
		$mediaFileType = pathinfo($path);
		$ext = strtolower($mediaFileType['extension']);
		if($ext == "jpg" || $ext == "png" || $ext == "jpeg"
			|| $ext == "gif" ) 
		{
			$fp = fopen($path, 'rb');

			header("Content-Type: image/png");
			header("Content-Length: " . filesize($path));

			fpassthru($fp);
		}
		else if ($ext == "mov" || $ext == "mpg" || $ext == "mp4" || $ext == "avi")	
		{
			$thumbPath = $mediaFileType[PATHINFO_DIRNAME] . $mediaFileType[PATHINFO_FILENAME] . "_thumb.png";
			
			$fp = fopen($path, 'rb');

			header("Content-Type: image/png");
			header("Content-Length: " . filesize($path));

			fpassthru($fp);
		}
	}
	else if (isset($_POST['up']))
	{
		$path = $_POST['path'];
		$media = $_FILES["medias"];
		$type = $_POST['type'];
		$target_file = $target_dir . "/" . clean($media["name"]);
		$thumb_path = getcwd().$target_file;
		$target_path = getOrigPath($thumb_path);
		//$path_parts = pathinfo($target_path);
		$previous_path = getcwd().$_POST["previousPath"];
		if (!file_exists(getcwd().$target_dir))
		{
			mkdir(getcwd().$target_dir, 0777, true);
		}
		print_r($media);
		
		$error = $target_path . " -- " . $thumb_path;
		$error .= $_POST["previousPath"] . " -- " . $media["previousPath"];
		// Check if image file is a actual image or fake image
		echo $type;
		if(isset($_POST["up"]) && strcmp($type, "Images") == 0) {
			$check = exif_imagetype($media["tmp_name"]);
			if($check !== FALSE && $check < 18) {
				$error .= "File is an image - " . $check["mime"] . ".";
				$uploadOk = 1;
			}
			else {
				error_log ( "File". $media["tmp_name"] ." is not an image : ". $check);
				$decoded = base64_decode($media["tmp_name"], true);
				if ($decoded !== false)
				{
					$media["tmp_name"] = $decoded;
				}
				else
				{
					$error .= "File". $media["tmp_name"] ." is not an image.";
					$uploadOk = 0;
				}
			}
		}
		
		// Check if file already exists and change name
		/*if (file_exists($target_path)) {
			$i = 0;
			$newFile = $target_file;
			while(file_exists($target_path))
			{           
				$newFile = (string)$path_parts['filename'].$i.".".$path_parts['extension'];
				$target_path = $path_parts['dirname']."/".$newFile;
				$i++;
			}
			$target_file = $target_dir . "/" . $newFile;
			$thumb_path = getThumbPath($target_path);
			$error = $target_path;
		}*/
		// Check file size
		if ($media["size"] > 5242880) { // 5Mo x 1024 x 1024
			$error .= "Sorry, your file is too large.";
			$uploadOk = 0;
		}
		else if (!isset($uploadOk))
			$uploadOk = 1;
		
		// Check if $uploadOk is set to 0 by an error
		if ($uploadOk == 0) {
			$error .= "Sorry, your file was not uploaded.";
		// if everything is ok, try to upload file
		} else {
			if (move_uploaded_file($media["tmp_name"], $target_path))
			{
				if (file_exists($thumb_path))
					$error .= "thubmnail exists and will be replaced";
				$error .= make_thumb($target_path, $thumb_path, 512);
				$error .= "Unlink ".$target_path .": ".unlink($target_path);
				$error .= "Unlink previousPath (" . $previous_path .") :" . unlink($previous_path);
			} else {
				$error .= "Sorry, there was an error uploading your file to :". $target_file ." : ".$media["error"];
			}
		}
		
		if ($error != "")
			error_log("upload image with error : " . $error);
		
		$array = array("path" => $target_file, "error" => $error);
		echo json_encode($array);
	}
	else if (isset($_POST["delete"])) {
		DeleteFile($_POST['path']);
	}
}


function DeleteFile($filePath) {
	$path = getcwd().$filePath;
	$error = "Unlink ".$path .": ".unlink($path);
	
	$array = array("error" => $error);
	echo json_encode($array);
}
/*********************************************************/

function CreateThumbToExistingMedia() 
{
	$files = array();
	$basePath = getcwd() . "/img";
	$imgExts = "jpg,jpeg,png";
	$movExts = "mpeg,mp4,avi,mov";
	$paths = "{".$basePath."*.{".strtoupper($imgExts).",".$imgExts."}}";
	echo $paths + "<br>";
	$filesWithoutThumbs = array();
	$i = 0;
	
	foreach (glob($paths, GLOB_BRACE) as $file) 
	{
		$files[] = $file;
	}
	
	// une boucle pour checker si les images ont des thumbs
	foreach ($files as &$file) 
	{
		$imagePath = $file;
		echo "piou: " . $imagePath . " " . $i;
		if (strpos($imagePath, '_thumb') === false) 
		{
			$filesWithoutThumbs[$i] = $imagePath;
			$i = $i+1;
			echo "add<br>";
			echo count($filesWithoutThumbs) . "<br>";
		} else {
			$filesWithoutThumbs [$i-1] = null;
			echo "remove<br>";
			echo count($filesWithoutThumbs) . "<br>";
		}
	}
	echo count($filesWithoutThumbs) . "<br>";
	
	// une boucle pour faire les thumbs manquantes
	foreach ($filesWithoutThumbs as &$fileWithoutThumb) 
	{
		echo "piou2: " + $fileWithoutThumb;
		if ($fileWithoutThumb !== null) 
		{
			echo "piou3: " + $fileWithoutThumb;
			if (strpos($imagePath, '_thumb') === false) 
			{
				$thumbPath = getThumbPath($imagePath);
				make_thumb($imagePath, $thumbPath, 512);
				echo "-Create thumb: " . $thumbPath  + "<br>";
			}
		}
	}
}
/*********************************************************/

?>
