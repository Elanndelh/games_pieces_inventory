<?php

if (!isset($_POST['action'])) {
	echo "no action<br>";
	/*$iv = substr(hash('sha256', "xOKevSjsl5THGMfvG+8Swg=="), 0, 16);
	echo $iv . "<br>";
	echo openssl_decrypt("EeHqL6WmKcAQxOdlzUd9+g==", "AES-256-CBC", "pioupiou", 0, $iv);
	echo "piou<br>";*/
	
	
	//$plaintext = 'getGames';
	$password = '3sc3RLrpd17';
	// CBC has an IV and thus needs randomness every time a message is encrypted
	$method = 'aes-256-cbc';
	// Must be exact 32 chars (256 bit)
	// You must store this secret random key in a safe place of your system.
	$key = substr(hash('sha256', $password, true), 0, 32);
	echo "Password:" . $password . "<br>";
	// Most secure key
	//$key = openssl_random_pseudo_bytes(openssl_cipher_iv_length($method));

	// IV must be exact 16 chars (128 bit)
	$iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);

	// Most secure iv
	// Never ever use iv=0 in real life. Better use this iv:
	// $ivlen = openssl_cipher_iv_length($method);
	// $iv = openssl_random_pseudo_bytes($ivlen);

	// av3DYGLkwBsErphcyYp+imUW4QKs19hUnFyyYcXwURU=
	//$encrypted = base64_encode(openssl_encrypt($plaintext, $method, $key, OPENSSL_RAW_DATA, $iv));
	$encrypted = 'zNATASIHmEMx7S7ENO7dww==';

	// My secret message 1234
	$decrypted = openssl_decrypt(base64_decode($encrypted), $method, $key, OPENSSL_RAW_DATA, $iv);

	echo 'cipher=' . $method . "<br>";
	echo 'encrypted to: ' . $encrypted . "<br>";
	echo 'decrypted to: ' . $decrypted . "<br><br>";
	return;
}
else
{
	$parameter = "";
	$secretKey = "dfzeaô('2dfg54thr";

	//$mem_usage_before = memory_get_usage(true);

	foreach($_POST as $key => $value)
	{
		if($key != 'token')
		{
			$parameter .= $_POST[$key];
		}
	}
	$parameter .= $secretKey;
	$token = hash('sha256', $parameter);
	
	if (isset($_POST['token']) == false || strcmp($token, $_POST['token']) != 0)
		return;
	
	include("db_config.php");
	
	switch ($_POST['action']) {
		case 'getGamesNumber':
			echo GetGamesNumber($conn);
			break;	
		case 'getGames':
			echo GetGames($conn);
			break;	
		case 'addGame':
			echo AddGame($conn);
			break;	
		case 'updateGame':
			UpdateGame($conn);
			break;
		case 'deleteGame':
			DeleteGame($conn);
			break;
			
		case 'getPieces':
			echo GetPieces($conn);
			break;		
		case 'addPiece':
			echo AddPiece($conn);
			break;		
		case 'updatePiece':
			UpdatePiece($conn);
			break;
		case 'deletePiece':
			DeletePiece($conn);
			break;
			
		case 'getCollectionsNumber':
			echo GetCollectionsNumber($conn);
			break;	
		case 'getCollections':
			echo GetCollections($conn);
			break;	
		case 'addCollection':
			echo AddCollection($conn);
			break;	
		case 'updateCollection':
			UpdateCollection($conn);
			break;
		case 'deleteCollection':
			DeleteCollection($conn);
			break;
			
		case 'getBooks':
			echo GetBooks($conn);
			break;		
		case 'addBook':
			echo AddBook($conn);
			break;		
		case 'updateBook':
			UpdateBook($conn);
			break;
		case 'deleteBook':
			DeleteBook($conn);
			break;
	}
}


function GetGamesNumber(mysqli $conn) : string {
	$unvalidatedDate = isset($_POST['unvalidatedDate']) ? $_POST['unvalidatedDate'] : -1;
	$search = isset($_POST['search']) ? "%".$_POST['search']."%" : -1;
	$age = isset($_POST['age']) ? $_POST['age'] : -1;
	$playerNumber = isset($_POST['playerNumber']) ? $_POST['playerNumber'] : -1;
	$duration = isset($_POST['duration']) ? $_POST['duration'] : -1;
	
	$typesStr = '';
	$arrayParams = [];
	$whereClauses = [];
	
	if ($unvalidatedDate != -1) {
		$typesStr .= 's';
		array_push($arrayParams, $unvalidatedDate);
		array_push($whereClauses, 'last_validation_date < ?');
	}
	if ($search != -1) {
		$typesStr .= 'sss';
		array_push($arrayParams, $search);
		array_push($arrayParams, $search);
		array_push($arrayParams, $search);
		array_push($whereClauses, '(name LIKE ? OR scan LIKE ? OR custom_id LIKE ?)');
	}
	if ($age != -1) {
		$typesStr .= 'i';
		array_push($arrayParams, $age);
		array_push($whereClauses, 'age_limit <= ?');
	}
	if ($playerNumber != -1) {
		$typesStr .= 'ii';
		array_push($arrayParams, $playerNumber);
		array_push($arrayParams, $playerNumber);
		array_push($whereClauses, 'min_player_limit <= ? AND max_player_limit >= ?');
	}
	if ($duration != -1) {
		$typesStr .= 'i';
		array_push($arrayParams, $duration);
		array_push($whereClauses, 'duration >= ?');
	}
	
	$requestStr = 'SELECT COUNT(*) as numberOfGame FROM game';
	if (count($whereClauses) == 0) {
		$stmt = mysqli_prepare($conn, $requestStr);
	} else {
		$requestStr .= ' WHERE ';
		for ($i = 0; $i < count($whereClauses); $i++) {
			if ($i > 0) {
				$requestStr .= ' AND ';
			}
			$requestStr .= $whereClauses[$i];
		}
		$stmt = mysqli_prepare($conn, $requestStr);
		if ($stmt != false) 
			mysqli_stmt_bind_param($stmt, $typesStr, ...$arrayParams);
	}
	
	
	/*if ($unvalidatedDate != -1) {
		if ($search != -1) {
			$stmt = mysqli_prepare($conn, "SELECT COUNT(*) as numberOfGame FROM game WHERE last_validation_date < ? AND name LIKE ? OR scan LIKE ?");
			if ($stmt != false) 
				mysqli_stmt_bind_param($stmt, 'sss', $unvalidatedDate, $search, $search);
		}
		else {
			$stmt = mysqli_prepare($conn, "SELECT COUNT(*) as numberOfGame FROM game WHERE last_validation_date < ?");
			if ($stmt != false) 
				mysqli_stmt_bind_param($stmt, 's', $unvalidatedDate);
		}
	} else {
		if ($search != -1) {
			$stmt = mysqli_prepare($conn, "SELECT COUNT(*) as numberOfGame FROM game WHERE name LIKE ? OR scan LIKE ?");
			if ($stmt != false) {
				$arrayParams = [];
				array_push($arrayParams, $search);
				array_push($arrayParams, $search);
				//$arrayParams = [$search, $search];
				mysqli_stmt_bind_param($stmt, 'ss', ...$arrayParams);//$search, $search);
			}
		} 
		else {
			$stmt = mysqli_prepare($conn, "SELECT COUNT(*) as numberOfGame FROM game");
		}
	}*/
	
	if ($stmt != false) {
		
		mysqli_stmt_execute($stmt);
		$result = mysqli_stmt_get_result($stmt);
		if ($result != false)  {
			$data = mysqli_fetch_assoc($result);
			return $data['numberOfGame'];
		}
	}
	
	return "";
}
/*********************************************************/

function GetGames(mysqli $conn) : string {
	
	$begin = isset($_POST['begin']) ? $_POST['begin'] : -1;
	$elementsNumber = isset($_POST['elementsNumber']) ? $_POST['elementsNumber'] : -1;
	$unvalidatedDate = isset($_POST['unvalidatedDate']) ? $_POST['unvalidatedDate'] : -1;
	$search = isset($_POST['search']) ? "%".$_POST['search']."%" : -1;
	$age = isset($_POST['age']) ? $_POST['age'] : -1;
	$playerNumber = isset($_POST['playerNumber']) ? $_POST['playerNumber'] : -1;
	$duration = isset($_POST['duration']) ? $_POST['duration'] : -1;
	$games = "";
	
	
	$typesStr = '';
	$arrayParams = [];
	$whereClauses = [];
	
	if ($unvalidatedDate != -1) {
		$typesStr .= 's';
		array_push($arrayParams, $unvalidatedDate);
		array_push($whereClauses, 'last_validation_date < ?');
	}
	if ($search != -1) {
		$typesStr .= 'sss';
		array_push($arrayParams, $search);
		array_push($arrayParams, $search);
		array_push($arrayParams, $search);
		array_push($whereClauses, '(name LIKE ? OR scan LIKE ? OR custom_id LIKE ?)');
	}
	if ($age != -1) {
		$typesStr .= 'i';
		array_push($arrayParams, $age);
		array_push($whereClauses, 'age_limit <= ?');
	}
	if ($playerNumber != -1) {
		$typesStr .= 'ii';
		array_push($arrayParams, $playerNumber);
		array_push($arrayParams, $playerNumber);
		array_push($whereClauses, 'min_player_limit <= ? AND max_player_limit >= ?');
	}
	if ($duration != -1) {
		$typesStr .= 'i';
		array_push($arrayParams, $duration);
		array_push($whereClauses, 'duration >= ?');
	}
	$typesStr .= 'ii';
	array_push($arrayParams, $begin);
	array_push($arrayParams, $elementsNumber);
	
	$requestStr = 'SELECT * FROM game';
	if (count($whereClauses) > 0) {
		$requestStr .= ' WHERE ';
		for ($i = 0; $i < count($whereClauses); $i++) {
			if ($i > 0) {
				$requestStr .= ' AND ';
			}
			$requestStr .= $whereClauses[$i];
		}
	}
	$requestStr .= ' ORDER BY name LIMIT ?, ?';
	$stmt = mysqli_prepare($conn, $requestStr);
	if ($stmt != false)
		mysqli_stmt_bind_param($stmt, $typesStr, ...$arrayParams);
	
	
	/*if ($unvalidatedDate != -1) {
		if ($search != -1) {
			$stmt = mysqli_prepare($conn, "SELECT * FROM game WHERE last_validation_date < ? AND name LIKE ? OR scan LIKE ? ORDER BY name LIMIT ?, ?");
			if ($stmt != false)
				mysqli_stmt_bind_param($stmt, 'sssii', $unvalidatedDate, $search, $search, $begin, $elementsNumber);
		}
		else {
			$stmt = mysqli_prepare($conn, "SELECT * FROM game WHERE last_validation_date < ? ORDER BY name LIMIT ?, ?");
			if ($stmt != false)
				mysqli_stmt_bind_param($stmt, 'sii', $unvalidatedDate, $begin, $elementsNumber);
		}
	} else {
		if ($search != -1) {
			$stmt = mysqli_prepare($conn, "SELECT * FROM game WHERE name LIKE ? OR scan LIKE ? ORDER BY name LIMIT ?, ?");
			if ($stmt != false)
				mysqli_stmt_bind_param($stmt, 'ssii', $search, $search, $begin, $elementsNumber);
		} 
		else {
			$stmt = mysqli_prepare($conn, "SELECT * FROM game ORDER BY name LIMIT ?, ?");
			if ($stmt != false)
				mysqli_stmt_bind_param($stmt, 'ii', $begin, $elementsNumber);
		}
	}*/
	
	if ($stmt != false) {
		mysqli_stmt_execute($stmt);
		$result = mysqli_stmt_get_result($stmt);
		
		if ($result != false && mysqli_num_rows($result) > 0) {
			$games .= "games:[";
			while ($row = mysqli_fetch_assoc($result)) {
				$games .= "{ID¤" . $row['id'] . "|name¤". $row['name'] . "|path¤" . $row['path']. "|last_validation_date¤" . $row['last_validation_date']. "|scan¤" . $row['scan']. "|descr¤" . $row['descr'] . "|placeType¤". $row['place_type'] . "|place_details¤". $row['place_details'] . "|place_name¤". $row['place_name'] . "|place_reminder¤". $row['place_reminder'] . "|age_limit¤". $row['age_limit'] . "|min_player_limit¤". $row['min_player_limit'] . "|max_player_limit¤". $row['max_player_limit'] . "|duration¤". $row['duration'] . "|custom_id¤". $row['custom_id']  . "},";
			}
			$games .= "]";
		}
	}

	return $games;
}
/*********************************************************/
function GetPieces(mysqli $conn) : string {
	$idGame = $_POST['idGame'];
	$pieces = "";
	
	$stmt = mysqli_prepare($conn, "SELECT * FROM piece WHERE id_game = ?");
	if ($stmt != false) {
		mysqli_stmt_bind_param($stmt, 'i', $idGame);
		mysqli_stmt_execute($stmt);
		$result = mysqli_stmt_get_result($stmt);
		
		if ($result != false && mysqli_num_rows($result) > 0) {
			$pieces .= "pieces:[";
			while ($row = mysqli_fetch_assoc($result)) {
				$pieces .= "{ID¤" . $row['id'] . "|id_game¤". $row['id_game'] . "|name¤" . $row['name']. "|path¤" . $row['path'] . "|number¤". $row['number'] . "|number_orig¤". $row['number_orig'] . "},";
			}
			$pieces .= "]";
		}
	}
	
	return $pieces;
}
/*********************************************************/

function AddGame(mysqli $conn) : string {
	$name = $_POST['name'];
	$path = $_POST['path'];
	$lastValidationDate = $_POST['lastValidationDate'];
	$scan = $_POST['scan'];
	$descr = $_POST['descr'];
	$placeType = $_POST['placeType'];
	$placeDetails = $_POST['placeDetails'];
	$placeName = $_POST['placeName'];
	$placeReminder = $_POST['placeReminder'];
	$ageLimit = $_POST['ageLimit'];
	$minPlayerLimit = $_POST['minPlayerLimit'];
	$maxPlayerLimit = $_POST['maxPlayerLimit'];
	$duration = $_POST['duration'];
	$customId = $_POST['customId'];
		
	$stmt = mysqli_prepare($conn, "INSERT INTO game (name, path, last_validation_date, scan, descr, place_type, place_details, place_name, place_reminder, age_limit, min_player_limit, max_player_limit, duration, custom_id) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
	if ($stmt != false) {
		mysqli_stmt_bind_param($stmt, 'sssssisssiiiis', $name, $path, $lastValidationDate, $scan, $descr, $placeType, $placeDetails, $placeName, $placeReminder, $ageLimit, $minPlayerLimit, $maxPlayerLimit, $duration, $customId);
		mysqli_stmt_execute($stmt);
	}
	
	return (string)mysqli_insert_id($conn);
}
/*********************************************************/
function AddPiece(mysqli $conn) : string {
	$idGame = $_POST['idGame'];
	$name = $_POST['name'];
	$path = $_POST['path'];
	$number = $_POST['number'];
	$numberOrig = $_POST['numberOrig'];
		
	$stmt = mysqli_prepare($conn, "INSERT INTO piece (id_game, name, path, number, number_orig) VALUES (?, ?, ?, ?, ?)");
	if ($stmt != false) {
		mysqli_stmt_bind_param($stmt, 'issii', $idGame, $name, $path, $number, $numberOrig);
		mysqli_stmt_execute($stmt);
	}
	
	return (string)mysqli_insert_id($conn);
}
/*********************************************************/

function UpdateGame(mysqli $conn) : void {
	$id = $_POST['id'];
	$name = $_POST['name'];
	$path = $_POST['path'];
	$lastValidationDate = $_POST['lastValidationDate'];
	$scan = $_POST['scan'];
	$descr = $_POST['descr'];
	$placeType = $_POST['placeType'];
	$placeDetails = $_POST['placeDetails'];
	$placeName = $_POST['placeName'];
	$placeReminder = $_POST['placeReminder'];
	$ageLimit = $_POST['ageLimit'];
	$minPlayerLimit = $_POST['minPlayerLimit'];
	$maxPlayerLimit = $_POST['maxPlayerLimit'];
	$duration = $_POST['duration'];
	$customId = $_POST['customId'];
		
	$stmt = mysqli_prepare($conn, "UPDATE game SET name = ?, path = ?, last_validation_date = ?, scan = ?, descr = ?, place_type = ?, place_details = ?, place_name = ?, place_reminder = ?, age_limit = ?, min_player_limit = ?, max_player_limit = ?, duration = ?, custom_id = ? WHERE id = ?");
	if ($stmt != false) {
		mysqli_stmt_bind_param($stmt, 'sssssisssiiiisi', $name, $path, $lastValidationDate, $scan, $descr, $placeType, $placeDetails, $placeName, $placeReminder, $ageLimit, $minPlayerLimit, $maxPlayerLimit, $duration, $customId, $id);
		mysqli_stmt_execute($stmt);
	}
}
/*********************************************************/
function UpdatePiece(mysqli $conn) : void {
	$id = $_POST['id'];
	$name = $_POST['name'];
	$path = $_POST['path'];
	$number = $_POST['number'];
	$numberOrig = $_POST['numberOrig'];
		
	$stmt = mysqli_prepare($conn, "UPDATE piece SET name = ?, path = ?, number = ?, number_orig = ? WHERE id = ?");
	if ($stmt != false) {
		mysqli_stmt_bind_param($stmt, 'ssiii', $name, $path, $number, $numberOrig, $id);
		mysqli_stmt_execute($stmt);
	}
}
/*********************************************************/

function DeleteGame(mysqli $conn) : void {
	$id = $_POST['id'];
	
	require_once('medias.php');
	
	// Delete pieces' images
	$stmt = mysqli_prepare($conn, "SELECT * FROM piece WHERE id_game = ?");
	if ($stmt != false) {
		mysqli_stmt_bind_param($stmt, 'i', $id);
		mysqli_stmt_execute($stmt);
		$result = mysqli_stmt_get_result($stmt);
		if ($result != false && mysqli_num_rows($result) > 0) {
			while ($row = mysqli_fetch_assoc($result)) {
				if ($row["path"] != "" && $row["path"] != NULL)
					DeleteFile($row["path"]);
			}
		}
	}
	
	// Delete pieces
	$stmt = mysqli_prepare($conn, "DELETE FROM piece WHERE id_game = ?");
	if ($stmt != false) {
		mysqli_stmt_bind_param($stmt, 'i', $id);
		mysqli_stmt_execute($stmt);
	}
	
	// Delete game
	$stmt = mysqli_prepare($conn, "DELETE FROM game WHERE id = ?");
	if ($stmt != false) {
		mysqli_stmt_bind_param($stmt, 'i', $id);
		mysqli_stmt_execute($stmt);
	}
}
/*********************************************************/
function DeletePiece(mysqli $conn) : void {
	$id = $_POST['id'];
	
	$stmt = mysqli_prepare($conn, "DELETE FROM piece WHERE id = ?");
	if ($stmt != false) {
		mysqli_stmt_bind_param($stmt, 'i', $id);
		mysqli_stmt_execute($stmt);
	}
}
/*********************************************************/


function GetCollectionsNumber(mysqli $conn) : string {
	$search = isset($_POST['search']) ? "%".$_POST['search']."%" : -1;
	
	if ($search != -1) {
		$stmt = mysqli_prepare($conn, "SELECT COUNT(*) as numberOfCollections FROM collection WHERE name LIKE ?");
		if ($stmt != false) 
			mysqli_stmt_bind_param($stmt, 's', $search);
	} 
	else {
		$stmt = mysqli_prepare($conn, "SELECT COUNT(*) as numberOfCollections FROM collection");
	}
	
	if ($stmt != false) {
		
		mysqli_stmt_execute($stmt);
		$result = mysqli_stmt_get_result($stmt);
		if ($result != false)  {
			$data = mysqli_fetch_assoc($result);
			return $data['numberOfCollections'];
		}
	}
	
	return "";
}
/*********************************************************/

function GetCollections(mysqli $conn) : string {
	
	$begin = isset($_POST['begin']) ? $_POST['begin'] : -1;
	$elementsNumber = isset($_POST['elementsNumber']) ? $_POST['elementsNumber'] : -1;
	$search = isset($_POST['search']) ? "%".$_POST['search']."%" : -1;
	$collections = "";
	
	if ($search != -1) {
		$stmt = mysqli_prepare($conn, "SELECT * FROM collection WHERE name LIKE ? ORDER BY name LIMIT ?, ?");
		if ($stmt != false)
			mysqli_stmt_bind_param($stmt, 'sii', $search, $begin, $elementsNumber);
	} 
	else {
		$stmt = mysqli_prepare($conn, "SELECT * FROM collection ORDER BY name LIMIT ?, ?");
		if ($stmt != false)
			mysqli_stmt_bind_param($stmt, 'ii', $begin, $elementsNumber);
	}
	
	if ($stmt != false) {
		mysqli_stmt_execute($stmt);
		$result = mysqli_stmt_get_result($stmt);
		
		if ($result != false && mysqli_num_rows($result) > 0) {
			$collections .= "collections:[";
			while ($row = mysqli_fetch_assoc($result)) {
				$collections .= "{ID¤" . $row['id'] . "|name¤". $row['name'] . "|path¤" . $row['path']. "|descr¤" . $row['descr']. "|complete¤" . $row['complete'] . "},";
			}
			$collections .= "]";
		}
	}

	return $collections;
}
/*********************************************************/
function AddCollection(mysqli $conn) : string {
	$name = $_POST['name'];
	$path = $_POST['path'];
	$descr = $_POST['descr'];
	$complete = $_POST['complete'];
		
	$stmt = mysqli_prepare($conn, "INSERT INTO collection (name, path, descr, complete) VALUES(?, ?, ?, ?)");
	if ($stmt != false) {
		mysqli_stmt_bind_param($stmt, 'sssi', $name, $path, $descr, $complete);
		mysqli_stmt_execute($stmt);
	}
	return (string)mysqli_insert_id($conn);
}
/*********************************************************/
function UpdateCollection(mysqli $conn) : void {
	$id = $_POST['id'];
	$name = $_POST['name'];
	$path = $_POST['path'];
	$descr = $_POST['descr'];
	$complete = $_POST['complete'];
		
	$stmt = mysqli_prepare($conn, "UPDATE collection SET name = ?, path = ?, descr = ?, complete = ? WHERE id = ?");
	if ($stmt != false) {
		mysqli_stmt_bind_param($stmt, 'sssii', $name, $path, $descr, $complete, $id);
		mysqli_stmt_execute($stmt);
	}
}
/*********************************************************/
function DeleteCollection(mysqli $conn) : void {
	$id = $_POST['id'];
	
	require_once('medias.php');
	
	// Delete books' images
	$stmt = mysqli_prepare($conn, "SELECT * FROM book WHERE id_collection = ?");
	if ($stmt != false) {
		mysqli_stmt_bind_param($stmt, 'i', $id);
		mysqli_stmt_execute($stmt);
		$result = mysqli_stmt_get_result($stmt);
		if ($result != false && mysqli_num_rows($result) > 0) {
			while ($row = mysqli_fetch_assoc($result)) {
				if ($row["path"] != "" && $row["path"] != NULL)
					DeleteFile($row["path"]);
			}
		}
	}
	
	// Delete books
	$stmt = mysqli_prepare($conn, "DELETE FROM book WHERE id_collection = ?");
	if ($stmt != false) {
		mysqli_stmt_bind_param($stmt, 'i', $id);
		mysqli_stmt_execute($stmt);
	}
	
	// Delete collection
	$stmt = mysqli_prepare($conn, "DELETE FROM collection WHERE id = ?");
	if ($stmt != false) {
		mysqli_stmt_bind_param($stmt, 'i', $id);
		mysqli_stmt_execute($stmt);
	}
}
/*********************************************************/

function GetBooks(mysqli $conn) : string {
	$idCollection = $_POST['idCollection'];
	$data = "";
	
	$stmt = mysqli_prepare($conn, "SELECT * FROM book WHERE id_collection = ?");
	if ($stmt != false) {
		mysqli_stmt_bind_param($stmt, 'i', $idCollection);
		mysqli_stmt_execute($stmt);
		$result = mysqli_stmt_get_result($stmt);
		
		if ($result != false && mysqli_num_rows($result) > 0) {
			$data .= "books:[";
			while ($row = mysqli_fetch_assoc($result)) {
				$data .= "{ID¤" . $row['id'] . "|id_collection¤". $row['id_collection'] . "|name¤" . $row['name']. "|path¤" . $row['path'] . "|page_number¤". $row['page_number'] . "|is_read¤". $row['is_read'] . "|scan¤". $row['scan'] . "|isbn¤". $row['isbn'] . "|descr¤". $row['descr'] . "|placeType¤". $row['place_type'] . "|place_details¤". $row['place_details'] . "|place_name¤". $row['place_name'] . "|place_reminder¤". $row['place_reminder'] . "|volume_number¤". $row['volume_number'] . "},";
			}
			$data .= "]";
		}
	}
	
	return $data;
}
/*********************************************************/
function AddBook(mysqli $conn) : string {
	$idCollection = $_POST['idCollection'];
	$name = $_POST['name'];
	$path = $_POST['path'];
	$pageNumber = $_POST['pageNumber'];
	$isRead = $_POST['isRead'];
	$scan = $_POST['scan'];
	$isbn = $_POST['isbn'];
	$descr = $_POST['descr'];
	$placeType = $_POST['placeType'];
	$placeDetails = $_POST['placeDetails'];
	$placeName = $_POST['placeName'];
	$placeReminder = $_POST['placeReminder'];
	$volumeNumber = $_POST['volumeNumber'];
		
	$stmt = mysqli_prepare($conn, "INSERT INTO book (id_collection, name, path, page_number, is_read, scan, isbn, descr, place_type, place_details, place_name, place_reminder, volume_number) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
	if ($stmt != false) {
		mysqli_stmt_bind_param($stmt, 'issiisssisssi', $idCollection, $name, $path, $pageNumber, $isRead, $scan, $isbn, $descr, $placeType, $placeDetails, $placeName, $placeReminder, $volumeNumber);
		mysqli_stmt_execute($stmt);
	}
	
	return (string)mysqli_insert_id($conn);
}
/*********************************************************/
function UpdateBook(mysqli $conn) : void {
	$id = $_POST['id'];
	$name = $_POST['name'];
	$path = $_POST['path'];
	$pageNumber = $_POST['pageNumber'];
	$isRead = $_POST['isRead'];
	$scan = $_POST['scan'];
	$isbn = $_POST['isbn'];
	$descr = $_POST['descr'];
	$placeType = $_POST['placeType'];
	$placeDetails = $_POST['placeDetails'];
	$placeName = $_POST['placeName'];
	$placeReminder = $_POST['placeReminder'];
	$volumeNumber = $_POST['volumeNumber'];
		
	$stmt = mysqli_prepare($conn, "UPDATE book SET name = ?, path = ?, page_number = ?, is_read = ?, scan = ?, isbn = ?, descr = ?, place_type = ?, place_details = ?, place_name = ?, place_reminder = ?, volume_number = ? WHERE id = ?");
	if ($stmt != false) {
		mysqli_stmt_bind_param($stmt, 'ssiisssisssii', $name, $path, $pageNumber, $isRead, $scan, $isbn, $descr, $placeType, $placeDetails, $placeName, $placeReminder, $volumeNumber, $id);
		mysqli_stmt_execute($stmt);
	}
}
/*********************************************************/
function DeleteBook(mysqli $conn) : void {
	$id = $_POST['id'];
	
	$stmt = mysqli_prepare($conn, "DELETE FROM book WHERE id = ?");
	if ($stmt != false) {
		mysqli_stmt_bind_param($stmt, 'i', $id);
		mysqli_stmt_execute($stmt);
	}
}
/*********************************************************/
?>